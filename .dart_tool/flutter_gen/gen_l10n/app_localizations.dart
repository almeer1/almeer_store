import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'app_localizations_ar.dart';
import 'app_localizations_en.dart';

/// Callers can lookup localized strings with an instance of AppLocalizations
/// returned by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'gen_l10n/app_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('ar'),
    Locale('en')
  ];

  /// No description provided for @lang_menu_arabic.
  ///
  /// In ar, this message translates to:
  /// **'العربية'**
  String get lang_menu_arabic;

  /// No description provided for @lang_menu_english.
  ///
  /// In ar, this message translates to:
  /// **'English'**
  String get lang_menu_english;

  /// No description provided for @login_save_dont_have_account.
  ///
  /// In ar, this message translates to:
  /// **'ليس لديك حساب'**
  String get login_save_dont_have_account;

  /// No description provided for @signup_save_have_account.
  ///
  /// In ar, this message translates to:
  /// **' لديك حساب'**
  String get signup_save_have_account;

  /// No description provided for @login.
  ///
  /// In ar, this message translates to:
  /// **'تسجيل الدخول'**
  String get login;

  /// No description provided for @login_welcome.
  ///
  /// In ar, this message translates to:
  /// **'أهلاً بعودتك'**
  String get login_welcome;

  /// No description provided for @forget_pass.
  ///
  /// In ar, this message translates to:
  /// **'هل نسيت كلمة المرور؟'**
  String get forget_pass;

  /// No description provided for @email_label.
  ///
  /// In ar, this message translates to:
  /// **'الإيميل'**
  String get email_label;

  /// No description provided for @phone_number.
  ///
  /// In ar, this message translates to:
  /// **'رقم الهاتف'**
  String get phone_number;

  /// No description provided for @password_label.
  ///
  /// In ar, this message translates to:
  /// **'كلمة المرور'**
  String get password_label;

  /// No description provided for @phone_number_text.
  ///
  /// In ar, this message translates to:
  /// **'من فضلك ادخل رقم الهاتف'**
  String get phone_number_text;

  /// No description provided for @email_text.
  ///
  /// In ar, this message translates to:
  /// **'من فضلك ادخل بريدك الالكتروني'**
  String get email_text;

  /// No description provided for @pass_text.
  ///
  /// In ar, this message translates to:
  /// **'من فضلك ادخل كلمة المرور'**
  String get pass_text;

  /// No description provided for @signup.
  ///
  /// In ar, this message translates to:
  /// **'اشترك الآن'**
  String get signup;

  /// No description provided for @create_account.
  ///
  /// In ar, this message translates to:
  /// **'إنشاء حساب'**
  String get create_account;

  /// No description provided for @first_name.
  ///
  /// In ar, this message translates to:
  /// **'الأسم الأول'**
  String get first_name;

  /// No description provided for @userName.
  ///
  /// In ar, this message translates to:
  /// **'اسم المستخدم'**
  String get userName;

  /// No description provided for @enter_userName.
  ///
  /// In ar, this message translates to:
  /// **'من فضلك ادخل اسم المستخدم'**
  String get enter_userName;

  /// No description provided for @enter_first_name.
  ///
  /// In ar, this message translates to:
  /// **'من فضلك ادخل اسمك'**
  String get enter_first_name;

  /// No description provided for @last_name.
  ///
  /// In ar, this message translates to:
  /// **'الكنية '**
  String get last_name;

  /// No description provided for @enter_last_name.
  ///
  /// In ar, this message translates to:
  /// **'من فضلك ادخل الكنية'**
  String get enter_last_name;

  /// No description provided for @back.
  ///
  /// In ar, this message translates to:
  /// **'رجوع'**
  String get back;

  /// No description provided for @new_password_text.
  ///
  /// In ar, this message translates to:
  /// **'كلمة مرورك  يجب انت تكون مختلفة عن كلمات المرور السابقة'**
  String get new_password_text;

  /// No description provided for @password_hint.
  ///
  /// In ar, this message translates to:
  /// **'من فضلك ادخال كلمة المرور الجديدة'**
  String get password_hint;

  /// No description provided for @reset_password.
  ///
  /// In ar, this message translates to:
  /// **'إعادة تعيين كلمة المرور'**
  String get reset_password;

  /// No description provided for @create_new_password.
  ///
  /// In ar, this message translates to:
  /// **'انشاء كلمة مرور جديدة'**
  String get create_new_password;

  /// No description provided for @forgetPasswordText.
  ///
  /// In ar, this message translates to:
  /// **'ادخل البريد الالكتروني الخاص بك وسوف نرسل لك رابطاً لإعادة تعيين كلمة المرور الخاصة بك'**
  String get forgetPasswordText;

  /// No description provided for @check_email.
  ///
  /// In ar, this message translates to:
  /// **'تأكد من حسابك'**
  String get check_email;

  /// No description provided for @send_email.
  ///
  /// In ar, this message translates to:
  /// **'أرسلنا تأكيد كلمة المرور إلى حسابك'**
  String get send_email;

  /// No description provided for @open_email_app.
  ///
  /// In ar, this message translates to:
  /// **'افتح تطبيق الايميل'**
  String get open_email_app;

  /// No description provided for @skip.
  ///
  /// In ar, this message translates to:
  /// **'تخطي؟'**
  String get skip;

  /// No description provided for @confirm_later.
  ///
  /// In ar, this message translates to:
  /// **'سوف أؤكد لاحقاً'**
  String get confirm_later;

  /// No description provided for @receive_email.
  ///
  /// In ar, this message translates to:
  /// **'لم تستلم الإيميل؟ افحص جودة الانترنيت أو'**
  String get receive_email;

  /// No description provided for @another_email.
  ///
  /// In ar, this message translates to:
  /// **' جرب بريد إلكتروني آخر'**
  String get another_email;

  /// No description provided for @food.
  ///
  /// In ar, this message translates to:
  /// **'طعام'**
  String get food;

  /// No description provided for @noon.
  ///
  /// In ar, this message translates to:
  /// **'نون'**
  String get noon;

  /// No description provided for @grocery.
  ///
  /// In ar, this message translates to:
  /// **'بقالة'**
  String get grocery;

  /// No description provided for @food_text.
  ///
  /// In ar, this message translates to:
  /// **'عرووض رهيبة على ألذ الوجبات'**
  String get food_text;

  /// No description provided for @noon_text.
  ///
  /// In ar, this message translates to:
  /// **'أفضل الأسعار لكل ما تحتاجه'**
  String get noon_text;

  /// No description provided for @grocery_text.
  ///
  /// In ar, this message translates to:
  /// **'أكبر تشكيلة من المستلزمات'**
  String get grocery_text;

  /// No description provided for @hom_text.
  ///
  /// In ar, this message translates to:
  /// **'اقتراحات لك'**
  String get hom_text;

  /// No description provided for @search.
  ///
  /// In ar, this message translates to:
  /// **'البحث'**
  String get search;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['ar', 'en'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'ar': return AppLocalizationsAr();
    case 'en': return AppLocalizationsEn();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
