import 'app_localizations.dart';

/// The translations for Arabic (`ar`).
class AppLocalizationsAr extends AppLocalizations {
  AppLocalizationsAr([String locale = 'ar']) : super(locale);

  @override
  String get lang_menu_arabic => 'العربية';

  @override
  String get lang_menu_english => 'English';

  @override
  String get login_save_dont_have_account => 'ليس لديك حساب';

  @override
  String get signup_save_have_account => ' لديك حساب';

  @override
  String get login => 'تسجيل الدخول';

  @override
  String get login_welcome => 'أهلاً بعودتك';

  @override
  String get forget_pass => 'هل نسيت كلمة المرور؟';

  @override
  String get email_label => 'الإيميل';

  @override
  String get phone_number => 'رقم الهاتف';

  @override
  String get password_label => 'كلمة المرور';

  @override
  String get phone_number_text => 'من فضلك ادخل رقم الهاتف';

  @override
  String get email_text => 'من فضلك ادخل بريدك الالكتروني';

  @override
  String get pass_text => 'من فضلك ادخل كلمة المرور';

  @override
  String get signup => 'اشترك الآن';

  @override
  String get create_account => 'إنشاء حساب';

  @override
  String get first_name => 'الأسم الأول';

  @override
  String get userName => 'اسم المستخدم';

  @override
  String get enter_userName => 'من فضلك ادخل اسم المستخدم';

  @override
  String get enter_first_name => 'من فضلك ادخل اسمك';

  @override
  String get last_name => 'الكنية ';

  @override
  String get enter_last_name => 'من فضلك ادخل الكنية';

  @override
  String get back => 'رجوع';

  @override
  String get new_password_text => 'كلمة مرورك  يجب انت تكون مختلفة عن كلمات المرور السابقة';

  @override
  String get password_hint => 'من فضلك ادخال كلمة المرور الجديدة';

  @override
  String get reset_password => 'إعادة تعيين كلمة المرور';

  @override
  String get create_new_password => 'انشاء كلمة مرور جديدة';

  @override
  String get forgetPasswordText => 'ادخل البريد الالكتروني الخاص بك وسوف نرسل لك رابطاً لإعادة تعيين كلمة المرور الخاصة بك';

  @override
  String get check_email => 'تأكد من حسابك';

  @override
  String get send_email => 'أرسلنا تأكيد كلمة المرور إلى حسابك';

  @override
  String get open_email_app => 'افتح تطبيق الايميل';

  @override
  String get skip => 'تخطي؟';

  @override
  String get confirm_later => 'سوف أؤكد لاحقاً';

  @override
  String get receive_email => 'لم تستلم الإيميل؟ افحص جودة الانترنيت أو';

  @override
  String get another_email => ' جرب بريد إلكتروني آخر';

  @override
  String get food => 'طعام';

  @override
  String get noon => 'نون';

  @override
  String get grocery => 'بقالة';

  @override
  String get food_text => 'عرووض رهيبة على ألذ الوجبات';

  @override
  String get noon_text => 'أفضل الأسعار لكل ما تحتاجه';

  @override
  String get grocery_text => 'أكبر تشكيلة من المستلزمات';

  @override
  String get hom_text => 'اقتراحات لك';

  @override
  String get search => 'البحث';
}
