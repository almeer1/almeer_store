import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

import '../data/data_source/DBHelper.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  int? counter = 0;
  double? total=0.0;
  DatabaseHelper dbHelper = DatabaseHelper();
  CartBloc() : super(CartInitial()) {
    on<CartEvent>((event, emit) async {
      if (event is UpdateCartIconCount) {
        counter = await dbHelper.queryRowCount();
        emit(CounterValueChangeState(counter: counter));
      } else if (event is GetCartsSuccessState) {
        counter = await dbHelper.queryRowCount();
        emit(CounterValueChangeState(counter: counter));
      }  else {
        counter = 0;
        emit(CounterValueChangeState(counter: counter));
      }
    });
  }
}
