part of 'cart_bloc.dart';

@immutable
abstract class CartEvent {}

class UpdateCartIconCount extends CartEvent {}
class UpdateCartTotalPrice extends CartEvent {}


