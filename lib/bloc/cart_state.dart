part of 'cart_bloc.dart';

@immutable
abstract class CartState {}

class CartInitial extends CartState {}

//new counter value
class CounterValueChangeState extends CartState {
  final int? counter;

  CounterValueChangeState({
    required this.counter,
  });
}

class GetCartsSuccessState extends CartState {}
class GetCartsloadState extends CartState {}
class GetTolatSuccessState extends CartState{}
