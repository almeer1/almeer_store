import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'profail_event.dart';
part 'profail_state.dart';

class ProfailBloc extends Bloc<ProfailEvent, ProfailState> {
  String? imagePath = '';
  ProfailBloc() : super(ProfailInitial()) {
    on<ProfailEvent>((event, emit) async {
      // TODO: implement event handler
      if (event is DeletImage) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        imagePath = prefs.getString('path');
        prefs.remove('path');

        emit(ProfailDelte(imagePath: imagePath));
      } else if (event is ProfailDeletSuccessState) {
        emit(ProfailDelte(imagePath: imagePath));
      } else {
        imagePath = null;
        emit(ProfailDelte(imagePath: imagePath));
      }
    });
  }
}
