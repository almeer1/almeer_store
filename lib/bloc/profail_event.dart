part of 'profail_bloc.dart';

abstract class ProfailEvent extends Equatable {
  const ProfailEvent();

  @override
  List<Object> get props => [];
}

class DeletImage extends ProfailEvent {}
