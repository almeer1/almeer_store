part of 'profail_bloc.dart';

abstract class ProfailState extends Equatable {
  const ProfailState();

  @override
  List<Object> get props => [];
}

class ProfailDelte extends ProfailState {
  final String? imagePath;
  ProfailDelte({required this.imagePath});
}

class ProfailInitial extends ProfailState {}

class ProfailDeletSuccessState extends ProfailState {}
