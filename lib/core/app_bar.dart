import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:badges/badges.dart' as badges;

import '../bloc/cart_bloc.dart';
import '../data/data_source/api_controller.dart';
import '../data/model/searchproduct_model.dart';
import '../main.dart';
import '../view/notification/notification.dart';
import '../view/screen/cart/cart_screen.dart';
import '../view/screen/details/details_page.dart';
import '../view/widget/auth/medium_text.dart';
import '../view/widget/home/search.dart';
import 'constant/color_manger.dart';
import 'functions/cart_functions.dart';

class AppBarWidget extends StatefulWidget implements PreferredSizeWidget {
  final AppBar appBar;

  AppBarWidget({Key? key, required this.appBar}) : super(key: key);

  _AppBarWidgetState createState() => _AppBarWidgetState();

  @override
  Size get preferredSize => new Size.fromHeight(150);
}

class _AppBarWidgetState extends State<AppBarWidget> {
  ApiController apiController = ApiController();

  late String _base64;
  late Future getImage;

  List _listCartProduct = [];

  // get Image logo
  Future<String> _getImage() async {
    var data = await apiController.getImage();
    return data;
  }

  @override
  void initState() {
    getImage = _getImage();

    super.initState();
  }

  @override
  void didUpdateWidget(AppBarWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.only( top:MediaQuery.of(context).padding.top),
      child: SizedBox(
        height: 110,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                right: 16,
                left: 16,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Notifcation()));
                      },
                      child: Icon(
                          // Icons.shopping_cart,
                          FontAwesomeIcons.bell,
                          color: ColorManager.grey1,
                          size: 30),
                    ),
                  ),
                  Image(
                    width: 200,
                    image: AssetImage(
                      'assets/images/logo (1).png',
                    ),
                  ),
                  InkWell(
                    onTap: (){Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const CartScreen()));},
                    child: badges.Badge(
                        badgeContent: BlocBuilder<CartBloc, CartState>(
                          builder: (context, state) {
                            if (state is CartInitial) {
                              return const Text(
                                '0',
                                style:
                                    TextStyle(color: Colors.blueGrey, fontSize: 20),
                              );
                            } else if (state is CounterValueChangeState) {
                              return Text(
                                state.counter.toString(),
                                style: const TextStyle(
                                    color: Colors.blueGrey, fontSize: 20),
                              );
                            } else {
                              return const SizedBox(
                                height: 2,
                              );
                            }
                          },
                        ),
                        position: badges.BadgePosition.custom(end: 0),
                        badgeStyle: badges.BadgeStyle(
                          badgeColor: ColorManager.primary,
                        ),
                        child: InkWell(
                            // iconSize: 32,
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const CartScreen()));
                            },
                            child: Icon(
                              Icons.shopping_cart,
                              color: ColorManager.grey1,
                              size: 40,
                            ))),
                  ),
                ],
              ),
            ),
            HomeSearch(),
          ],
        ),
      ),
    );
  }
}

BoxDecoration _boxDecoration() {
  return const BoxDecoration(
    borderRadius: BorderRadius.vertical(
      bottom: Radius.circular(20),
    ),
    gradient: LinearGradient(
      colors: [Colors.white, Colors.white],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
    ),
  );
}



class AppBarDetWidget extends StatefulWidget implements PreferredSizeWidget {
  final AppBar appBar;

  AppBarDetWidget({Key? key, required this.appBar}) : super(key: key);

  _AppBarDetWidgetState createState() => _AppBarDetWidgetState();

  @override
  Size get preferredSize => new Size.fromHeight(150);
}

class _AppBarDetWidgetState extends State<AppBarDetWidget> {
  ApiController apiController = ApiController();

  late String _base64;
  late Future getImage;

  List _listCartProduct = [];

  // get Image logo
  Future<String> _getImage() async {
    var data = await apiController.getImage();
    return data;
  }

  @override
  void initState() {
    getImage = _getImage();

    super.initState();
  }

  @override
  void didUpdateWidget(AppBarDetWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:EdgeInsets.only( top:MediaQuery.of(context).padding.top),
      child: SizedBox(
        height: 110,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                right: 16,
                left: 16,

              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        alignment: Alignment.centerRight,


                        width:68 ,
                        child:
                          Icon(
                            // Icons.shopping_cart,
                              Icons.arrow_back,
                              color: ColorManager.grey1,
                              size: 30),

                      ),
                    ),
                  ),
                  Image(
                    width: 200,
                    image: AssetImage(
                      'assets/images/logo (1).png',
                    ),
                  ),
                  Row(
                    // s
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Notifcation()));
                        },
                        child: Icon(
                          // Icons.shopping_cart,
                            FontAwesomeIcons.bell,
                            color: ColorManager.grey1,
                            size: 30),
                      ),
                      SizedBox(width: 8,),

                      InkWell(
                        onTap: (){
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const CartScreen()));
                        },
                        child: badges.Badge(
                            badgeContent: BlocBuilder<CartBloc, CartState>(
                              builder: (context, state) {
                                if (state is CartInitial) {
                                  return const Text(
                                    '0',
                                    style:
                                    TextStyle(color: Colors.blueGrey, fontSize: 20),
                                  );
                                } else if (state is CounterValueChangeState) {
                                  return Text(
                                    state.counter.toString(),
                                    style: const TextStyle(
                                        color: Colors.blueGrey, fontSize: 20),
                                  );
                                } else {
                                  return const SizedBox(
                                    height: 2,
                                  );
                                }
                              },
                            ),
                            position: badges.BadgePosition.custom(end: 0),
                            badgeStyle: badges.BadgeStyle(
                              badgeColor: ColorManager.primary,
                            ),
                            child: InkWell(
                              // iconSize: 32,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => const CartScreen()));
                                },
                                child: Icon(
                                  Icons.shopping_cart,
                                  color: ColorManager.grey1,
                                  size: 40,
                                ))),
                      ),
                    ],
                  ),

                ],
              ),
            ),
            HomeSearch(),
          ],
        ),
      ),
    );
  }
}

BoxDecoration _boxDecorationd() {
  return const BoxDecoration(
    borderRadius: BorderRadius.vertical(
      bottom: Radius.circular(20),
    ),
    gradient: LinearGradient(
      colors: [Colors.white, Colors.white],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
    ),
  );
}