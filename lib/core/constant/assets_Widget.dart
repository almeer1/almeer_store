import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../data/data_source/DBHelper.dart';

import '../../data/model/product.dart';
import '../../view/screen/details/details_page.dart';
import '../../view/widget/auth/medium_text.dart';
import '../functions/cart_functions.dart';
import 'bottom_bar.dart';
import 'color_manger.dart';

class AssetWidget {
  Widget AuthTextField(TextEditingController controllerP, String hintTextP,
      bool obscuretextP, TextInputType keyboardType, TextStyle style) {
    return TextFormField(
        controller: controllerP,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(top: 2),
          hintText: hintTextP,
          hintStyle: TextStyle(
              fontFamily: 'Cairo', color: ColorManager.grey1, fontSize: 16),

          //label: Text(''),
        ),
        /* keyboardType: keyboardType,*/
        obscureText: obscuretextP,
        style: style);
  }

  Widget TextFieldwidgt(TextEditingController controllerP, String hintTextP,
      Icon iconp, TextInputType keyboardType, TextStyle style,
      [bool obscureText_p = true]) {
    return TextFormField(
        controller: controllerP,
        keyboardType: keyboardType,
        obscureText: obscureText_p,
        // autofocus: true,
        decoration: InputDecoration(
          //contentPadding: EdgeInsets.only(top:0.2),
          hintText: hintTextP,
          hintStyle: TextStyle(
              fontFamily: 'Cairo', color: ColorManager.grey1, fontSize: 16),
          suffixIcon: iconp,
          /* label: Text(labelP),*/
        ),
        style: style
        /* keyboardType: keyboardType,*/

        );
  }

  Widget TextFieldwidgtPass(
      TextEditingController controllerP,
      String hintTextP,
      IconButton iconp,
      TextInputType keyboardType,
      TextStyle style,
      obscureText_p) {
    return TextFormField(
        controller: controllerP,
        keyboardType: keyboardType,
        obscureText: obscureText_p,
        // autofocus: true,
        decoration: InputDecoration(
          //contentPadding: EdgeInsets.only(top:0.2),
          hintText: hintTextP,
          hintStyle: TextStyle(
              fontFamily: 'Cairo', color: ColorManager.grey1, fontSize: 16),
          suffixIcon: iconp,
          /* label: Text(labelP),*/
        ),
        style: style
        /* keyboardType: keyboardType,*/

        );
  }

  AlertDia(BuildContext context, String title, String content) {
    return showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Text(
                title,
                style: TextStyle(
                    fontFamily: 'Cairo',
                    color: ColorManager.grey1,
                    fontSize: 18),
              ),
              content: Text(content,
                  style: TextStyle(
                      fontFamily: 'Cairo',
                      color: ColorManager.grey1,
                      fontSize: 16)),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context, 'OK'),
                  child: Text('OK',
                      style: TextStyle(
                          fontFamily: 'Cairo',
                          color: ColorManager.primary,
                          fontSize: 16)),
                ),
              ],
            ));
  }

  AlertDial(BuildContext context, String title, String content) {
    return showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Text(
                title,
                style: TextStyle(
                    fontFamily: 'Cairo',
                    color: ColorManager.grey1,
                    fontSize: 18),
              ),
              content: Text(content,
                  style: TextStyle(
                      fontFamily: 'Cairo',
                      color: ColorManager.grey1,
                      fontSize: 16)),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.of(context)
                      .pushNamedAndRemoveUntil(
                          '/BottomNav', (Route<dynamic> route) => false),

                  child: Text('OK',
                      style: TextStyle(
                          fontFamily: 'Cairo',
                          color: ColorManager.primary,
                          fontSize: 16)),
                ),
              ],
            ));
  }
}
