import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:meerstore/core/constant/color_manger.dart';

import '../../view/screen/category/category.dart';
import '../../view/screen/favorite/favourit_iem.dart';
import '../../view/screen/home/home_page.dart';
import '../../view/screen/home/home_page_updata.dart';
import '../../view/screen/profail/profail.dart';

class BottomNav extends StatefulWidget {
    int id;
   BottomNav({key, required this.id}) : super(key: key);
  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {


   late  int _selectedPageIndex;
  bool page=false;
  late int?  idroute;
   late int idroute1;
  List<Map<String, Object>> _pages = [
    {'page': HomePageUpdata(), 'title': 'Home'},
    // {'page':HomePage(),'title':'Home'},
    {'page': FavouritItem(), 'title': 'favorite'},
    {'page': Categories(), 'title': 'category'},
    {'page': Profail(), 'title': 'profail'},
  ];


  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    _selectedPageIndex= widget.id;
  }
  // final _selectedPageIndex = 0 ;
  void _selectedPage(int index) {

    setState(() {

      _selectedPageIndex = index;

    });
  }
  // _pages[_selectedPageIndex]['page'] as Widget,

  @override
  Widget build(BuildContext context) {



    return Scaffold(
      body:_pages[_selectedPageIndex]['page'] as Widget,
      bottomNavigationBar: SizedBox(
        height: 80,
        child: BottomNavigationBar(
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: true,
          onTap: _selectedPage,
          unselectedItemColor: Colors.grey,
          selectedItemColor: ColorManager.grey1,
          currentIndex:  _selectedPageIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                size: 25.0.sp,
              ),
              label: 'الرئيسية',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.favorite,
                size: 25.0.sp,
              ),
              label: 'المفضلة',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.category,
                size: 25.0.sp,
              ),
              label: 'التصنيفات',
            ),

            BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
                size: 25.0.sp,
              ),
              label: 'حسابي',
            ),
            // BottomNavigationBarItem(
            //   icon: Icon(Icons.category,
            //     size: 25.0.sp,
            //   ),
            //   label: 'التصنيفات',
            // ),
          ],
          selectedLabelStyle: TextStyle(fontFamily: 'Cairo'),
        ),
      ),
    );
  }
}
/*class BottomNav extends StatelessWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int _selectedPageIndex = 0;
    List<Map<String,Object>> _pages =[
      {'page':HomePage(),'title':'Home'},
      {'page':FavouritItem(),'title':'favorite'},
      {'page':Categories(),'title':'category'},
      {'page':Profail(),'title':'profail'},
      {'page':HomePageUpdata(),'title':'profail'}
    ];
    // final _selectedPageIndex = 0 ;
    void _selectedPage(int index){
      setState(() {
        _selectedPageIndex = index;
      });
    }
    return Scaffold(
      body: _pages[_selectedPageIndex]['page'] as Widget,
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: true,
        onTap: _selectedPage,

        unselectedItemColor: Colors.grey,
        selectedItemColor: Color.fromARGB(255, 35, 41, 121),
        currentIndex: _selectedPageIndex,
        items:[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              size: 25.0.sp,

            ),
            label: 'الرئيسية',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite,
              size: 25.0.sp,
            ),
            label: 'المفضلة',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category,
              size: 25.0.sp,
            ),
            label: 'التصنيفات',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.person,
              size: 25.0.sp,
            ),
            label: 'حسابي',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category,
              size: 25.0.sp,
            ),
            label: 'التصنيفات',
          ),
        ],
        selectedLabelStyle: TextStyle(fontFamily: 'Cairo'),
      ),
    );
  }
}*/


