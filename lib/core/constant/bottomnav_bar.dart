
import 'dart:async';

import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:meerstore/core/constant/color_manger.dart';

import '../../view/screen/home/home_page_updata.dart';
import 'bottom_bar.dart';


class BottomNavBa extends StatefulWidget {
  final int pageIndex;

  const BottomNavBa({key, required this.pageIndex}) : super(key: key);

  @override
  _BottomNavBaState createState() => _BottomNavBaState();
}

class _BottomNavBaState extends State<BottomNavBa> {
int _selectedPageIndex = 0;
  // final _selectedPageIndex = 0 ;


  @override
  Widget build(BuildContext context) {
    return
      SizedBox(
        height: 70,
        child: BottomNavigationBar(
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          // showUnselectedLabels: true,


          onTap: (int index) {
            setState(() {
              _selectedPageIndex = index;
            });
                switch (index) {
                  case 0:
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) =>  BottomNav(id:0)),
                         );

                    break;
                  case 1:

                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) =>  BottomNav(id:1)),
                    );
                    break;
                  case 2:

                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) =>  BottomNav(id:2)),
                    );
                    break;
                  case 3:

                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) =>  BottomNav(id:3)),
                    );
                    break;

                }
          },
          selectedIconTheme: const IconThemeData(color: Color(0xFF4c4f7a)),
          unselectedItemColor: Color.fromRGBO(49, 49, 49, 1),
          selectedItemColor:
          _selectedPageIndex != -1 ? Colors.black : Colors.grey,

          currentIndex: _selectedPageIndex == -1 ? 0 : _selectedPageIndex,

          // selectedItemColor: Colors.black ,
          // unselectedItemColor: Colors.grey,
          // selectedItemColor: ColorManager.grey1,
          // currentIndex: _selectedPageIndex,


          items:

          <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                size: 25.0.sp,
                  color: widget.pageIndex == 0 ? ColorManager.grey1: Colors.grey
              ),
              label: 'الرئيسية',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite,
                  size: 25.0.sp,
                   color: widget.pageIndex == 1 ?ColorManager.grey1: Colors.grey
              ),
              label: 'المفضلة',

            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.category,
                  size: 20.0.sp,
                    color: widget.pageIndex == 2 ? ColorManager.grey1 : Colors
                 .grey
              ),
              label: 'التصنيفات',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person,
                  size: 25.0.sp,
                  color: widget.pageIndex == 3 ? ColorManager.grey1 : Colors
                      .grey
              ),
              label: 'حسابي',
            ),

          ],
          selectedLabelStyle: TextStyle(fontFamily: 'Cairo'),


        ),
      );
  }
}


