
import 'package:flutter/cupertino.dart';
import 'package:meerstore/main.dart';
void changeLanguage( BuildContext context, Locale locale) async {

  Locale currentLocale = Localizations.localeOf(context);


  if(currentLocale.languageCode == 'ar'){

    Locale _locale = await Locale('en');


    MyApp.setLocale(context, _locale);

  }else{
    Locale locale = await Locale('ar');
    MyApp.setLocale(context, locale);

  }}