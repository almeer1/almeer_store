import 'package:flutter/material.dart';

//project colors
class ColorManager {
  static Color primary = const Color(0xff72cc50);
  static Color lightPrimary = const Color(0xffFEF85E);
  static Color darkPrimary = const Color(0xffE5DE03);
  static Color grey1 = const Color(0xff525b88);
  static Color darkGrey = const Color(0xff525252);
  static Color grey = const Color(0xff737474);
  static Color lightGrey = const Color(0xff9E9E9E);
  static Color lightGrey1 = const Color(0xfff6f6f6);
  static Color white = const Color(0xffFFFFFF);
  static Color error = const Color(0xffFF3D00);
  static Color black = const Color(0xff000000);
}
