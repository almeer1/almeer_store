import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/string_manger.dart';
import 'package:meerstore/view/screen/Splash/splash.dart';
import 'package:meerstore/view/screen/auth/forget_passwowrd.dart';
import 'package:meerstore/view/screen/auth/login.dart';
import 'package:meerstore/view/screen/auth/signup.dart';
import 'package:meerstore/view/screen/details/details_page.dart';
//import 'package:meerstore/view/screen/details/product_details.dart';
import 'package:meerstore/view/screen/home/home_page.dart';

class Routes {
  static const String splashRoute = "/";
  static const String loginRoute = "/login";
  static const String signupRoute = "/signup";
  static const String forgotPasswordRoute = "/forgotPassword";
  static const String homeRoute = "/home";
  static const String productDetailsRoute = "/productDetails";

}

  class RouteGenerator {
  static Route<dynamic> getRoute(RouteSettings settings) {
  switch (settings.name) {
  case Routes.splashRoute:
  return MaterialPageRoute(builder: (_) => const Splash());
  case Routes.loginRoute:
  return MaterialPageRoute(builder: (_) => const Login());
  case Routes.signupRoute:
  return MaterialPageRoute(builder: (_) => const SignUp());
  case Routes.forgotPasswordRoute:
  return MaterialPageRoute(builder: (_) => const ForgetPass());
  case Routes.homeRoute:
 return MaterialPageRoute(builder: (_) =>  HomePage());
/*
  case Routes.productDetailsRoute:
  return MaterialPageRoute(builder: (_) => const DetailsPage());
*/
  default:
  return unDefinedRoute();
  }
  }

  static Route<dynamic> unDefinedRoute() {
  return MaterialPageRoute(
  builder: (_) => Scaffold(
  appBar: AppBar(
  title: const Text(
      AppStrings.noRouteFound),
  ),
  body: const Center(
  child: Text(
      AppStrings.noRouteFound)),
  ));
  }
}