class AppStrings{
  static const  noRouteFound="No Route Found";
  static const  signUpBigText="إنشاء حساب ";
  static const  loginBigText="هلا! أهلا بعودتك";
  static const  forgetPassword="هل نسيت كلمة السر؟";
  static const  forgetPasswordText="ادخل البريد الالكتروني الخاص بك وسوف نرسل لك رابطاً لإعادة تعيين كلمة المرور الخاصة بك";
  static const  emailHint="يرجى إدخال البريد الإلكتروني";
  static const  newPassword="إنشاء كلمة مرور جديدة";
  static const  newPasswotrdTex="كلمة مرورك  يجب انت تكون مختلفة عن كلمات المرور السابقة";
  static const  passwordhint ="يرجى إدخال كلمة المرور الجديدة";
  static const  confarmPassword="يرجى تأكيد كلمة المرور";
  static const  checkEmail="تأكد من إيميلك";
  static const  sentPassword="أرسلنا تأكيد كلمةالمرور الى إيميلك ";
  static const  sentPasswordText="لم تستلم الأيميل؟  ";
  static const  sentPasswordtext='افحص جودة الأنترنت أو جرب بريد الكتروني آخر';
}
