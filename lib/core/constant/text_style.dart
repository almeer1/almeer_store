import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/font_manger.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

TextStyle _getTextStyle(double fontSize, FontWeight fontWeight, Color color,) {
  return TextStyle(

      fontSize: fontSize,
      fontFamily: FontConstants.fontFamily,
      color: color,
      fontWeight: fontWeight,
  );
}

// regular style

TextStyle getRegularStyle(
    {double fontSize = FontSize.s12, required Color color}) {
  return _getTextStyle(fontSize.sp, FontWeightManager.regular, color);
}

// medium style

TextStyle getMediumStyle(
    {double fontSize = FontSize.s16, required Color color}) {
  return _getTextStyle(fontSize.sp, FontWeightManager.medium, color);
}
TextStyle getMediumStylle(
    {double fontSize = FontSize.s18, required Color color}) {
  return _getTextStyle(fontSize.sp, FontWeightManager.medium, color);
}
// medium style

TextStyle getLightStyle(
    {double fontSize = FontSize.s12, required Color color}) {
  return _getTextStyle(fontSize.sp, FontWeightManager.light, color);
}

// bold style
TextStyle getBoldtStyle(
{double fontSize = FontSize.s25, required Color color
}) {
  return _getTextStyle(fontSize.sp, FontWeightManager.bold, color);
}
TextStyle getBoldtSmallStyle(
    {double fontSize = FontSize.s12, required Color color
    }) {
  return _getTextStyle(fontSize.sp, FontWeightManager.bold, color);
}
TextStyle getBoldStyle(
    {double fontSize = FontSize.s16, required Color color
    }) {
  return _getTextStyle(fontSize.sp, FontWeightManager.bold, color);
}


// semibold style

TextStyle getSemiBoldStyle(
    {double fontSize = FontSize.s12, required Color color}) {
  return _getTextStyle(fontSize, FontWeightManager.semiBold, color);
}