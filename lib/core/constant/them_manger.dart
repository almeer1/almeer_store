import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/font_manger.dart';
import 'package:meerstore/core/constant/text_style.dart';
import 'package:meerstore/core/constant/value_manger.dart';

ThemeData getApplicationTheme() {
  return ThemeData(
    // main colors
    primaryColor: ColorManager.primary,
    primaryColorLight: ColorManager.lightPrimary,
    primaryColorDark: ColorManager.darkPrimary,
    disabledColor: ColorManager.grey1,
    splashColor: ColorManager.lightPrimary,



    // elevated button them
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
            textStyle: getRegularStyle(
                color: ColorManager.white, fontSize: FontSize.s17), backgroundColor: ColorManager.primary,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(AppSize.s12)))),


    // input decoration theme (text form field)
    inputDecorationTheme: InputDecorationTheme(

      // content padding
        contentPadding: const EdgeInsets.all(AppPadding.p8),
        // hint style
        hintStyle:
        TextStyle(color: ColorManager.grey, fontSize: FontSize.s14,fontFamily: FontConstants.fontFamily,
        ),
        labelStyle:
        TextStyle(color: ColorManager.grey, fontSize: FontSize.s14,),
        errorStyle: getRegularStyle(color: ColorManager.error),


        // focused border style
       /* focusedBorder: OutlineInputBorder(
            borderSide:
            BorderSide(color: ColorManager.grey, width: AppSize.s1_5),
            borderRadius: const BorderRadius.all(Radius.circular(AppSize.s8))),*/

        // error border style
        errorBorder: OutlineInputBorder(
            borderSide:
            BorderSide(color: ColorManager.error, width: AppSize.s1_5),
            borderRadius: const BorderRadius.all(Radius.circular(AppSize.s8))),

        focusedErrorBorder: OutlineInputBorder(
            borderSide:
            BorderSide(color: ColorManager.primary, width: AppSize.s1_5),
            borderRadius: const BorderRadius.all(Radius.circular(AppSize.s8)))),

  );


}