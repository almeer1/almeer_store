import 'package:geocoding/geocoding.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;


class LocationService{
  final String key ='AIzaSyBHhDGZFVS8rePvWEeu8RKlgMos_Lu5eVY';

 Future<String> getPlaceaId(String input) async {
   final String url='https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=$input&inputtype=textquer&key=$key';
   var reesponse = await http.get(Uri.parse(url));
   var json = convert.jsonDecode(reesponse.body);
   var placeId = json['candidates'][0]['place_id']  as String;
   return placeId;


 }
Future <Map<String,dynamic>> getPlace(String input) async {
   final placeId =  await getPlaceaId(input);
   final String url ='https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&key=$key';
   var reesponse = await http.get(Uri.parse(url));
   var json = convert.jsonDecode(reesponse.body);
   var result = json['result'] as Map<String, dynamic>;
   print(result);
   return result;

}


}
