/*
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:geolocator/geolocator.dart';

import 'location-srevices.dart';





class MapPage extends StatefulWidget {
  @override
  State<MapPage> createState() => MapPageState();
}

class MapPageState extends State<MapPage> {
  LocationService locationService= new LocationService();

  var cl;
  var latitude;
  var longitude;

  String address ='';

  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.





  //import dart sync
  final Completer<Googler> _controller = Completer();
  TextEditingController _searchcontroller=TextEditingController();
  //The place where the camera will open
 late CameraPosition _kGooglePlex;

  // marker position
  late Marker _keyGooglePlexMarker;

  static final CameraPosition _kLake = CameraPosition (
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);


  //
  // If the location is working or not
  Future getPostion() async {
    bool services;
    LocationPermission per;
    services = await Geolocator.isLocationServiceEnabled();
    if(services == false){
      AwesomeDialog(context: context,title: 'services',body: Text('services not enable'))..show();
    }
    per = await Geolocator.checkPermission();
    //The application does not have access to the site
    if(per == LocationPermission.denied){
      per = await Geolocator.requestPermission();
      if( per == LocationPermission.always){
        //getPostion
        getLatAndLong();
        getAddress(latitude, longitude);
      }
    }
    print("=========================");
    print(per);
    print("=====================");
    return per;
  }
 Future<void> getLatAndLong() async {
    cl = await Geolocator.getCurrentPosition().then((value) => value);
    print(cl);
    latitude = cl.latitude;
    longitude = cl.longitude;
    _kGooglePlex = CameraPosition(
      target: LatLng(latitude,longitude),
        zoom: 15.4746,
   );
    _keyGooglePlexMarker =  Marker(markerId:MarkerId('_keyGooglePlex'),
      infoWindow: InfoWindow(title: 'GooglePlex'),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      draggable: true,
      position: LatLng(latitude, longitude),
    );

    setState(() {
    });

  }

  getAddress(latitude,longitude) async {
    List<Placemark> placemarker = await placemarkFromCoordinates(latitude, longitude);
    setState(() {
      address = placemarker[0].street! + " " + placemarker[0].country!;
    });
    for(int i =0; i< placemarker.length; i++){
      print('INDEX $i ${placemarker[i]}');
    }
  }


  @override
  void initState() {
    getPostion();
    getLatAndLong();

    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      // body: GoogleMap(
      //     initialCameraPosition  : _kGooglePlex,
      //   mapType: MapType.normal,
      //   onMapCreated: (GoogleMapController controller){
      //       _controller.complete(controller);
      //   },
      //
      // )


    body:    Column(
         children: [
           Padding(
             padding: const EdgeInsets.only(top:30),
             child: Row(
               children: [
                 Expanded(child:TextFormField(
                   controller: _searchcontroller,
                   textCapitalization: TextCapitalization.words,
                   decoration: InputDecoration(hintText: 'Search by city'),
                   onChanged: (value){
                     print(value);
                   },
                 ) ),
                 IconButton(onPressed: (){
                   locationService.getPlace(_searchcontroller.text);
                 }, icon: Icon(Icons.search)),
               ],
             ),
           ),
           Expanded(
             child:_kGooglePlex == ''? CircularProgressIndicator(): GoogleMap(

               mapType: MapType.normal,
               markers: {_keyGooglePlexMarker},
               initialCameraPosition: _kGooglePlex,
               onMapCreated: (GoogleMapController controller) {
                 _controller.complete(controller);
               },

             ),
           ),
         ],
       ), floatingActionButton: FloatingActionButton.extended(
        onPressed: _goToTheLake,
        label: Text('To the lake!'),
        icon: Icon(Icons.directions_boat),
    ),



    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }
// get adress user



}

*//*



*/
/*
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';

import 'dart:async';

class MapPage extends StatefulWidget {
  const MapPage({Key? key}) : super(key: key);

  @override
  State<MapPage> createState() => _MapPageState();
}


class _MapPageState extends State<MapPage> {

  String? _currentAddress;
  Position? _currentPosition;

//create a method to check and request the user’s permission.
  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();
    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() => _currentPosition = position);
      _getAddressFromLatLng(_currentPosition!);
    }).catchError((e) {
     print(e);
    });
  }

  // Create a _getAddressFromLatLng() method that pass Position as a parameter,
  // call the placemarkFromCoordinates() method, and set the _currentAddress with
  // the Placemark information,
  // such as street, locality, postalCode, country, and more.

  Future<void> _getAddressFromLatLng(Position position) async {
    await placemarkFromCoordinates(
        _currentPosition!.latitude, _currentPosition!.longitude)
        .then((List<Placemark> placemarks) {
      Placemark place = placemarks[0];
      setState(() {
        _currentAddress =
        '${place.street}, ${place.subLocality},${place.subAdministrativeArea}, ${place.postalCode}';
      });
    }).catchError((e) {
      debugPrint(e);
    });
  }


@override
  void initState() {
  _handleLocationPermission();
  _getCurrentPosition();

    //
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Location Page")),
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('LAT: ${_currentPosition?.latitude ?? ""}'),
              Text('LNG: ${_currentPosition?.longitude ?? ""}'),
              Text('ADDRESS: ${_currentAddress ?? ""}'),
              const SizedBox(height: 32),
              ElevatedButton(
                onPressed: _getCurrentPosition,
                child: const Text("Get Current Location"),
              )
            ],
          ),
        ),
      ),
    );
  }
}

*//*

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocationPage extends StatefulWidget {
  const LocationPage({Key? key}) : super(key: key);

  @override
  State<LocationPage> createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  String? _currentAddress;
  double lat = 37.42796133580664;
  double long = -122.085749655962;

  late CameraPosition _kGooglePlex;

  final Completer<GoogleMapController> _controller = Completer();

  //create a method to check and request the user’s permission.
  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();

    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }


//we create _getCurrentLocation() method which calls the _handleLocationPermission() method to
// check whether the permission is granted or not. If yes, we create an instance of geolocator
// and make a call to the getCurrentPosition() method and
// get the current location as a Position.
  Future<void> _getCurrentPosition() async {

    final hasPermission = await _handleLocationPermission();

    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {

      setState(()
      {

        lat = position.latitude;
        long = position.longitude;
      });
      _getAddressFromLatLng(lat,long);
    }).catchError((e) {
      print(e.toString());
    });
  }

  //Create a _getAddressFromLatLng() method that pass Position as a parameter,
  // call the placemarkFromCoordinates() method, and set the _currentAddress with the
  // Placemark information, such as street, locality,
  // postalCode, country, and more.

  Future<void> _getAddressFromLatLng(lat,long  ) async {
    await placemarkFromCoordinates(lat, long, )
        .then((List<Placemark> placemarks) {
      Placemark place = placemarks[0];
      setState(() {
        _currentAddress =
        ' ${place.subAdministrativeArea},';
      });
    }).catchError((e) {
      debugPrint(e.toString());
    });
  }
  @override
  void initState() {


    _kGooglePlex = CameraPosition(
      target: LatLng(lat,long),
      zoom: 14.4746,
    );
     _getCurrentPosition();
    //
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Location Page")),
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(child:GoogleMap(
                mapType: MapType.normal,
                // initialCameraPosition:  CameraPosition(target: _currentPosition?.longitude as LatLng ,) ,
                initialCameraPosition: _kGooglePlex,
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                },

              )),
              // Text('LAT: ${_currentPosition?.latitude ?? ""}'),
              // Text('LNG: ${_currentPosition?.longitude ?? ""}'),
              // Text('ADDRESS: ${_currentAddress ?? ""}'),
              const SizedBox(height: 32),
              ElevatedButton(
                onPressed:(){_getCurrentPosition();
                  print(_currentAddress);} ,
                child: const Text("Get Current Location"),
              )
            ],
          ),
        ),
      ),
    );
  }
}

//
// String location ='Null, Press Button';
// String Address = 'search';

//The below method will receive parameter i.e. location position by using which we can convert lat long to address.
// //
// Future<void> GetAddressFromLatLong(Position position)async {
//   List<Placemark> placemarks = await placemarkFromCoordinates(position.latitude, position.longitude);
//   print(placemarks);
//   Placemark place = placemarks[0];
//   Address = '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
//
// }
//  Widget build(BuildContext context) {
//     return Scaffold(
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             Text('Coordinates Points',style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),),
//             SizedBox(height: 10,),
//             Text(location,style: TextStyle(color: Colors.black,fontSize: 16),),
//             SizedBox(height: 10,),
//             Text('ADDRESS',style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),),
//             SizedBox(height: 10,),
//             Text('${Address}'),
//             ElevatedButton(onPressed: () async{
//               Position position = await _getGeoLocationPosition();
//               location ='Lat: ${position.latitude} , Long: ${position.longitude}';
//               GetAddressFromLatLong(position);
//             }, child: Text('Get Location'))
//           ],
//         ),
//       ),
//     );
//   }




*/
