import 'dart:io';

import 'package:meerstore/data/model/cart_it.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static const _databaseName = "cart.db";
  static const _databaseVersion = 1;

  static const table = 'cart';
  static const tablef = 'favorite';
  int globalCartNumber = 0;

  static const columnId = 'id';
  static const columnProductId = 'p_id';
  static const columnProductName = 'productName';
  static const columnImage = 'image';
  static const columnPrice = 'price';
  static const columnAmount = 'Amount';
  static const columnMinAmount = 'MinAmount';
  static const columnQuantety = 'Quantety';
  static const columnDescription = 'description';
  static const columnFavorite = 'favorite';
  static const columnColor = 'color';

  void updateCartCounter(int count) {
    globalCartNumber = count;
  }

  static Database? _db;
  Future<Database?> get db async {
    if (_db == null) {
      _db = await init();
      return _db!;
    } else {
      return _db;
    }
  }

  // this opens the database (and creates it if it doesn't exist)
  init() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, _databaseName);
    _db = await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) {
    if (oldVersion < newVersion) {
      db.execute("ALTER TABLE tabEmployee ADD COLUMN newCol TEXT;");
    }
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute(
        '''
          CREATE TABLE $table (
            $columnId INTEGER PRIMARY KEY,
            $columnProductId TEXT NOT NULL,
            $columnProductName  TEXT NOT NULL,
            $columnImage  TEXT NOT NULL,
            $columnPrice TEXT NOT NULL,
            $columnAmount TEXT NOT NULL,
            $columnMinAmount TEXT NOT NULL,
            $columnQuantety TEXT NOT NULL
          )
          ''');
    await db.execute(
        '''
          CREATE TABLE $tablef (
            $columnId INTEGER PRIMARY KEY,
            $columnProductId TEXT NOT NULL,
            $columnProductName  TEXT NOT NULL,
            $columnImage  TEXT NOT NULL,
            $columnPrice TEXT NOT NULL,
            $columnAmount TEXT NOT NULL,
            $columnDescription TEXT NOT NULL,
            $columnFavorite BOOL NOT NULL,
            $columnColor TEXT NOT NULL
          )
          ''');

    print('+++++++++++++++++++++${db.execute}');
  }

  // Helper methods

  // Inserts a row in the database where each key in the Map is a column name
  // and the value is the column value. The return value is the id of the
  // inserted row.
  Future<int> insert(Map<String, dynamic> row) async {
    return await _db!.insert(table, row);
  }

  // All of the rows are returned as a list of maps, where each map is
  // a key-value list of columns.
  Future<List<Map<String, dynamic>>> queryAllRows() async {
    return await _db!.query(table);
  }

  // All of the methods (insert, query, update, delete) can also be done using
  // raw SQL commands. This method uses a raw query to give the row count.
  Future<int?> queryRowCount() async {
    final results = Sqflite.firstIntValue(
        await _db!.rawQuery('SELECT COUNT(*) FROM $table'));
    return results;
  }

  //Delet all row from table
  deleteAll() async {
    // Database db = await .database;
    return await _db!.rawDelete("Delete from $table");
  }

  deleteAllf() async {
    // Database db = await .database;
    return await _db!.rawDelete("Delete from $tablef");
  }

  // We are assuming here that the id column in the map is set. The other
  // column values will be used to update the row.
  Future<int> update(Map<String, dynamic> row) async {
    int id = row[columnId];

    return await _db!.update(
      table,
      row,
      where: '$columnId = ?',
      whereArgs: [id],
    );
  }

  // Deletes the row specified by the id. The number of affected rows is
  // returned. This should be 1 as long as the row exists.
  Future<int> delete(int id) async {
    return await _db!.delete(
      table,
      where: '$columnId = ?',
      whereArgs: [id],
    );
  }

  //delete database table
  Future<bool> mydeleteDatabase() async {
    bool databaseDeleted = false;
    // String databasepath = await getDatabasesPath();
    // String path = join(databasepath,_databaseName);
    try {
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      String path = join(documentsDirectory.path, _databaseName);
      await deleteDatabase(path).whenComplete(() {
        databaseDeleted = true;
      }).catchError((onError) {
        databaseDeleted = false;
      });
    } on DatabaseException catch (error) {
      print(error);
    } catch (error) {
      print(error);
    }

    return databaseDeleted;

    print('delet');
  }

  // Inserts a row in the database where each key in the Map is a column name
  // and the value is the column value. The return value is the id of the
  // inserted row.
  Future<int> insertF(Map<String, dynamic> row) async {
    return await _db!.insert(tablef, row);
  }

  // All of the rows are returned as a list of maps, where each map is
  // a key-value list of columns.
  Future<List<Map<String, dynamic>>> queryAllRowsf() async {
    return await _db!.query(tablef);
  }

  // We are assuming here that the id column in the map is set. The other
  // column values will be used to update the row.
  Future<int> updatef(Map<String, dynamic> row) async {
    int id = row[columnId];
    return await _db!.update(
      tablef,
      row,
      where: '$columnId = ?',
      whereArgs: [id],
    );
  }

  // Deletes the row specified by the id. The number of affected rows is
  // returned. This should be 1 as long as the row exists.
  Future<int> deletef(int id) async {
    return await _db!.delete(
      tablef,
      where: '$columnId = ?',
      whereArgs: [id],
    );
  }

  checkFavorit(int id) async {
    var fav = await _db!.rawQuery('SELECT * FROM favorite WHERE p_id=$id');

    //
    //   (
    //   tablef,
    //   where: '$columnId = ?',
    //   whereArgs: [id],
    // );
  }
}
