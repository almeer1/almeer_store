import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:http/http.dart' as http;
import 'package:meerstore/data/model/prduct_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'endpoints_api.dart';

class ApiController {
  Dio dio = Dio();

  // fetch all product from api
  Future getAllProduct() async {
    try {
      DioCacheManager dioCacheManager = DioCacheManager(CacheConfig());
      Options myoptions = buildCacheOptions(
        const Duration(days: 30),
        forceRefresh: true,
      );
      dio.options.connectTimeout = 20000;
      dio.options.baseUrl = Endpoints.baseUrl;

      dio.interceptors.add(dioCacheManager.interceptor);
      final response = await dio.get(Endpoints.products, options: myoptions);
      return response;
    } catch (e) {
      print(e);
    }
  }

  // String url = 'https://mapi.orasys.org/public/api/products/all?page=1';
  // fetch all product from api
  Future fulttergetAllProduct() async {
    String url = 'https://mapi.orasys.org/api/products/all?page=1';
    http.Response res = await http.get(Uri.parse(url));

    final jsonData = json.decode(res.body);
    var map = Map<String, dynamic>.from(jsonData);
    var response = productPag.fromJson(map);
    print(response.data!.length);

    if (res.statusCode == 200) {
      return response;
    } else {
      throw Exception('Failed to load post');
    }
  }

//  fetch all category from api
  Future getCategory() async {
    try {
      DioCacheManager dioCacheManager = DioCacheManager(CacheConfig());
      Options myOption =
          buildCacheOptions(const Duration(days: 30), forceRefresh: true);
      dio.options.baseUrl = Endpoints.baseUrl;
      dio.interceptors.add(dioCacheManager.interceptor);
      final response = await dio.get(Endpoints.categories, options: myOption);
      return response;
    } catch (e) {
      print(e);
    }
  }

  // 'https://mapi.orasys.org/public/api/products/categoryProducts/$categoryId',
  //fetch gategoryproduct from Api
  Future getCategoryProduct({required String categoryId}) async {
    try {
      DioCacheManager dioCacheManager = DioCacheManager(CacheConfig());
      Options myOptions =
          buildCacheOptions(const Duration(days: 30), forceRefresh: true);
      dio.interceptors.add(dioCacheManager.interceptor);
      dio.options.baseUrl = Endpoints.baseUrl;
      // print(categoryId);
      final response = await dio.get(
          'https://mapi.orasys.xyz/api/products/categoryProducts/$categoryId',
          options: myOptions);
      return response;
    } catch (e) {
      print(e);
    }
  }

  // get product details
  Future getdetailbyid({required String productId}) async {
    try {
      DioCacheManager dioCacheManager = DioCacheManager(CacheConfig());
      Options myOptions =
          buildCacheOptions(const Duration(days: 30), forceRefresh: true);
      dio.interceptors.add(dioCacheManager.interceptor);
      dio.options.baseUrl = Endpoints.baseUrl;
      final response = await dio.get(Endpoints.detailsProducts + '/$productId',
          options: myOptions);

      return response;
    } catch (e) {
      print(e);
    }
  }

// search product
  Future searchAllProduct() async {
    try {
      final productAr = '';

      DioCacheManager dioCacheManager = DioCacheManager(CacheConfig());
      Options myOptions =
          buildCacheOptions(const Duration(days: 30), forceRefresh: true);
      dio.interceptors.add(dioCacheManager.interceptor);
      dio.options.baseUrl = Endpoints.baseUrl;
      final response = await dio.get(Endpoints.searchProducts,
          queryParameters: {'searchText': productAr}, options: myOptions);
    } catch (e) {
      print(e);
    }
  }

  Future getSplashImage() async {
    String? base64;
    String Url = 'https://mapi.orasys.org/images/logo-h.png';
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    base64 = prefs.getString("base64SplashImage");
    if (base64 == null) {
      final http.Response response = await http.get(Uri.parse(Url));
      base64 = base64Encode(response.bodyBytes);
      prefs.setString("base64SplashImage", base64);
    }
    return base64;
  }

  Future getImage() async {
    String? base64;
    String Url2 = 'https://mapi.orasys.org/images/logo.png';
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    base64 = prefs.getString("base64Image");
    if (base64 == null) {
      final http.Response response = await http.get(Uri.parse(Url2));
      base64 = base64Encode(response.bodyBytes);
      prefs.setString("base64Image", base64);
    }
    return base64;
  }
}
