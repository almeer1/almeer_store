class Endpoints {
  // static const String baseUrl = 'https://mapi.orasys.org/public/api';
  static const String baseUrll = 'https://mapi.orasys.org/api';
  static const String baseUrl = 'https://mapi.orasys.xyz/api';

  static const String products = '/products';
  static const String categories = '/categories';
  static const String categoryProducts = 'products/categoryProducts';
  static const String detailsProducts = '/products';
  static const String searchProducts = '/products/searchProducts';
  static const String filterProducts =
      'https://jsonplaceholder.typicode.com/posts?_start=startIndex&_limit=limit';
}
// 'https://jsonplaceholder.typicode.com/posts?_start=$startIndex&_limit=$limit';