import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../model/cart_it.dart';

class User_controller {
  bool g_login_status = true;
  Dio dio = Dio();

  // String myUrl = "https://mapi.orasys.org/public/api/login";
// UserLogin
  Future<bool> user_login(String userName, String password) async {
    String myUrl = "https://mapi.orasys.xyz/api/login";
    http.Response response = await http.post(Uri.parse(myUrl), body: {
      'userName': userName,
      'password': password,
    });
    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      SharedPreferences _storage = await SharedPreferences.getInstance();
      await _storage.setString('token', data['token']);
      await _storage.setString('firstName', data['user']['firstName']);
      await _storage.setString('lastName', data['user']['lastName']);
      await _storage.setString('username', userName);
      await _storage.setString('mobile', data['user']['mobile']);
      await _storage.setString('address', data['user']['address']);
      await _storage.setInt('accountNm', data['user']['accountNumber']);

      return true;
    } else {
      print('in');
      return false;
    }
  }

  // String myUrl = "https://mapi.orasys.org/public/api/register";
  // UserRegister
  Future<dynamic> user_register(String userName, String mobile, String password,
      String firstName, String lastName) async {
    String myUrl = "https://mapi.orasys.xyz/api/register";
    http.Response response = await http.post(
      Uri.parse(myUrl),
      body: {
        'userName': userName,
        'mobile': mobile,
        'password': password,
        'firstName': firstName,
        'lastName': lastName,
        'type': 'user',
        'address': 'tartus',
      },
      // headers: <String, String>{
      //   'Content-Type': 'application/json; charset=UTF-8',
      // },
    );

    // var data = (json.decode(utf8.decode(response.bodyBytes)));
//  &&
//         data['message'] != 'اسم المستخدم معرف مسبقا'
    // print(response.body);
    var data = json.decode(response.body);
    print(data['message']);
    if (response.statusCode == 200 &&
        data['message'] != 'اسم المستخدم معرف مسبقا') {
      // var data = (json.decode(utf8.decode(response.bodyBytes)));
      // print(response.body);
      SharedPreferences _storage = await SharedPreferences.getInstance();
      await _storage.setString('token', data['token']);
      await _storage.setString('username', userName);
      await _storage.setString('password', password);
      await _storage.setString('mobile', mobile);
      await _storage.setString('firstName', firstName);
      await _storage.setString('lastName', lastName);
      await _storage.setString('address', data['user']['address']);
      await _storage.setInt('accountNm', data['user']['accountNumber']);
      // await _storage.setString('accountNumber', data['user']['accountNumber']);
    }
    return data;
  }

// check if user login or not
  Future<bool> check_login() async {
    final prefs = await SharedPreferences.getInstance();
    const key = 'token';
    final is_login_value = prefs.get(key) ?? 0;

    if (is_login_value == 1) {
      return true;
    } else {
      return false;
    }
  }

// send order
  Future<dynamic> user_Order(String mobile, String address, int accountNumber,
      DateTime time, List<cartIt> cartit) async {
    // try{
    String lines = "[";
    for (int i = 0; i < cartit.length; i++) {
      String temp = '';
      if (i == 0) {
        temp = "{";
      } else {
        temp = ", {";
      }

      temp +=
          'productId: ${cartit[i].productId}, amount: ${cartit[i].amount}, price: ${cartit[i].price}';
      temp += "}";

      lines += temp;
    }
    lines += "]";

    var Lines = cartit.map((e) {
      return {"productId": e.productId, "amount": e.amount, "price": e.price};
    }).toList();

    var dataS = json.encode({
      "accountNumber": "$accountNumber",
      "date": "$time",
      "address": "$address",
      "mobile": "$mobile",
      "delivery": "1",
      "lines": Lines
    });
    print(dataS);

    // Map data = dataS;
    //print(data);
    // var data2 = {
    //   "accountNumber": "12",
    //   "date": "2022-12-12",
    //   "address": "tartus",
    //   "mobile": "0992963927",
    //   "delivery": "1",
    //   "lines": [
    //     {
    //       "productId": 3,
    //       "amount": 1,
    //       "price": 1200
    //     }
    //
    //   ]
    // };
    //print(data2);
    //  String myUrl = "https://mapi.orasys.org/public/api/orders";
    String myUrl = "https://mapi.orasys.xyz/api/orders";
    Response response = await dio.post(myUrl,
        options: Options(
            headers: {HttpHeaders.contentTypeHeader: "application/json"}),
        data: dataS);
    // print('pro:' +response.data.toString());
    if (response.statusCode == 200) {
      var body = response.data;
      // print(body);
      return body;
    } else {
      return false;
    }
    // }catch(e){
    //  print(e);
    // }
  }

  Future<dynamic> user_order(String mobile, String address, int accountNumber,
      DateTime time, List<cartIt> cartit) async {
    String myUrl = "https://mapi.orasys.org/public/api/orders";
    String lines = "[";
    for (int i = 0; i < cartit.length; i++) {
      String temp = '';
      if (i == 0) {
        temp = "{";
      } else {
        temp = ", {";
      }

      temp +=
          'productId: ${cartit[i].productId}, amount: ${cartit[i].amount}, price: ${cartit[i].price}';
      temp += "}";

      lines += temp;
    }
    lines += "]";

    http.Response response = await http.post(Uri.parse(myUrl), body: {
      "accountNumber": "$accountNumber",
      "date": "$time",
      "address": "$address",
      "mobile": "$mobile",
      "delivery": "1",
      "lines": lines
    });

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print(data);
      return data;
    }
  }
}



































/*
import 'dart:convert';

import 'package:meerstore/data/data_source/login_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;


import '../../view/screen/auth/login.dart';
import '../model/user_model.dart';

class LoginService extends Ilogin {
  @override
  Future<UserModel?> login(String email, String password) async {
    final api = Uri.parse('https://reqres.in/api/login');
    final data = {"email": email, "password": password};
    // final dio = Dio();
    http.Response response;
    response = await http.post(api, body: data);
    if (response.statusCode == 200) {
      SharedPreferences storage = await SharedPreferences.getInstance();
      final body = json.decode(response.body);
      await storage.setString('TOKEN', body['token']);
      await storage.setString('EMAIL', email);
      return UserModel(email: email, token: body['token']);
    } else {
      return null;
    }
  }
}
*/

