class category {
  int? id;
  int? parent;
  int? categoryId;
  String? title;
  String? description;
  String? icon;
  String? dateAdd;
  String? dateUpd;
  int? productsCount;

  category(
      {this.id,
      this.parent,
      this.categoryId,
      this.title,
      this.description,
      this.icon,
      this.dateAdd,
      this.dateUpd,
      this.productsCount});

  category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    parent = json['parent'];
    categoryId = json['categoryId'];
    title = json['title'];
    description = json['description'];
    icon = json['icon'];
    dateAdd = json['created_at'];
    dateUpd = json['updated_at'];
    productsCount = json['productsCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['parent'] = this.parent;
    data['categoryId'] = this.categoryId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['icon'] = this.icon;
    data['created_at'] = this.dateAdd;
    data['updated_at'] = this.dateUpd;
    data['productsCount'] = this.productsCount;
    return data;
  }
}
