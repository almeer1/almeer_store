class categoryProduct {
  int? id;
  String? productId;
  String? source;
  int? categoryId;
  String? productAr;
  String? productEn;
  String? description;
  String? company;
  String? distributor;
  int? minAmount;
  int? unit;
  int? price;
  int? cost;
  int? amount;
  String? barcode;
  String? image;
  String? createdAt;
  String? updatedAt;
  String? expireDate;
  int? parent;
  String? title;
  String? icon;

  categoryProduct(
      {this.id,
        this.productId,
        this.source,
        this.categoryId,
        this.productAr,
        this.productEn,
        this.description,
        this.company,
        this.distributor,
        this.minAmount,
        this.unit,
        this.price,
        this.cost,
        this.amount,
        this.barcode,
        this.image,
        this.createdAt,
        this.updatedAt,
        this.expireDate,
        this.parent,
        this.title,
        this.icon});

  categoryProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['productId'];
    source = json['source'];
    categoryId = json['categoryId'];
    productAr = json['productAr'];
    productEn = json['productEn'];
    description = json['description'];
    company = json['company'];
    distributor = json['distributor'];
    minAmount = json['minAmount'];
    unit = json['unit'];
    price = json['price'];
    cost = json['cost'];
    amount = json['amount'];
    barcode = json['barcode'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    expireDate = json['expireDate'];
    parent = json['parent'];
    title = json['title'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['productId'] = this.productId;
    data['source'] = this.source;
    data['categoryId'] = this.categoryId;
    data['productAr'] = this.productAr;
    data['productEn'] = this.productEn;
    data['description'] = this.description;
    data['company'] = this.company;
    data['distributor'] = this.distributor;
    data['minAmount'] = this.minAmount;
    data['unit'] = this.unit;
    data['price'] = this.price;
    data['cost'] = this.cost;
    data['amount'] = this.amount;
    data['barcode'] = this.barcode;
    data['image'] = this.image;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['expireDate'] = this.expireDate;
    data['parent'] = this.parent;
    data['title'] = this.title;
    data['icon'] = this.icon;
    return data;
  }
}