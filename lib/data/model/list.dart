import '../module/categories.dart';
import '../module/offers.dart';
import '../module/pro.dart';
import '../module/recommended.dart';

List<String> image_list = [
  'assets/images/ads-everywhere-1.jpg',
  'assets/images/night-square.jpg',
  'assets/images/ads-everywhere-1.jpg'
];
List<String> image_List = [
  'assets/images/al-meer-1 (1).jpg',
  'assets/images/al-meer-3 (1).jpg',
  'assets/images/al-meer-2 (1).jpg'
];
List<String> imagepro_list = [
  'assets/images/photo2.png',
  'assets/images/photo2.png',
  'assets/images/photo2.png'
];

List<Categories> category_list = [
  Categories(
    name: 'الأطفال',
    image: 'assets/images/Kids.jpeg',
  ),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
  Categories(name: 'الأطفال', image: 'assets/images/Kids.jpeg'),
];

List<Recommend> recommend_list = [
  Recommend(
      name: 'T500 ساعة ذكية',
      image: 'assets/images/Kids.jpeg',
      price: '\$24.50',
      description: 'مزودة بشاشة تعمل باللمس با',
      text1: '89.00 د.إ',
      text2: 'خصم 72%'),
  Recommend(
      name: 'T500 ساعة ذكية ',
      image: 'assets/images/Kids.jpeg',
      price: '\$24.50',
      description: 'مزودة بشاشة تعمل باللمس با',
      text1: '89.00 د.إ',
      text2: 'خصم 72%'),
  Recommend(
      name: 'T500 ساعة ذكية',
      image: 'assets/images/Kids.jpeg',
      price: '\$24.50',
      description: 'مزودة بشاشة تعمل باللمس با',
      text1: '89.00 د.إ',
      text2: 'خصم 72%'),
  Recommend(
      name: 'T500 ساعة ذكية',
      image: 'assets/images/Kids.jpeg',
      price: '\$24.50',
      description: 'مزودة بشاشة تعمل باللمس با',
      text1: '89.00 د.إ',
      text2: 'خصم 72%'),
  Recommend(
      name: 'T500 ساعة ذكية ',
      image: 'assets/images/Kids.jpeg',
      price: '\$24.50',
      description: 'مزودة بشاشة تعمل باللمس با',
      text1: '89.00 د.إ',
      text2: 'خصم 72%'),
  Recommend(
      name: 'T500 ساعة ذكية',
      image: 'assets/images/Kids.jpeg',
      price: '\$24.50',
      description: 'مزودة بشاشة تعمل باللمس با',
      text1: '89.00 د.إ',
      text2: 'خصم 72%'),
  Recommend(
      name: 'T500 ساعة ذكية',
      image: 'assets/images/Kids.jpeg',
      price: '\$24.50',
      description: 'مزودة بشاشة تعمل باللمس با',
      text1: '89.00 د.إ',
      text2: 'خصم 72%'),
];
List<Offers> offers_list = [
  Offers(
      name: 'أطقم',
      image: 'assets/images/Kids.jpeg',
      price: 'تبدأمن 20 د.إ',
      description: 'فرن نيكاوي 4 شعلات'),
  Offers(
      name: 'أطقم',
      image: 'assets/images/Kids.jpeg',
      price: 'تبدأمن 20 د.إ',
      description: 'فرن نيكاوي 4 شعلات'),
  Offers(
      name: 'أطقم',
      image: 'assets/images/Kids.jpeg',
      price: 'تبدأمن 20 د.إ',
      description: 'فرن نيكاوي 4 شعلات'),
  Offers(
      name: 'أطقم',
      image: 'assets/images/Kids.jpeg',
      price: 'تبدأمن 20 د.إ',
      description: 'فرن نيكاوي 4 شعلات'),
];

List<Pro> pro_list = [
  Pro(
      txt1: 'نون كرت هدية',
      txt2: 'تسليم هدية',
      image: 'assets/images/images.jpg'),
  Pro(
      txt1: 'نون كرت هدية',
      txt2: 'تسليم هدية',
      image: 'assets/images/images.jpg'),
  /* Pro(txt1: 'بيانات',txt2: 'بيانات',image: 'assets/images/images.jpg'),
  Pro(txt1: 'بيانات',txt2: 'بيانات',image: 'assets/images/images.jpg'),*/
];

List<String> categ_list = [
  'فقط لأجلك',
  'أزياء الرجال',
  'أزياء الأطفال',
  'الأجهزة الإلكترونية',
  'أزياء النساء',
  'أزياء الأطفال',
  'الأجهزة الإلكترونية',
  'أزياء النساء',
  'أزياء الأطفال',
  'الأجهزة الإلكترونية',
  'أزياء النساء',
  'أزياء الأطفال',
  'الأجهزة الإلكترونية',
  'أزياء النساء',
  'أزياء الأطفال',
  'الأجهزة الإلكترونية',
  'أزياء النساء',
  'أزياء الأطفال',
  'الأجهزة الإلكترونية',
  'أزياء النساء',
  'أزياء الأطفال',
  'الأجهزة الإلكترونية',
  'الجمال',
  'الصحةو التغذية',
  'الجمال',
  'البقالة',
];

List<String> imag_menlist = [
  'assets/images/men3.jpg',
  'assets/images/men0.jpg',
  'assets/images/men1.jpg',
  'assets/images/men6.jpg',
  'assets/images/men7.jpg',
  'assets/images/men5.jpg',
  'assets/images/men4.jpg',
  'assets/images/shoes1.jpg',
  'assets/images/shoes2.jpg',
  'assets/images/shoes4.jpg',
  'assets/images/shoes5.jpg',
  'assets/images/accessories2.jpg',
  'assets/images/accessories3.jpg',
];
List<String> lable_menlist = [
  'آخر ما وصل',
  'الملابس المريحة',
  'التيشرتات',
  'الشورتات',
  'البدلات ',
  'الجينزات',
  'سترات',
  'رسمي',
  'اللوفرز',
  'السنيكرز',
  'الأحذية الرياضية',
  'النظارات',
  'الساعات',
];
/*
List<String>imag_cloths=[
  'assets/images/men3.jpg',
  'assets/images/men0.jpg',
  'assets/images/men1.jpg',
  'assets/images/men6.jpg',
  'assets/images/men7.jpg',
  'assets/images/men5.jpg',
  'assets/images/men4.jpg',
  'assets/images/men2.jpg',

];
List<String>cloth_namelist=[
  'آخر ما وصل',
  'الملابس المريحة',
  'التيشرتات',
  'الشورتات',
  'البدلات ',
  'الجينزات',
  'سترات',
  'سترات',
];

*/
