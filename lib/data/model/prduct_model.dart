
import 'dart:convert';


class productPag {
  int? currentPage;
  List<Data>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  List<Links>? links;
  String? nextPageUrl;
  String? path;
  int? perPage;
  Null? prevPageUrl;
  int? to;
  int? total;

  productPag(
      {this.currentPage,
        this.data,
        this.firstPageUrl,
        this.from,
        this.lastPage,
        this.lastPageUrl,
        this.links,
        this.nextPageUrl,
        this.path,
        this.perPage,
        this.prevPageUrl,
        this.to,
        this.total});

  productPag.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add( Data.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    if (json['links'] != null) {
      links = <Links>[];
      json['links'].forEach((v) {
        links!.add(new Links.fromJson(v));
      });
    }
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    if (this.links != null) {
      data['links'] = this.links!.map((v) => v.toJson()).toList();
    }
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class Data {
  int? id;
  String? productId;
  String? source;
  int? categoryId;
  String? productAr;
  String? productEn;
  String? description;
  String? company;
  String? distributor;
  int? minAmount;
  String? unit;
  int? price;
  int? cost;
  int? amount;
  String? barcode;
  String? image;
  String? createdAt;
  String? updatedAt;
  Null? expireDate;
  Category? category;

  Data(
      {this.id,
        this.productId,
        this.source,
        this.categoryId,
        this.productAr,
        this.productEn,
        this.description,
        this.company,
        this.distributor,
        this.minAmount,
        this.unit,
        this.price,
        this.cost,
        this.amount,
        this.barcode,
        this.image,
        this.createdAt,
        this.updatedAt,
        this.expireDate,
        this.category});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['productId'];
    source = json['source'];
    categoryId = json['categoryId'];
    productAr = json['productAr'];
    productEn = json['productEn'];
    description = json['description'];
    company = json['company'];
    distributor = json['distributor'];
    minAmount = json['minAmount'];
    unit = json['unit'];
    price = json['price'];
    cost = json['cost'];
    amount = json['amount'];
    barcode = json['barcode'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    expireDate = json['expireDate'];
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['productId'] = this.productId;
    data['source'] = this.source;
    data['categoryId'] = this.categoryId;
    data['productAr'] = this.productAr;
    data['productEn'] = this.productEn;
    data['description'] = this.description;
    data['company'] = this.company;
    data['distributor'] = this.distributor;
    data['minAmount'] = this.minAmount;
    data['unit'] = this.unit;
    data['price'] = this.price;
    data['cost'] = this.cost;
    data['amount'] = this.amount;
    data['barcode'] = this.barcode;
    data['image'] = this.image;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['expireDate'] = this.expireDate;
    if (this.category != null) {
      data['category'] = this.category!.toJson();
    }
    return data;
  }
}

class Category {
  int? id;
  int? parent;
  int? categoryId;
  String? title;
  Null? description;
  String? icon;
  String? createdAt;
  String? updatedAt;

  Category(
      {this.id,
        this.parent,
        this.categoryId,
        this.title,
        this.description,
        this.icon,
        this.createdAt,
        this.updatedAt});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    parent = json['parent'];
    categoryId = json['categoryId'];
    title = json['title'];
    description = json['description'];
    icon = json['icon'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['parent'] = this.parent;
    data['categoryId'] = this.categoryId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['icon'] = this.icon;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Links {
  String? url;
  String? label;
  bool? active;

  Links({this.url, this.label, this.active});

  Links.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    label = json['label'];
    active = json['active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = this.url;
    data['label'] = this.label;
    data['active'] = this.active;
    return data;
  }
}