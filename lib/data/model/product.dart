class product {
  int? id;
  String? productId;
  String? source;
  String? productAr;
  String? productEn;
  String? description;
  int? company;
  int? distributor;
  int? minAmount;
  int? unit;
  int? price;

  int? amount;
  String? barcode;
  String? image;
  String? expireDate;
  String? dateAdd;
  String? dateUpd;
  Category? category;

  product(
      {this.id,
      this.productId,
      this.source,
      this.productAr,
      this.productEn,
      this.description,
      this.company,
      this.distributor,
      this.minAmount,
      this.unit,
      this.price,

      this.amount,
      this.barcode,
      this.image,
      this.expireDate,
      this.dateAdd,
      this.dateUpd,
      this.category});

  product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['productId'];
    source = json['source'];
    productAr = json['productAr'];
    productEn = json['productEn'];
    description = json['description'];
    company = json['company'];
    distributor = json['distributor'];
    minAmount = json['minAmount'];
    unit = json['unit'];
    price = json['price'];

    amount = json['amount'];
    barcode = json['barcode'];
    image = json['image'];
    expireDate = json['expireDate'];
    dateAdd = json['date_add'];
    dateUpd = json['date_upd'];
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['productId'] = this.productId;
    data['source'] = this.source;
    data['productAr'] = this.productAr;
    data['productEn'] = this.productEn;
    data['description'] = this.description;
    data['company'] = this.company;
    data['distributor'] = this.distributor;
    data['minAmount'] = this.minAmount;
    data['unit'] = this.unit;
    data['price'] = this.price;

    data['amount'] = this.amount;
    data['barcode'] = this.barcode;
    data['image'] = this.image;
    data['expireDate'] = this.expireDate;
    data['date_add'] = this.dateAdd;
    data['date_upd'] = this.dateUpd;
    if (this.category != null) {
      data['category'] = this.category!.toJson();
    }
    return data;
  }
}

class Category {
  int? id;
  bool? forceId;
  String? title;
  Null? description;
  int? parent;
  int? categoryId;
  String? icon;
  String? dateAdd;
  String? dateUpd;

  Category(
      {this.id,
      this.forceId,
      this.title,
      this.description,
      this.parent,
      this.categoryId,
      this.icon,
      this.dateAdd,
      this.dateUpd});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    forceId = json['force_id'];
    title = json['title'];
    description = json['description'];
    parent = json['parent'];
    categoryId = json['categoryId'];
    icon = json['icon'];
    dateAdd = json['date_add'];
    dateUpd = json['date_upd'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['force_id'] = this.forceId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['parent'] = this.parent;
    data['categoryId'] = this.categoryId;
    data['icon'] = this.icon;
    data['date_add'] = this.dateAdd;
    data['date_upd'] = this.dateUpd;
    return data;
  }
}
