class searchProduct {
  int? id;
  String? productId;
  String? source;
  int? categoryId;
  String? productAr;
  String? productEn;
  String? description;
  String? company;
  String? distributor;
  int? minAmount;
  int? unit;
  int? price;

  int? amount;
  String? barcode;
  String? image;
  String? createdAt;
  String? updatedAt;
  String? expireDate;

  searchProduct(
      {this.id,
      this.productId,
      this.source,
      this.categoryId,
      this.productAr,
      this.productEn,
      this.description,
      this.company,
      this.distributor,
      this.minAmount,
      this.unit,
      this.price,
      this.amount,
      this.barcode,
      this.image,
      this.createdAt,
      this.updatedAt,
      this.expireDate});

  searchProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['productId'];
    source = json['source'];
    categoryId = json['categoryId'];
    productAr = json['productAr'];
    productEn = json['productEn'];
    description = json['description'];
    company = json['company'];
    distributor = json['distributor'];
    minAmount = json['minAmount'];
    unit = json['unit'];
    price = json['price'];
    amount = json['amount'];
    barcode = json['barcode'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    expireDate = json['expireDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['productId'] = this.productId;
    data['source'] = this.source;
    data['categoryId'] = this.categoryId;
    data['productAr'] = this.productAr;
    data['productEn'] = this.productEn;
    data['description'] = this.description;
    data['company'] = this.company;
    data['distributor'] = this.distributor;
    data['minAmount'] = this.minAmount;
    data['unit'] = this.unit;
    data['price'] = this.price;
    data['amount'] = this.amount;
    data['barcode'] = this.barcode;
    data['image'] = this.image;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['expireDate'] = this.expireDate;
    return data;
  }
}

class searchProductt {
  final int id;
  final String productId;
  final String source;
  final int categoryId;
  final String productAr;
  final String productEn;
  final String description;


  final int minAmount;

  final int price;

  final int amount;
  final String barcode;
  final String image;
  final String createdAt;
  final String updatedAt;

  searchProductt(
      {
        required this.id,
      required this.productId,
      required this.source,
      required this.categoryId,
      required this.productAr,
      required this.productEn,
      required this.description,

      required this.minAmount,

      required this.price,
      required this.amount,
      required this.barcode,
      required this.image,
      required this.createdAt,
      required this.updatedAt,
     });

    static searchProductt fromJson(Map<String, dynamic> json) => searchProductt(
    id : json['id'],
    productId : json['productId'],
    source : json['source'],
    categoryId : json['categoryId'],
    productAr : json['productAr'],
    productEn : json['productEn'],
    description : json['description'],

    minAmount : json['minAmount'],

    price : json['price'],
    amount : json['amount'],
    barcode : json['barcode'],
    image : json['image'],
    createdAt : json['created_at'],
    updatedAt : json['updated_at'],

   );
  }



