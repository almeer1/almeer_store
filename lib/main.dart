import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import 'package:meerstore/core/constant/them_manger.dart';
import 'package:meerstore/view/notification/notification.dart';

import 'package:meerstore/view/screen/Splash/splash.dart';

import 'package:meerstore/view/screen/category/category.dart';
import 'package:meerstore/view/screen/favorite/favourit_iem.dart';
import 'package:meerstore/view/screen/home/home_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:meerstore/view/screen/home/home_page_updata.dart';
import 'package:meerstore/view/screen/home/home_product.dart';
import 'package:meerstore/view/screen/profail/profail.dart';

import 'bloc/cart_bloc.dart';
import 'bloc/profail_bloc.dart';
import 'core/constant/bottom_bar.dart';
import 'data/data_source/DBHelper.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

Future backgroundMessage(RemoteMessage message) async {
  print("++++++++== NOTIFICATION++++++++++==");
  print("${message.notification!.body}");
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(backgroundMessage);
  DatabaseHelper dbHelper = DatabaseHelper();

  runApp(MyApp());
  await dbHelper.init();

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
}

class MyApp extends StatefulWidget {
  MyApp({super.key});

  static void setLocale(BuildContext buildContext, Locale locale) {
    _MyAppState? state = buildContext.findAncestorStateOfType<_MyAppState>();
    state!.setLocale(locale);
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale = Locale('ar', '');
  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  void initState() {
    // AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
    //   if(!isAllowed){
    //     AwesomeNotifications().requestPermissionToSendNotifications();
    //   }
    // });

    // TODO: implement initState
    super.initState();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    //   statusBarColor: Colors.indigo,
    //   systemNavigationBarColor: Colors.transparent,
    //   systemNavigationBarDividerColor: Colors.transparent,
    //   statusBarBrightness: Brightness.light
    // ));



    int id;

    return ScreenUtilInit(
        designSize: ScreenUtil.defaultSize,
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (contex, child) {
          return GetMaterialApp(
              builder: (context, child) {
                final mediQuary = MediaQuery.of(context);
                final scal = mediQuary.textScaleFactor.clamp(0.8, 0.9);
                return MultiBlocProvider(
                  providers: [
                    BlocProvider(create: (context) => CartBloc()),
                    BlocProvider(create: (context) => ProfailBloc()),
                  ],
                  child: MediaQuery(
                      data: MediaQuery.of(context)
                          .copyWith(textScaleFactor: scal),
                      child: child!),
                );
              },
              title: 'store',
              debugShowCheckedModeBanner: false,
              theme: getApplicationTheme(),
              localizationsDelegates: const [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: const [
                Locale('ar', 'SA'),
                Locale('en', 'US'),
              ],
              //localaization check null
              localeResolutionCallback: (currentlang, suppotlang) {
                if (currentlang != null) {
                  for (Locale _locale in suppotlang) {
                    if (_locale.languageCode == currentlang.languageCode) {
                      return currentlang;
                    }
                  }
                }
                return suppotlang.first;
              },
              locale: _locale,
              home: Splash(),
              routes: {
                '/BottomNav': (Contex) => BottomNav(id: 0),
                '/HomePageUpdata': (BuildContext Context) => HomePageUpdata(),
                //  '/home': (BuildContext Context) => new HomePage(),
                '/categoris': (BuildContext Context) => Categories(),
                '/profail': (BuildContext Context) => new Profail(),
                '/FavouritItem': (BuildContext Context) => new FavouritItem(),
                '/Notifcation': (BuildContext Context) => new Notifcation()
                // '/HomePagesc'
              });
        });
  }
}
