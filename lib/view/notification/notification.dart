import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/view/screen/profail/appar_not.dart';
import 'package:meerstore/view/widget/auth/medium_text.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/auth/small_text.dart';

class Notifcation extends StatelessWidget {
  const Notifcation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Colors.white,
          automaticallyImplyLeading: false,
        elevation: 0.0,
        title: Row(children: [
            InkWell(
            onTap: (){ Navigator.pop(context);},
             child: Icon(FontAwesomeIcons.chevronRight,size: 25,color: ColorManager.grey1,),
      ),
          const SizedBox(width:8),
        Image(
        width: 200,

        image:  AssetImage(
          'assets/images/logo (1).png',
        ),),],),
      ),
      body: ListView(
       children: [


         Container(
           padding: const EdgeInsets.only(top: 15),
           decoration: BoxDecoration(
             color: Colors.grey.shade100,

           ),
           child: Padding(
             padding: const EdgeInsets.only(left: 16,right: 16),
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: [

                 Row(
                   children: [
                     const SizedBox(width:8),
                     MediumTextBold(text: 'الإشعارات', color: Colors.grey,),
                   ],
                 ),
                 Row(
                   children: [
                     const SizedBox(width:8),
                     SmallText( text:'لا تفوت أي رسالة منا,انقر الرابط أدناه لتفعيل الإشعارات بسهولة',),
                   ],
                 )


               ],
             ),
           ),

         ),
         Container(
           height: 90,
           // height:MediaQuery.of(context).size.height*0.06,
           padding: const EdgeInsets.only(top: 15),
             color: Colors.white,
           child: Center(child: MediumText(text:'افتح اعدادات الإشعارات',color:ColorManager.primary)),

         ),
         Container(
           height: 300,
           // height:MediaQuery.of(context).size.height*0.9,
           padding: const EdgeInsets.only(top: 15),
           decoration: BoxDecoration(
             color: Colors.grey.shade100,

           ),


         ),

       ],


      ),
    );
  }
}
