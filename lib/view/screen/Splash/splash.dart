
import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meerstore/view/screen/auth/login.dart';
import 'package:http/http.dart' as http;


import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../bloc/cart_bloc.dart';
class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  State<Splash> createState() => _SplashState();
}



class _SplashState extends State<Splash> {
  late String _base64;
  late Future getImage;

  Future<String> _getImage() async {
    String? base64;
    String Url =  'https://mapi.orasys.org/images/logo-h.png';
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    base64 = prefs.getString("base64Image");
    if (base64 == null) {
      final http.Response response = await http.get(Uri.parse(Url));
      base64 = base64Encode(response.bodyBytes);
      prefs.setString("base64Image", base64);
    }
    return base64;
  }


  Future check_login() async {
    SharedPreferences  prefs = await SharedPreferences.getInstance();
    const key = 'token';
    final _accountNumber = prefs.getInt('accountNm') ?? 0;
    print(_accountNumber);
    final is_login_value = prefs.get(key) ?? 0;
    if (is_login_value != 0) {
      BlocProvider.of<CartBloc>(
          context)
          .add(
          UpdateCartIconCount());

      Navigator.of(context).pushNamedAndRemoveUntil(
          '/BottomNav', (Route<dynamic> route) => false);
    }

    else {
      Navigator.push(context, MaterialPageRoute(builder:(context)=>Login() ));
    }
    // print(is_login_value);

  }


  void startTimer() {
    Timer timer = Timer.periodic(const Duration(seconds: 6), (time) {
      check_login();

      time.cancel();
    });
  }

  @override
  initState() {
    startTimer();
    getImage = _getImage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);


    return Scaffold(
      // backgroundColor: ColorManager.primary,
      body: SingleChildScrollView(
        child: Center(
          child: SizedBox(
            height: mq.size.height,
            child:
          Stack(
            alignment: Alignment.center,
            children: [
              const Image(
                image: AssetImage( 'assets/images/login-bg.jpg'),
                height: 900,
                fit: BoxFit.fitHeight,
              ),
              SizedBox(
                height: 200,
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 1.0, sigmaY: 1.0),
                  child: const Image(image: AssetImage('assets/images/logo-h (1).png'),fit: BoxFit.cover,

                  )
                ),
              ),
            ],
          )
            // FutureBuilder<dynamic>(
            //   future: getImage,
            //   builder: (context, snapshot) {
            //     if (snapshot.hasData) {
            //       _base64 = snapshot.data!;
            //       final Uint8List bytes = base64Decode(_base64);
            //       return
            //
            //         Image.memory(bytes,
            //           width: MediaQuery.of(context).size.width / 1.5,
            //           //height: MediaQuery.of(context).size.height,
            //           fit: BoxFit.fitWidth,
            //
            //         );
            //
            //
            //     }
            //     return const Center(child: CircularProgressIndicator());
            //   },
            // ),
          ),
        ),),);

  }
}
