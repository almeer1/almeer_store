import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/string_manger.dart';
import 'package:meerstore/view/screen/auth/forget_passwowrd.dart';
import 'package:meerstore/view/screen/auth/new_password.dart';
import 'package:meerstore/view/widget/auth/big_text.dart';
import 'package:meerstore/core/constant/change_lang.dart';
import 'package:meerstore/view/widget/auth/medium_text.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/auth/small_text.dart';

class EmailMessage extends StatefulWidget {
  const EmailMessage({Key? key}) : super(key: key);

  @override
  _EmailMessageState createState() => _EmailMessageState();
}

class _EmailMessageState extends State<EmailMessage> {
  Locale _locale = Locale('ar', '');
  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);
    var lang = AppLocalizations.of(context);
    return Scaffold(
        body: Padding(
        padding: const EdgeInsets.only(top:60,left:16,right: 16),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(

            children: [
              InkWell(
                child: Icon(FontAwesomeIcons.arrowRight,color:ColorManager.grey1) ,onTap: (){
                Navigator.pop(context);

              },),
              SizedBox(width: 8,),
              MediumTextt(text: lang!.back, color: ColorManager.grey1),
            ],
          ),
        SizedBox(
          height:24,
        ),
        BigTextt(text: lang!.check_email),
        SizedBox(
          height: mq.size.height * 0.03,
        ),
        MediumText(
          text: lang!.send_email,
          color: ColorManager.grey1,
        ),
        SizedBox(
          height: mq.size.height * 0.03,
        ),
        Container(
            height: mq.size.height * 0.06,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: ColorManager.grey1),
            child: InkWell(
              child: Text(
                lang!.open_email_app,
                style: const TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontFamily: 'Cairo',
                    fontWeight: FontWeight.bold),
              ),
              onTap: () {},
            )),
        SizedBox(
          height: mq.size.height * 0.03,
        ),
        Row(
          children: [
            InkWell(
              child: MediumTextBold(
                text: lang!.skip,
                color: ColorManager.black,
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => NewPssword()));
              },
            ),
            SizedBox(
              width: mq.size.width * 0.01,
            ),
            MediumText(
              text: lang!.confirm_later,
              color: ColorManager.grey1,
            ),
          ],
        ),
        SizedBox(
          height: mq.size.height * 0.06,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MediumText(text: lang!.receive_email, color: ColorManager.grey1),
            InkWell(
                child: MediumTextBold(
                    text: lang!.another_email, color: ColorManager.grey1),
                onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ForgetPass()),
                    ))
          ],
        ),
      ]),
    ));
  }
}
