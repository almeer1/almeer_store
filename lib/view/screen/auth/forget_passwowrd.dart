import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/font_manger.dart';
import 'package:meerstore/core/constant/string_manger.dart';
import 'package:meerstore/view/screen/auth/login.dart';
import 'package:meerstore/view/screen/auth/new_password.dart';
import 'package:meerstore/view/screen/auth/signup.dart';
import 'package:meerstore/view/widget/auth/big_text.dart';
import 'package:meerstore/core/constant/change_lang.dart';
import 'package:meerstore/view/widget/auth/medium_text.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/auth/small_text.dart';
import 'package:meerstore/core/constant/assets_Widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
class ForgetPass extends StatefulWidget {
  const ForgetPass({Key? key}) : super(key: key);

  @override
  _ForgetPassState createState() => _ForgetPassState();
}

class _ForgetPassState extends State<ForgetPass> {
  AssetWidget _ass=AssetWidget();
  TextEditingController _emailController=TextEditingController();
  Locale _locale = Locale('ar', '');
  @override
  Widget build(BuildContext context) {
    var mq=MediaQuery.of(context);
    var lang=AppLocalizations.of(context);
    return Scaffold(
      body:SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top:34,left: 16,right: 16),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,

              children:[

                SizedBox(
                  height: mq.size.height *0.02
                  ,
                ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image(
                  width: 200,
                  image:  AssetImage(
                   'assets/images/logo (1).png',
                  ),),

                IconButton(
                  padding: EdgeInsets.zero,
                  icon: Icon(FontAwesomeIcons.xmark,color: ColorManager.grey1,) ,onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder:(context)=>Login()));
                },),

              ],
            ),
                SizedBox(
                  height:mq.size.height *0.06,),

            BigTextt(text: lang!.forget_pass),
                SizedBox(
                  height:mq.size.height *0.03,),
            MediumText(text:lang!.forgetPasswordText ,color: ColorManager.grey1,),
                SizedBox(
                  height:mq.size.height *0.01,),
            _ass.AuthTextField(_emailController, lang!.email_text,false,TextInputType.emailAddress, TextStyle(fontSize: 24,fontFamily: 'Cairo',color: ColorManager.grey1,),),
                SizedBox(
                  height:mq.size.height *0.03,),
            Container(

                  height:mq.size.height *0.06,
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(0),
                alignment: Alignment.center,
                decoration: BoxDecoration(

                    borderRadius: BorderRadius.circular(5),
                    color: ColorManager.grey1 ),
                child: InkWell(
                  child: Text(lang!.reset_password, style: TextStyle(fontSize: 18,
                      color: Colors.white, fontWeight: FontWeight.bold),),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder:(context)=>NewPssword()));
                  },

                )),
                SizedBox(
                  height:mq.size.height *0.03,),
            Row(children: [
              MediumText(text:lang!.login_save_dont_have_account, color: ColorManager.grey1,),
              MediumTextt(text:'؟', color: ColorManager.primary,),
              SizedBox(width: 5,),
              InkWell(child: MediumTextBold(text: lang!.signup,color: ColorManager.grey1,),
              onTap:() =>Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp ()),)
              )],),

          ]),
        ),
      )
    );
  }
}
