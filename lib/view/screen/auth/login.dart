import 'dart:convert';
import 'dart:typed_data';


import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/string_manger.dart';
import 'package:meerstore/data/data_source/login%20_servece.dart';
import 'package:meerstore/view/screen/auth/forget_passwowrd.dart';
import 'package:meerstore/view/screen/auth/signup.dart';

import 'package:meerstore/view/widget/auth/big_text.dart';

import 'package:meerstore/view/widget/auth/medium_text.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/auth/small_text.dart';
import 'package:meerstore/core/constant/assets_Widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../core/constant/bottom_bar.dart';
import '../../../data/data_source/api_controller.dart';





class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}


class _LoginState extends State<Login> {


  ApiController apiController =ApiController();
  User_controller userController = new User_controller();
  final AssetWidget _assetWidget=AssetWidget();
  final AppStrings _appStrings=AppStrings();

  final TextEditingController _emailController=TextEditingController();
  final TextEditingController _passwordController=TextEditingController();
  final TextEditingController _usernameController=TextEditingController();

  bool is_loading = false;
  late String _base64;
  late Future getImage;
  bool _lang= false;
  Locale _locale = Locale('ar', '');
  bool obscure = true;
  Icon icon = Icon(Icons.visibility_off);

  // login _-proccess
  login_proccess() async {


    if(_usernameController.text.isEmpty)
    {
      _assetWidget.AlertDia(context,'بيانات ناقصة','يرجى كتابةاسم المستخدم بشكل صحيح');

      return;
    }
    //password validator
    if(_passwordController.text.length < 8)
    {
      _assetWidget.AlertDia(context,'بيانات ناقصة','يجب أن تكون كلمة المرور ٨ أحرف على الأقل');
      return;
    }

    setState(() {
      is_loading = true;
    });
   var  data = await userController.user_login(_usernameController.text, _passwordController.text);
    if(data)
    {
      // print(data);
      Navigator.of(context).pushNamedAndRemoveUntil('/BottomNav',(Route<dynamic> route) => false);
      is_loading =false;
    }else
      {
      _assetWidget.AlertDia(context, 'فشل تسجيل الدخول', 'تأكد من صحة اسم المستخدم أو كلمة المرور');
      is_loading =false;
    }

  }

  // get Image logo
  Future<String> _getImage() async {
    var data =await apiController.getImage();
    return data;
  }

  @override
  void initState() {

    // getImage = _getImage();
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose(){
    super.dispose();
    // _getImage();
  }

  @override
  Widget build(BuildContext context) {
    var mq=MediaQuery.of(context);
   var lang=AppLocalizations.of(context);
    return Scaffold(
      backgroundColor: ColorManager.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left:16.0,right: 16,top:34),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Image(
                    width: 200,
                    image:  AssetImage(
                      'assets/images/logo (1).png',
                    ),),
                  // IconButton(icon: const Icon(FontAwesomeIcons.globe) ,onPressed: (){
                  //   changeLanguage(context,_locale);
                  // },),
                  InkWell(

                    child:  Icon(FontAwesomeIcons.xmark,color:ColorManager.grey1) ,onTap: (){
                    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder:(context)=>BottomNav(id:0),),(Route<dynamic> route) => false);
                  },),


                ],
              ),
              SizedBox(height:mq.size.height *0.06,),
              BigTextt(text: lang!.login_welcome),
              SizedBox(height:mq.size.height *0.01,),
               SmallTextt(text: lang!.userName,),
              SizedBox(height:mq.size.height *0.01,),
              _assetWidget.AuthTextField(_usernameController, lang!.enter_userName, false,TextInputType.emailAddress, TextStyle(fontSize: 24,fontFamily: 'Cairo',color: ColorManager.grey1,),),

              // _assetWidget.AuthTextField(_emailController, lang!.email_text, false,TextInputType.emailAddress),
              SizedBox(height:mq.size.height *0.01,),
              SmallTextt(text: lang!.password_label,),SizedBox(
                height:mq.size.height *0.01,
              ),

              _assetWidget.TextFieldwidgtPass(_passwordController, lang!.pass_text ,  IconButton(
                  onPressed: () {
                    setState(() {
                      if (obscure == true) {
                        obscure = false;
                        icon = Icon(Icons.visibility,color: ColorManager.grey1,);
                      } else {
                        obscure = true;
                        icon = Icon(Icons.visibility_off,color:ColorManager.grey1,);
                      }
                    });
                  },
                  icon: icon
              ),TextInputType.text,TextStyle(fontSize: 24,fontFamily: 'Cairo',color: ColorManager.grey1,),obscure,),
              SizedBox(
                height:mq.size.height *0.06,),
              InkWell(child: MediumTextBold(text:lang.forget_pass,color: ColorManager.grey1 ),
              onTap: (){
                // Navigator.push(context, MaterialPageRoute(builder:(context)=>ForgetPass() ),);
                },),
              SizedBox(height:mq.size.height *0.03,),
              InkWell(
                onTap: () {
                  login_proccess();
                 // Navigator.push(context, MaterialPageRoute(builder:(context)=>HomePage() ),);
                },
                child: Container(
                    height:mq.size.height *0.06,
                           width: MediaQuery.of(context).size.width,
                         padding: const EdgeInsets.all(0),
                     alignment: Alignment.center,
                      decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                           color: ColorManager.primary ),
                         child: is_loading ? CircularProgressIndicator() :
                  Text(lang!.login, style:  TextStyle(fontSize: 18,
                               color: Colors.white,
                               fontWeight: FontWeight.bold),
            )),
              ),
              SizedBox(height:mq.size.height *0.03,),
              Row(children: [
                MediumText(text:lang!.login_save_dont_have_account, color: ColorManager.grey1,),
                MediumTextt(text:'؟', color: ColorManager.primary,),
                InkWell(child: MediumTextBold(text: lang!.signup,color: ColorManager.grey1,),
                    onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp()),)
                )],),
            ],

          ),
        ),
      ),

    );
  }
}
