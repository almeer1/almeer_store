
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/string_manger.dart';
import 'package:meerstore/view/screen/auth/email_message.dart';
import 'package:meerstore/view/screen/auth/forget_passwowrd.dart';
import 'package:meerstore/view/widget/auth/big_text.dart';
import 'package:meerstore/core/constant/change_lang.dart';
import 'package:meerstore/view/widget/auth/medium_text.dart';
import 'package:meerstore/core/constant/assets_Widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NewPssword extends StatefulWidget {

  const NewPssword({Key? key}) : super(key: key);

  @override
  _NewPsswordState createState() => _NewPsswordState();
}

class _NewPsswordState extends State<NewPssword> {
  AssetWidget _assetWidget1=AssetWidget();
  TextEditingController _passwordcontroller1 =TextEditingController();
  bool _lang= false;
  Locale _locale = Locale('ar', '');

  @override
  Widget build(BuildContext context) {
    var mq=MediaQuery.of(context);
    var lang=AppLocalizations.of(context);
    return Scaffold(
        body:SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 34,left: 16,right: 16),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children:[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      IconButton(icon: Icon(FontAwesomeIcons.arrowRight,color:ColorManager.grey1) ,onPressed: (){
                        Navigator.pop(context);

                      },),
                      MediumTextt(text: lang!.back, color: ColorManager.grey1),
                    ],
                  ),
                  SizedBox(
                    height: mq.size.height *0.06),

                  BigTextt(text: lang!.create_new_password),
                  SizedBox(
                    height: mq.size.height *0.03),
                  MediumText(text:lang!.new_password_text ,color: ColorManager.grey1,),
                  SizedBox(
                    height: mq.size.height *0.01),
                  _assetWidget1.TextFieldwidgt(_passwordcontroller1, lang!.password_hint, Icon(
                      Icons.visibility_off,color:ColorManager.grey1),TextInputType.emailAddress,TextStyle(fontSize: 24,fontFamily: 'Cairo',color: ColorManager.grey1,),false),
                  SizedBox(
                    height: mq.size.height *0.03),
                  //_assetWidget1.AuthTextField(_passwordcontroller1, AppStrings.confarmPassword, "نأكيد كلمة المرور",  false,TextInputType.emailAddress),
                  Container(
                      height: mq.size.height *0.06,
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.all(0),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(

                          borderRadius: BorderRadius.circular(5),
                          color: ColorManager.grey1 ),
                      child: InkWell(
                        child: Text(lang!.reset_password, style: TextStyle(fontSize: 18,
                            color: Colors.white, fontWeight: FontWeight.bold,fontFamily: 'Cairo'),),
                        onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder:(context)=>EmailMessage()));
                        },

                      )),


                ]),
          ),
        )
    );
  }
}
