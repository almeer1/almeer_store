import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/font_manger.dart';
import 'package:meerstore/core/constant/string_manger.dart';
import 'package:meerstore/main.dart';
import 'package:meerstore/view/screen/auth/login.dart';
import 'package:meerstore/view/screen/home/home_page.dart';
import 'package:meerstore/view/widget/auth/big_text.dart';
import 'package:meerstore/core/constant/change_lang.dart';
import 'package:meerstore/view/widget/auth/medium_text.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/auth/small_text.dart';
import 'package:meerstore/core/constant/assets_Widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../core/constant/bottom_bar.dart';
import '../../../data/data_source/api_controller.dart';
import '../../../data/data_source/login _servece.dart';
import '../home/home_page_updata.dart';
import '../home/home_product.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final AssetWidget _assetWidget = AssetWidget();
  User_controller userController = new User_controller();
  ApiController apiController = ApiController();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController firstnameController = TextEditingController();
  final TextEditingController lastnameController = TextEditingController();
  final TextEditingController mobileController = TextEditingController();
  final TextEditingController userNameController = TextEditingController();
  final TextEditingController _typeController = TextEditingController();

  bool is_loading = false;
  bool _lang = false;
  Locale _locale = Locale('ar', '');
  late String _base64;
  late Future getImage;

  bool obscure = true;
  Icon icon = Icon(Icons.visibility_off);

  SingUpProcces() async {
    // if (userNameController.text.length < 4) {
    //   _assetWidget.AlertDia(
    //       context, 'بيانات ناقصة', 'يجب أن يكون اسم المتسخدم 2 أحرف على الأقل');

    //   return;
    // }
    //check if name length = 3 or more
    if (firstnameController.text.length < 3) {
      _assetWidget.AlertDia(
          context, 'بيانات ناقصة', 'يجب أن يكون الاسم ٣ أحرف على الأقل');

      return;
    }

    if (lastnameController.text.length < 3) {
      _assetWidget.AlertDia(
          context, 'بيانات ناقصة', 'يجب أن يكون الاسم الثاني ٣ أحرف على الأقل');

      return;
    }
    if (lastnameController.text.length < 3) {
      _assetWidget.AlertDia(
          context, 'بيانات ناقصة', 'يجب أن يكون الاسم الثاني ٣ أحرف على الأقل');

      return;
    }
    //phone validator
    if (mobileController.text.length != 10 ||
        mobileController.text[0] != '0' ||
        mobileController.text[1] != '9') {
      _assetWidget.AlertDia(
          context, 'بيانات ناقصة', 'يرجى كتابة رقم الهاتف بشكل صحيح');
      return;
    }

    //password validator
    if (passwordController.text.length < 8) {
      _assetWidget.AlertDia(
          context, 'بيانات ناقصة', 'يجب أن تكون كلمة المرور ٨ أحرف على الأقل');
      return;
    }
    setState(() {
      // is_loading = true;
      // print('object');
    });
    var data = await userController.user_register(
        userNameController.text,
        mobileController.text,
        passwordController.text,
        firstnameController.text,
        lastnameController.text);
    if (data['message'] != 'اسم المستخدم معرف مسبقا') {
      Navigator.of(context).pushNamed(
        '/BottomNav',
      );

      setState(() {
        // is_loading = false;
        // print('object');
      });
      // _assetWidget.AlertDia(context,'فشل العملية','تأكد من صحة اسم المستخدم أو كلمة المرور');
    } else if (data['message'] == 'اسم المستخدم معرف مسبقا') {
      _assetWidget.AlertDia(context, 'فشل العملية', data['message']);
      // is_loading = false;
    }
  }

  // get Image logo

  Future<String> _getImage() async {
    var data = await apiController.getImage();
    return data;
  }

  @override
  void initState() {
    // getImage = _getImage();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);
    var lang = AppLocalizations.of(context);
    return Scaffold(
      backgroundColor: ColorManager.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 34.0, left: 16, right: 16),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image(
                      width: 200,
                      image: AssetImage(
                        'assets/images/logo (1).png',
                      ),
                    ),
                    // IconButton(icon: const Icon(FontAwesomeIcons.globe) ,onPressed: (){
                    // changeLanguage(context,_locale);
                    //
                    // },),
                    IconButton(
                      padding: EdgeInsets.zero,
                      icon: const Icon(FontAwesomeIcons.xmark),
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BottomNav(id: 0),
                            ),
                            (Route<dynamic> route) => false);
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: mq.size.height * 0.06,
                ),

                BigTextt(text: lang!.create_account),
                SizedBox(
                  height: mq.size.height * 0.03,
                ),

                //userName
                SmallTextt(text: lang!.userName),
                TextFormField(
                  controller: userNameController,
                  validator: (input) {
                    if (userNameController.text.isEmpty) {
                      return;
                    } else {
                      return null;
                    }
                  },
                  decoration: InputDecoration(
                    hintText: lang!.enter_userName,
                    hintStyle: TextStyle(
                        fontFamily: 'Cairo',
                        color: ColorManager.grey1,
                        fontSize: 16),
                    contentPadding: EdgeInsets.only(top: 2),
                  ),
                ),
                // _assetWidget.AuthTextField(userNameController, lang!.enter_userName,false,TextInputType.text, TextStyle(fontSize: 24,fontFamily: 'Cairo',color: ColorManager.grey1,),),
                SizedBox(
                  height: mq.size.height * 0.01,
                ),
                // SmallText(text: lang!.first_name  ),
                // _assetWidget.AuthTextField(_typeController, lang!.enter_first_name,false,TextInputType.text,),
                // SizedBox(
                //   height:mq.size.height *0.01,
                // ),
                //firstName
                SmallTextt(text: lang!.first_name),
                _assetWidget.AuthTextField(
                  firstnameController,
                  lang!.enter_first_name,
                  false,
                  TextInputType.text,
                  TextStyle(
                    fontSize: 24,
                    fontFamily: 'Cairo',
                    color: ColorManager.grey1,
                  ),
                ),
                SizedBox(
                  height: mq.size.height * 0.01,
                ),
                //lastname
                SmallTextt(
                  text: lang!.last_name,
                ),
                _assetWidget.AuthTextField(
                  lastnameController,
                  lang!.enter_last_name,
                  false,
                  TextInputType.text,
                  TextStyle(
                    fontSize: 24,
                    fontFamily: 'Cairo',
                    color: ColorManager.grey1,
                  ),
                ),
                SizedBox(
                  height: mq.size.height * 0.01,
                ),
                //phonenumber
                SmallTextt(text: lang!.phone_number),
                _assetWidget.AuthTextField(
                  mobileController,
                  lang!.phone_number_text,
                  false,
                  TextInputType.text,
                  TextStyle(
                    fontSize: 24,
                    fontFamily: 'Cairo',
                    color: ColorManager.grey1,
                  ),
                ),
                SizedBox(
                  height: mq.size.height * 0.01,
                ),
                //password
                SmallTextt(text: lang!.password_label),
                // TextField(
                //   obscureText: _isObscure,
                //   decoration: InputDecoration(
                //       labelText: lang!.pass_text,
                //       // this button is used to toggle the password visibility
                //       suffixIcon: IconButton(
                //           icon: Icon(
                //               _isObscure ? Icons.visibility_off : Icons.visibility),
                //           onPressed: () {
                //             setState(() {
                //               _isObscure = !_isObscure;
                //             });
                //           })),
                // ),

                _assetWidget.TextFieldwidgtPass(
                    passwordController,
                    lang!.pass_text,
                    IconButton(
                        onPressed: () {
                          setState(() {
                            if (obscure == true) {
                              obscure = false;
                              icon = Icon(
                                Icons.visibility,
                                color: ColorManager.grey1,
                              );
                            } else {
                              obscure = true;
                              icon = Icon(
                                Icons.visibility_off,
                                color: ColorManager.grey1,
                              );
                            }
                          });
                        },
                        icon: icon),
                    TextInputType.emailAddress,
                    TextStyle(
                      fontSize: 24,
                      fontFamily: 'Cairo',
                      color: ColorManager.grey1,
                    ),
                    obscure),
                //button is used to signup
                SizedBox(
                  height: mq.size.height * 0.03,
                ),
                InkWell(
                  onTap: () {
                    SingUpProcces();
                  },
                  child: Container(
                      height: mq.size.height * 0.06,
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.all(0),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: ColorManager.primary),
                      child: is_loading
                          ? CircularProgressIndicator()
                          : Text(
                              lang!.signup,
                              style: const TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )),
                  // onTap: () =>Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(user:  user,))),
                ),
                SizedBox(
                  height: mq.size.height * 0.03,
                ),

                Row(
                  children: [
                    MediumText(
                      text: lang!.signup_save_have_account,
                      color: ColorManager.grey1,
                    ),
                    MediumTextt(
                      text: '؟',
                      color: ColorManager.primary,
                    ),
                    SizedBox(width: mq.size.height * 0.001),
                    InkWell(
                        child: MediumTextBold(
                          text: lang!.login,
                          color: ColorManager.grey1,
                        ),
                        onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Login()),
                            ))
                  ],
                ),
              ]),
        ),
      ),
    );
  }
}

Widget _textFieldItem(
    {bool? isSecure,
    required TextEditingController controller,
    required String hintText}) {
  return TextFormField(
    controller: controller,
    validator: (input) {
      if (controller.text.isEmpty) {
        return "$hintText must not be empty";
      } else {
        return null;
      }
    },
    obscureText: isSecure ?? false,
    decoration:
        InputDecoration(hintText: hintText, border: OutlineInputBorder()),
  );
}
