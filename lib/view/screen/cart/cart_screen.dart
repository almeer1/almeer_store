import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../core/constant/bottomnav_bar.dart';
import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/DBHelper.dart';
import '../../../data/data_source/login _servece.dart';

import '../../widget/auth/medium_text.dart';
import '../../widget/auth/medium_txt_blod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../auth/signup.dart';
import 'check_out .dart';
import '../../../bloc/cart_bloc.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  DatabaseHelper dbHelper = DatabaseHelper();
  User_controller usercontroller = new User_controller();

  String address = "";
  String mobile = '';
  bool isloading = true;
  double totalPrice = 0.0;

  List _listproduct = [];

  //read data from database
  Future readData() async {
    double Price = 0.0;
    _listproduct.clear();
    List<Map<String, dynamic>> responce = await dbHelper.queryAllRows();
    _listproduct.addAll(responce);

    for (int i = 0; i < _listproduct.length; i++) {
      setState(() {
        isloading = false;
        Price = Price +
            (double.parse(_listproduct[i]['price']) *
                int.parse(_listproduct[i]['Amount']));
      });
    }
    totalPrice = Price;

    if (this.mounted) {
      setState(() {});
    }
  }

  // Check if the user is logged in or not
  check_login() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs);
    const key = 'token';
    final is_login_value = prefs.get(key) ?? 1;
    final _mobile = prefs.getString('mobile') ?? 0;
    final _address = prefs.getString('address') ?? 0;
    setState(() {
      address = _address.toString();
      mobile = _mobile.toString();
    });
    if (is_login_value != 1) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => CheckoutScreen()));
    } else {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => SignUp()));
    }
  }

  @override
  void initState() {
    readData();

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorManager.grey1,
        title: MediumText(
          text: 'عربة التسوق',
          color: Colors.white,
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: InkWell(
          onTap: () {
            BlocProvider.of<CartBloc>(context).add(UpdateCartIconCount());
            Navigator.popAndPushNamed(context, '/BottomNav');
          },
          child: const Icon(Icons.arrow_back),
        ),
      ),
      body:
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  _listproduct.isEmpty
                      ? Center(
                          child: Align(
                            alignment: Alignment.center,
                            child: Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Image(
                                    image: AssetImage(
                                        'assets/images/empty_cart.png'),
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01,
                                  ),
                                  MediumTextBold(
                                      text: 'عربة التسوق فارغة 😌',
                                      color: ColorManager.grey1),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01,
                                  ),
                                  MediumTextBold(
                                    text:
                                        'استكشف المنتجات وتسوق العناصر المفضلة لديك',
                                    color: ColorManager.grey1,
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      : ListView.builder(
                          itemCount: _listproduct.length,
                          shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Card(
                              child: ListTile(
                                leading: Container(
                                  height:
                                      MediaQuery.of(context).size.width * 0.4,
                                  width:
                                      MediaQuery.of(context).size.width * 0.2,
                                  margin: const EdgeInsets.only(right: 10),
                                  child: Image.network(
                                      _listproduct[index]['image'],
                                      fit: BoxFit.fitHeight),
                                ),
                                title: MediumTextBold(
                                  text: _listproduct[index]['productName'],
                                  color: ColorManager.grey1,
                                ),
                                subtitle: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        int.parse(_listproduct[index]
                                                    ["Amount"]) ==
                                                int.parse(_listproduct[index]
                                                    ['MinAmount'])
                                            ? InkWell(
                                                onTap: () async {
                                                  int quantity = int.parse(
                                                      _listproduct[index]
                                                          ["Amount"]);
                                                  int price = int.parse(
                                                      _listproduct[index]
                                                          ["price"]);
                                                  totalPrice = totalPrice -
                                                      (double.parse(
                                                          (price * quantity)
                                                              .toString()));
                                                  int delete =
                                                      await dbHelper.delete(
                                                          _listproduct[index]
                                                              ['id']);
                                                  if (delete > 0) {
                                                    _listproduct.removeWhere(
                                                        (element) =>
                                                            element['id'] ==
                                                            _listproduct[index]
                                                                ['id']);
                                                    setState(() {});
                                                    BlocProvider.of<CartBloc>(
                                                            context)
                                                        .add(
                                                            UpdateCartIconCount());
                                                  }
                                                },
                                                child: const Icon(
                                                  Icons.remove,
                                                  color: Colors.black,
                                                ),
                                              )
                                            : InkWell(
                                                child: SizedBox(
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.05,
                                                  child: const Icon(
                                                    Icons.remove,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                onTap: () async {
                                                  int quantity = int.parse(
                                                      _listproduct[index]
                                                          ['Amount']);
                                                  int price = int.parse(
                                                      _listproduct[index]
                                                          ["price"]);
                                                  quantity--;
                                                  int? newPrice =
                                                      price * quantity;
                                                  if (quantity > 0) {
                                                    await dbHelper.update({
                                                      "id": _listproduct[index]
                                                          ["id"],
                                                      "p_id":
                                                          _listproduct[index]
                                                              ["p_id"],
                                                      "productName":
                                                          _listproduct[index]
                                                              ["productName"],
                                                      "image":
                                                          _listproduct[index]
                                                              ["image"],
                                                      "price":
                                                          _listproduct[index]
                                                              ["price"],
                                                      "Amount": quantity
                                                    }).then((value) async {
                                                      quantity = 0;
                                                      await readData();
                                                    }).onError(
                                                        (error, stackTrace) {
                                                      print(error.toString());
                                                    });
                                                  }
                                                  setState(() {});
                                                }),
                                        const SizedBox(
                                          width: 5.0,
                                        ),
                                        SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.04,
                                          child: MediumText(
                                            text:
                                                '${_listproduct[index]['Amount']}',
                                            color: ColorManager.grey1,
                                          ),
                                        ),
                                        const SizedBox(width: 5.0),
                                        Listener(
                                          child: int.parse(_listproduct[index]
                                                      ["Amount"]) >
                                                  int.parse(_listproduct[index]
                                                      ['Quantety'])
                                              ? InkWell(
                                                  onTap: () {
                                                    const snackBar = SnackBar(
                                                      backgroundColor:
                                                          Colors.green,
                                                      content: Text(
                                                        ' عذراً غير متوفرة الآن',
                                                        style: TextStyle(
                                                            fontSize: 18,
                                                            fontFamily:
                                                                'Cairo'),
                                                      ),
                                                      duration:
                                                          Duration(seconds: 1),
                                                    );
                                                    ScaffoldMessenger.of(
                                                            context)
                                                        .showSnackBar(snackBar);
                                                  },
                                                  child: const Icon(
                                                    Icons.add,
                                                    color: Colors.black,
                                                  ),
                                                )
                                              : InkWell(
                                                  child: SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.05,
                                                    child: const Icon(
                                                      Icons.add,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                  onTap: () async {
                                                    int quantity = int.parse(
                                                        _listproduct[index]
                                                            ["Amount"]);
                                                    int price = int.parse(
                                                        _listproduct[index]
                                                            ["price"]);
                                                    quantity++;
                                                    int? newPrice =
                                                        price * quantity;
                                                    await dbHelper.update({
                                                      "id": _listproduct[index]
                                                          ["id"],
                                                      "p_id":
                                                          _listproduct[index]
                                                              ["p_id"],
                                                      "image":
                                                          _listproduct[index]
                                                              ["image"],
                                                      "price":
                                                          _listproduct[index]
                                                              ["price"],
                                                      "Amount": quantity
                                                    }).then((value) async {
                                                      quantity = 0;
                                                      newPrice = 0;
                                                      await readData();
                                                    }).onError(
                                                        (error, stackTrace) {
                                                      print(error.toString());
                                                    });
                                                    setState(() {});
                                                  },
                                                ),
                                        ),
                                      ],
                                    ),
                                    MediumTextBold(
                                      text:
                                          'ل س${double.parse(_listproduct[index]['price'])}',
                                      color: ColorManager.grey1,
                                    )
                                  ],
                                ),
                                trailing: IconButton(
                                  icon: const Icon(Icons.delete),
                                  onPressed: () async {
                                    int quantity = int.parse(
                                        _listproduct[index]["Amount"]);
                                    int price =
                                        int.parse(_listproduct[index]["price"]);
                                    totalPrice = totalPrice -
                                        (double.parse(
                                            (price * quantity).toString()));
                                    int delete = await dbHelper
                                        .delete(_listproduct[index]['id']);
                                    if (delete > 0) {
                                      _listproduct.removeWhere((element) =>
                                          element['id'] ==
                                          _listproduct[index]['id']);
                                      setState(() {});
                                      BlocProvider.of<CartBloc>(context)
                                          .add(UpdateCartIconCount());
                                    }
                                  },
                                ),
                              ),
                            );
                          }),

                ],
              ),
            ),
          ),


      // bottomNavigationBar: BottomNavBa(
      //   pageIndex: 8,
      // ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: ColorManager.primary,
            ),
            padding: const EdgeInsets.all(24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'السعر الاجمالي',
                      style: TextStyle(
                          fontFamily: 'Cairo',
                          color: ColorManager.grey1,
                          fontSize: 18.sp),
                    ),

                    const SizedBox(height: 8),
                    //total price

                    Text(
                      '${totalPrice.toStringAsFixed(2)}',
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold,
                        color: ColorManager.grey1,
                      ),
                    ),
                  ],
                ),

                // pay now
                if (_listproduct.isNotEmpty)
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.green.shade200),
                      borderRadius: BorderRadius.circular(28),
                    ),
                    padding: const EdgeInsets.all(12),
                    child: InkWell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'الدفع',
                            style: TextStyle(
                                color: ColorManager.grey1,
                                fontSize: 17.sp,
                                fontFamily: 'Cairo'),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 16.sp,
                            color: ColorManager.grey1,
                          ),
                        ],
                      ),
                      onTap: () {
                        check_login();

                        // Navigator.of(context).pushNamedAndRemoveUntil('/CheckoutScreen',(Route<dynamic> route) => false);
                      },
                    ),
                  ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
