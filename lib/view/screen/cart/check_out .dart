import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:meerstore/view/screen/cart/cart_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../bloc/cart_bloc.dart';
import '../../../core/constant/color_manger.dart';
import '../../../core/location/map.dart';
import '../../../data/data_source/DBHelper.dart';
import '../../../data/data_source/login _servece.dart';
import '../../../data/model/cart_it.dart';

import '../../widget/auth/medium_text.dart';

import '../../../core/constant/assets_Widget.dart';
import '../../widget/auth/medium_txt_blod.dart';

class CheckoutScreen extends StatefulWidget {
  static const routeName = "/checkoutScreen";
  @override
  State<CheckoutScreen> createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  DatabaseHelper dbHelper = DatabaseHelper();
  User_controller usercontroller = new User_controller();
  final AssetWidget _assetWidget = AssetWidget();

  //
  String address = "";
  String mobile = '';
  int accountNumber = 0;

  //
  bool is_user_login_loading = true;
  bool is_login = false;
  bool is_login_p = false;
  bool is_login_p1 = false;

  //
  List<Map<String, dynamic>> _getcartpro = [];
  List<cartIt> _cartp = [];

  // Check if the user is logged in or not
  check_login() async {
    is_login = await usercontroller.check_login();
    if (is_login = true) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final _mobile = prefs.getString('mobile') ?? 0;
      final _address = prefs.getString('address') ?? 0;
      final _accountNumber = prefs.getInt('accountNm') ?? 0;
      setState(() {
        address = _address.toString();
        mobile = _mobile.toString();
        accountNumber = _accountNumber;
        is_login_p = true;
      });
    } else {}
  }

  // Bring products from the database and put them in the list _cartp
  // Future<List<cartIt>> get_order_list() async {
  //     _getcartpro = await dbHelper.queryAllRows();
  //     for( int i =0 ; i< _getcartpro.length ; i++){
  //       final cartIt mycart = cartIt(
  //           productId:_getcartpro[i]['p_id'].toString(),
  //           amount:int.parse(_getcartpro[i]['Amount']) ,
  //           price: int.parse(_getcartpro[i]['price']),
  //       );
  //       _cartp.add(mycart);
  //       print(mycart);
  //       String jsonTags = jsonEncode(_cartp);
  //     }
  //     return _cartp;
  // }

  Future<List<cartIt>> get_order_list() async {
    _getcartpro = await dbHelper.queryAllRows();
    // print(_getcartpro);
    for (int i = 0; i < _getcartpro.length; i++) {
      final cartIt mycart = cartIt(
        productId: _getcartpro[i]['p_id'].toString(),
        amount: int.parse(_getcartpro[i]['Amount']),
        price: int.parse(_getcartpro[i]['price']),
      );
      _cartp.add(mycart);

      String jsonTags = jsonEncode(_cartp);
    }
    return _cartp;
  }

  // sender  user order
  SendOrder() async {
    var result = await usercontroller.user_Order(
        mobile, address, accountNumber, DateTime.now(), _cartp);

    if (result = true) {
      _assetWidget.AlertDial(context, 'تقرير الطلب', ' تم ارسال الطلب بنجاح');
      await dbHelper.deleteAll();
      BlocProvider.of<
          CartBloc>(
          context)
          .add(
          UpdateCartIconCount());

      // print("success ");
    } else {
      _assetWidget.AlertDia(context, 'تقرير الطلب',
          ' لم يتم ارسال الطلب بنجاح تأكد من صحة البيانات ');
      // print("not success");
    }
  }

  void _awaitReturnValueFromSecondScreen(BuildContext context) async {
    // start the SecondScreen and wait for it to finish with a result
    final result = await showModalBottomSheet(
      context: context,
      builder: ((builder) => bottomSheet()),
    );
    // after the SecondScreen result comes back update the Text widget with it
    setState(() {
      mobile = result!.toString();
      print(mobile);
    });
  }

  void _awaitReturnValueFromMapScreen(BuildContext context) async {
    // start the SecondScreen and wait for it to finish with a result
    final result = await showModalBottomSheet(
      context: context,
      builder: ((builder) => bottomSheetAdrees()),
    );
    // after the SecondScreen result comes back update the Text widget with it
    setState(() {
      address = result!.toString();
    });
  }

  @override
  void initState() {
    check_login();
    get_order_list();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorManager.grey1,
        title: MediumText(
          text: 'عربة التسوق',
          color: Colors.white,
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(Icons.arrow_back),
        ),
      ),
      body: Stack(
        children: [
          SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: MediumText(
                      text: "عنوان التسليم", color: ColorManager.grey1),
                ),
                const SizedBox(
                  height: 10,
                ),

                // address
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Container(
                    decoration: const BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        MediumText(
                          text: address.toString(),
                          color: ColorManager.grey1,
                        ),
                        TextButton(
                          onPressed: () {
                            _awaitReturnValueFromMapScreen(context);
                            // Navigator.push(context, MaterialPageRoute(
                            //     builder: (context) =>MyLocation()));
                          },
                          child: Text(
                            "تغيير",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: ColorManager.grey1,
                                fontFamily: 'Cairo',
                                fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Container(
                    decoration: const BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        MediumText(
                          text: mobile.toString(),
                          color: ColorManager.grey1,
                        ),
                        TextButton(
                          onPressed: () {
                            _awaitReturnValueFromSecondScreen(context);
                          },
                          child: Text(
                            "تغيير",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                                color: ColorManager.grey1,
                                fontFamily: 'Cairo'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ), //mobile
                const SizedBox(
                  height: 20,
                ),

                //send Order
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: SizedBox(
                      height: 50,
                      width: double.infinity,
                      child: TextButton(
                        child: Text(
                          "ارسال الطلب",
                          style: TextStyle(
                              fontSize: 24,
                              fontFamily: 'Cairo',
                              color: ColorManager.grey1),
                        ),
                        onPressed: () async {
                          SendOrder();
                          setState(() {});
                        },
                      ),
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class bottomSheet extends StatefulWidget {
  const bottomSheet({Key? key}) : super(key: key);

  @override
  State<bottomSheet> createState() => _bottomSheetState();
}

class _bottomSheetState extends State<bottomSheet> {
  final AssetWidget _assetWidget = AssetWidget();
  final TextEditingController _mobileController = TextEditingController();

  void _sendDataBack(
    BuildContext context,
  ) {
    String textToSendBack = _mobileController.text;
    Navigator.pop(context, textToSendBack);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 200.sp,
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Column(
          children: <Widget>[
            MediumTextBold(text: "ادخل رقم الهاتف ", color: ColorManager.grey1),
            SizedBox(
              height: 10.sp,
            ),
            _assetWidget.AuthTextField(
              _mobileController,
              "رقم الهاتف",
              false,
              TextInputType.emailAddress,
              TextStyle(
                fontSize: 24,
                fontFamily: 'Cairo',
                color: ColorManager.grey1,
              ),
            ),
            SizedBox(
              height: 30.sp,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: ColorManager.grey1),
              child: Text(
                'حفظ',
                style: TextStyle(fontSize: 24, fontFamily: 'Cairo'),
              ),
              onPressed: () {
                _sendDataBack(context);
              },
            )
          ],
        ),
      ),
    );
  }
}

class bottomSheetAdrees extends StatefulWidget {
  const bottomSheetAdrees({Key? key}) : super(key: key);

  @override
  State<bottomSheetAdrees> createState() => _bottomSheetAdreesState();
}

class _bottomSheetAdreesState extends State<bottomSheetAdrees> {
  TextEditingController textFieldController = TextEditingController();

  final AssetWidget _assetWidget = AssetWidget();

  void _sendDataBack(
    BuildContext context,
  ) {
    String textToSendBack = textFieldController.text;
    Navigator.pop(context, textToSendBack);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 200.sp,
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Column(
          children: <Widget>[
            MediumTextBold(
              text: "ادخل عنوانك ",
              color: ColorManager.grey1,
            ),
            SizedBox(
              height: 10.sp,
            ),
            _assetWidget.AuthTextField(
              textFieldController,
              "العنوان",
              false,
              TextInputType.emailAddress,
              TextStyle(
                fontSize: 24,
                fontFamily: 'Cairo',
                color: ColorManager.grey1,
              ),
            ),
            SizedBox(
              height: 30.sp,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: ColorManager.grey1),
              child: const Text(
                'حفظ',
                style: TextStyle(fontSize: 24, fontFamily: 'Cairo'),
              ),
              onPressed: () {
                _sendDataBack(context);
              },
            )
          ],
        ),
      ),
    );
  }
}
