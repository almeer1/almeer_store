import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:meerstore/view/screen/category/category.dart';
import 'package:meerstore/view/widget/auth/big_text.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/home/banner.dart';
import 'package:meerstore/view/widget/home/home_category.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../core/constant/assets_Widget.dart';
import '../../../core/constant/bottom_bar.dart';
import '../../../data/data_source/DBHelper.dart';
import '../../../data/data_source/api_controller.dart';
import '../../widget/home/recommend.dart';

class AllCatgory extends StatefulWidget {
  const AllCatgory({Key? key}) : super(key: key);

  @override
  State<AllCatgory> createState() => _AllCatgoryState();
}

class _AllCatgoryState extends State<AllCatgory> {
  int _selectedPageIndex = 0;

  @override
  void initState() {
    // _selectedPageIndex;
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var lang = AppLocalizations.of(context);
    var mq = MediaQuery.of(context);

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const HomeBanner(), //scoralimage
          const SizedBox(
            height: 20,
            // height: MediaQuery
            //   .of(context)
            //   .size
            //   .height * 0.01,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16, left: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MediumTextBold(
                  text: "التصنيفات",
                  color: Colors.black87,
                ),
                InkWell(
                  child: MediumTextBold(
                    text: "عرض الكل",
                    color: Colors.black87,
                  ),
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BottomNav(id: 2)));
                  },
                ),
              ],
            ),
          ),
          SizedBox(
              // height: 9,
              height: MediaQuery.of(context).size.height * 0.01),
          Container(
            // height: categoryHeight,
            height: 300,
            // height: MediaQuery.of(context).size.height * 0.37,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(16),
            child: const HomeCategory(), // catgory
          ),
          const SizedBox(
            height: 0.09,
          ),
        ],
      ),
    );
  }
}

class Logic {
  late BuildContext context;
  Logic(BuildContext con) {
    context = con;
  }
  int convertToInt(double number_double) {
    double multiplier = -5;
    return (multiplier * number_double).round();
  }
}
