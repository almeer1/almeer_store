import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

import 'package:meerstore/core/constant/color_manger.dart';

import 'package:meerstore/view/screen/category/type_category.dart';

import '../../../core/app_bar.dart';


import '../../../core/constant/bottomnav_bar.dart';
import '../../../data/data_source/api_controller.dart';
import '../../../data/model/category_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:dio_http_cache/dio_http_cache.dart';

class Categories extends StatefulWidget {

  const Categories({Key? key}) : super(key: key);

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  // Cardd _card=Cardd();
  TextEditingController textController = TextEditingController();
  PageController _pagecontroller = PageController();
  bool _isloading = true;
  var dio = Dio();

  ApiController api = new ApiController();
  List<category> _category = [];
  List parent =[];


  // fetch Category from Api
  Future<void> getCategory() async {
    var response = await api.getCategory();
    for (var post in response.data) {
      _category.add(category.fromJson(post));
  setState(() {
  parent = _category.where((e) => e.categoryId !=e.parent).toList();
   });

      setState(() {
        _isloading=false;
      });
    }
    // _category.removeWhere((e) =>e.categoryId !=e.parent );
  }

  @override
  void initState() {
    getCategory();
    // TODO: implement initState
    super.initState();
  }
  @override
  void dispose() {
    getCategory();
    super.dispose();
  }
  int tappedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorManager.white,
      appBar: AppBarWidget(
        appBar: AppBar(),
      ),
      body: SingleChildScrollView(
        child:  _isloading
            ? SizedBox(
          height: 200,
             child: Center(
              child: LoadingAnimationWidget.threeRotatingDots(
              size: 64, color: ColorManager.grey1,
            ),
          ),
        ): Column(

          children: [
            Center(
              child:  Column(
                children: [
                 Row(
                 crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [

                      Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        height: MediaQuery.of(context).size.height - AppBarWidget(appBar: AppBar()).preferredSize.height-80,
                        //   width:45,
                        // height:500,
                        color: Colors.grey,
                        child: ListView.builder(
                            padding: const EdgeInsets.all(0),
                            // physics: const BouncingScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount:parent.length,
                            itemBuilder: (context, index) {
                               return Container(
                                   decoration: BoxDecoration(
                                     color: tappedIndex == index
                                         ? const Color(0xFFf7f7f7)
                                         : Colors.white,
                                     border:  Border(
                                       bottom: BorderSide(
                                           width: 0.6, color: ColorManager.grey1),
                                     ),
                                   ),
                                   child: ListTile(
                                       minVerticalPadding: 5.0,
                                       title: Center(

                                         child:
                                           Text(parent[index].title.toString(),
                                           textAlign: TextAlign.center,
                                           style: TextStyle(
                                               height: 1.5,
                                               fontSize: 14.sp,
                                               fontFamily: 'Cairo',
                                               color: ColorManager.grey1),
                                         ),
                                       ),
                                       onTap: () {
                                         _pagecontroller.jumpToPage(index);
                                         // _pagecontroller.animateToPage(index, duration: Duration(milliseconds: 400), curve: Curves.bounceInOut,);
                                         setState(() {
                                           tappedIndex = index;
                                         });
                                       }));


                              }
                          ),
                      ),
                      Flexible(
                        child: Column(
                          children: [
                            Container(
                              // height:500,
                              // width: 300,
                              height:MediaQuery.of(context).size.height - AppBarWidget(appBar: AppBar()).preferredSize.height-80,
                              // MediaQuery.of(context).size.height * 0.9,
                              width: MediaQuery.of(context).size.width * 0.9,
                              color: const Color(0xFFf7f7f7),
                              child: PageView(
                                  controller: _pagecontroller,
                                  onPageChanged: (value) {
                                    setState(() {
                                      tappedIndex = value;
                                    });
                                  },
                                  physics:
                                      const NeverScrollableScrollPhysics(),
                                  children: [
                                    for (int i = 0; i < parent.length; i++)
                                      parent[i]
                                          .categoryId!=  parent[i]
                                          .parent ?TypeCateg(
                                          //cubit[index].category.
                                          id: parent[i]
                                              .categoryId
                                              .toString()):
                                     Center(
                                       child: Text('قريبا',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        height: 1.5,
                                         fontSize: 50.sp,
                                         fontFamily: 'Cairo',
                                           color: ColorManager.grey1),
    ),
                                     ),
                                  ]),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
       // bottomNavigationBar: BottomNavBa(pageIndex: 2,),


    );
  }
}
