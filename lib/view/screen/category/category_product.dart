import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:badges/badges.dart' as badges;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import '../../../bloc/cart_bloc.dart';
import '../../../core/app_bar.dart';

import '../../../data/data_source/DBHelper.dart';
import '../../../data/model/categoryproduct_model.dart';

import '../../widget/auth/medium_text.dart';
import '../../widget/home/home_category.dart';
import '../cart/cart_screen.dart';
import '../details/details_page.dart';
import '../favorite/favourit_iem.dart';

class TypeCateg2 extends StatefulWidget {
  String id;
  TypeCateg2({Key? key, required this.id}) : super(key: key);

  @override
  _TypeCateg2State createState() => _TypeCateg2State();
}

class _TypeCateg2State extends State<TypeCateg2> {
  var dio = Dio();
  DatabaseHelper dbHelper = DatabaseHelper();

  List<categoryProduct> _categoryPro = [];
  bool _isloading = true;
  bool _isFavorit = false;
  List<Map<String, dynamic>> _getproduct = [];
  List<Map<String, dynamic>> _getfavorite = [];

  // fetch categoryproduct from Api
  Future<void> getCategoryProduct({required String categoryId}) async {
    try {
      print(categoryId);
      final response = await dio.get(
          'https://mapi.orasys.org/public/api/products/categoryProducts/$categoryId');
      if (response.statusCode == 200) {
        for (var post in response.data) {
          print(response.data);
          setState(() {
            _categoryPro.add(categoryProduct.fromJson(post));
            _isloading = false;
          });
        }
      }
    } catch (e) {
      print(e);
    }
  }

  void initState() {
    getCategoryProduct(categoryId: widget.id);
    super.initState();
  }
  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      centerTitle: true,
      actions: [
        Row(
          children: [
            IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) =>  FavouritItem()));
              },
              icon:  Icon(
                FontAwesomeIcons.heart,
                color: ColorManager.grey1,
                size: 30,
              ),
            ),

            badges.Badge(
              badgeContent: BlocBuilder<CartBloc, CartState>(
                builder: (context, state) {
                  if (state is CartInitial) {
                    return const Text(
                      '0',
                      style: TextStyle(color: Colors.blueGrey, fontSize: 20),
                    );
                  } else if (state is CounterValueChangeState) {
                    return Text(
                      state.counter.toString(),
                      style: const TextStyle(color: Colors.blueGrey, fontSize: 20),
                    );
                  } else {
                    return const SizedBox(
                      height: 2,
                    );
                  }
                },
              ),
              position: badges.BadgePosition.topEnd(top: 0, end: 3),
              badgeStyle: badges.BadgeStyle(
                badgeColor: ColorManager.primary,
              ),

              child: IconButton(
                iconSize: 30.sp,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => const CartScreen()));
                },
                icon:  Icon(
                  Icons.shopping_cart,
                  color: ColorManager.grey1,
                ),
              ),
            ),

          ],
        ),
      ],
      leading: IconButton(
        iconSize: 30.sp,
        onPressed: () {
          Navigator.pop(context);
        },
        icon:  Icon(
          Icons.arrow_back,
          color:ColorManager.grey1,
        ),
      ),

    );
  }

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(122),
        child:  buildAppBar(context),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Column(children: [
            _isloading
                ? const Center(child: CircularProgressIndicator())
                : Column(
                    children: [
                      // for(int i=0;i<_categoryPro.length;i++)
                      // MediumText(text:_categoryPro[i].category.toString(), color:Colors.black),
                      Center(
                          child: GridView.builder(
                              shrinkWrap: true,
                              primary: false,
                              padding: const EdgeInsets.all(20),
                              gridDelegate: (mq.size.width < 370)
                                  ? const SliverGridDelegateWithFixedCrossAxisCount(
                                      childAspectRatio:
                                          0.60, //heigh of container
                                      crossAxisSpacing: 10,
                                      mainAxisSpacing: 10,
                                      crossAxisCount: 2)
                                  : const SliverGridDelegateWithFixedCrossAxisCount(
                                      childAspectRatio:
                                          0.64, //size of container
                                      crossAxisSpacing: 5,
                                      mainAxisSpacing: 10,
                                      crossAxisCount: 2),
                              itemCount: _categoryPro.length,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                    child: Container(
                                        // width: MediaQuery.of(context).size.width*0.5,
                                        // height: MediaQuery.of(context).size.height*0.04,
                                        // width: 250,
                                        // height: 100,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius:
                                                BorderRadius.circular(0),
                                            color: Colors.white),
                                        child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Stack(
                                                children: [
                                                  InkWell(
                                                    onTap: () {
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) =>
                                                                      DetailsPage(
                                                                        id: _categoryPro[index]
                                                                            .productId
                                                                            .toString(),
                                                                      )));
                                                      // print(__products[index].productId);
                                                    },
                                                    child: Center(
                                                        child: Image.network(
                                                            "https://mapi.orasys.org/images/512/${_categoryPro[index].image}",
                                                            fit: BoxFit
                                                                .fitWidth)),
                                                  ),
                                                  Positioned(
                                                    right: 2.sp,
                                                    top: 2.sp,
                                                    child: IconButton(
                                                      icon: const Icon(Icons.favorite,
                                                          color: Colors.white),
                                                      onPressed: () async {
                                                        // get data from database
                                                        _getfavorite =
                                                            await dbHelper
                                                                .queryAllRowsf();
                                                        bool k = false;
                                                        if (_getfavorite
                                                                .length >
                                                            0) {
                                                          for (int i = 0;
                                                              i <
                                                                  _getfavorite
                                                                      .length;
                                                              i++) {
                                                            if (_categoryPro[
                                                                        index]
                                                                    .productId ==
                                                                _getfavorite[i]
                                                                    ["p_id"]) {
                                                              k = true;
                                                              await dbHelper
                                                                  .updatef(
                                                                {
                                                                  "id":
                                                                      _getfavorite[
                                                                              i]
                                                                          [
                                                                          "id"],
                                                                  "p_id":
                                                                      _getfavorite[
                                                                              i]
                                                                          [
                                                                          "p_id"],
                                                                  "productName":
                                                                      _getfavorite[
                                                                              i]
                                                                          [
                                                                          "productName"],
                                                                  "description":
                                                                      _getfavorite[
                                                                              i]
                                                                          [
                                                                          'description'],
                                                                  "image":
                                                                      _getfavorite[
                                                                              i]
                                                                          [
                                                                          "image"],
                                                                  "price":
                                                                      _getfavorite[
                                                                              i]
                                                                          [
                                                                          "price"],
                                                                  "Amount": 1,
                                                                  "favorite":
                                                                      "False",
                                                                  "color":
                                                                      "white"
                                                                },
                                                              ).then((value) {
                                                                const snackBar = SnackBar(
                                                                    backgroundColor:
                                                                        Colors
                                                                            .red,
                                                                    content: Text(
                                                                        'تمت إضافة المنتج بالفعل في المفضلة'),
                                                                    duration: Duration(
                                                                        seconds:
                                                                            1));
                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                        snackBar);
                                                              });
                                                            }
                                                          }
                                                        }
                                                        if (k == false) {
                                                          await dbHelper
                                                              .insertF({
                                                            "p_id":
                                                                _categoryPro[
                                                                        index]
                                                                    .productId,
                                                            "productName":
                                                                _categoryPro[
                                                                        index]
                                                                    .productAr,
                                                            "description":
                                                                _categoryPro[
                                                                        index]
                                                                    .description,
                                                            "image":
                                                                "https://mapi.orasys.org/images/512/${_categoryPro[index].image}",
                                                            "price":
                                                                _categoryPro[
                                                                        index]
                                                                    .price,
                                                            "Amount": 1,
                                                            "favorite": "True",
                                                            "color": "red"
                                                          }).then((value) {
                                                            const snackBar =
                                                                SnackBar(
                                                              backgroundColor:
                                                                  Colors.green,
                                                              content: Text(
                                                                  'تمت إضافة المنتج للمفضلة'),
                                                              duration:
                                                                  Duration(
                                                                      seconds:
                                                                          1),
                                                            );
                                                            ScaffoldMessenger
                                                                    .of(context)
                                                                .showSnackBar(
                                                                    snackBar);
                                                          });

                                                          // for(int i =0 ; i< _getfavorite.length ; i++){
                                                          //   if (_getfavorite[i].containsKey("p_id")) {
                                                          //     if (_getfavorite[i]["p_id"] == _categoryPro[index]['productId']) {
                                                          //       isColor = _getfavorite[i]["favorite"];
                                                          //       print(isColor);
                                                          //     }
                                                          //   }
                                                          // }
                                                        }

                                                        setState(() {
                                                          //   for(int i =0 ; i< _getfavorite.length ; i++){
                                                          //   // print(_getfavorite.map((e) {e["p_id"] == _categoryPro[index]['productId'];
                                                          //   // }  ).toList());
                                                          //   if (_getfavorite[i].containsKey("p_id")) {
                                                          //     if (_getfavorite[i]["p_id"] == _categoryPro[index]['productId']) {
                                                          //       isColor = _getfavorite[i]["favorite"];
                                                          //       print(isColor);
                                                          //     }
                                                          //   }
                                                          // }
                                                        });
                                                      },
                                                    ),
                                                  ),
                                                  Positioned(
                                                      left: 0,
                                                      // top: 5,
                                                      bottom: 130.sp,
                                                      // right: 58,
                                                      // top:0.2,
                                                      child: RotationTransition(
                                                          turns:
                                                              const AlwaysStoppedAnimation(
                                                                  -43 / 360),
                                                          child: Container(
                                                            height: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height *
                                                                0.02,
                                                            // width: MediaQuery.of(context).size.width*0.01,
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            2),
                                                                color: ColorManager.grey1),
                                                            child: _categoryPro[
                                                                            index]
                                                                        .amount ==
                                                                    '0'
                                                                ? Center(
                                                                    child:
                                                                        MediumText(
                                                                    text:
                                                                        'Out of stock',
                                                                    color: Colors
                                                                        .white70,
                                                                  ))
                                                                : _categoryPro[index]
                                                                            .amount ==
                                                                        _categoryPro[index]
                                                                            .minAmount
                                                                    ? Center(
                                                                        child:
                                                                            MediumText(
                                                                        text:
                                                                            'Out of stock',
                                                                        color: Colors
                                                                            .white70,
                                                                      ))
                                                                    : null,
                                                          ))),
                                                ],
                                              ),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  Container(
                                                    padding:
                                                        const EdgeInsets.all(8),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment.end,
                                                      children: [
                                                        Text(
                                                          _categoryPro[index]
                                                              .productAr
                                                              .toString(),
                                                          style:
                                                               TextStyle(
                                                            fontSize: 16,
                                                            fontFamily: 'Cairo',
                                                            color:
                                                            ColorManager.grey1,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                        ),

                                                        _categoryPro[index]
                                                                    .description ==
                                                                null
                                                            ? Text('')
                                                            : Text(
                                                                _categoryPro[
                                                                        index]
                                                                    .description
                                                                    .toString(),
                                                                style:
                                                                     TextStyle(
                                                                  fontSize: 16,
                                                                  fontFamily:
                                                                      'Cairo',
                                                                  color: ColorManager.grey1,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                ),
                                                              ),

                                                        // SizedBox(height:0.1),
                                                        Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .end,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            MediumText(
                                                              text: _categoryPro[
                                                                      index]
                                                                  .price
                                                                  .toString(),
                                                              color:  ColorManager.grey1,
                                                            ),
                                                            Container(
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              2),
                                                                  color: ColorManager.primary),
                                                              child: _categoryPro[
                                                                              index]
                                                                          .amount ==
                                                                      '0'
                                                                  ? null
                                                                  : InkWell(
                                                                      onTap:
                                                                          () async {
                                                                        // get data from database
                                                                        _getproduct =
                                                                            await dbHelper.queryAllRows();

                                                                        bool k =
                                                                            false;
                                                                        if (_getproduct.length >
                                                                            0) {
                                                                          for (int i = 0;
                                                                              i < _getproduct.length;
                                                                              i++) {
                                                                            if (_categoryPro[index].productId ==
                                                                                _getproduct[i]["p_id"]) {
                                                                              k = true;
                                                                              await dbHelper.update(
                                                                                {
                                                                                  "id": _getproduct[i]["id"],
                                                                                  "p_id": _getproduct[i]["p_id"],
                                                                                  "productName": _getproduct[i]["productName"],
                                                                                  "image": _getproduct[i]["image"],
                                                                                  "price": _getproduct[i]["price"],
                                                                                  "Amount": int.parse(_getproduct[i]["Amount"]) + 1,
                                                                                },
                                                                              ).then((value) {
                                                                                const snackBar = SnackBar(backgroundColor: Colors.red, content: Text('تمت إضافة المنتج بالفعل في سلة التسوق'), duration: Duration(seconds: 1));
                                                                                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                                                              });
                                                                            }
                                                                          }
                                                                        }
                                                                        if (k ==
                                                                            false) {
                                                                          await dbHelper
                                                                              .insert({
                                                                            "p_id":
                                                                                _categoryPro[index].productId,
                                                                            "productName":
                                                                                _categoryPro[index].productAr,
                                                                            "image":
                                                                                "https://mapi.orasys.org/images/512/${_categoryPro[index].image}",
                                                                            "price":
                                                                                _categoryPro[index].price,
                                                                            "Amount":
                                                                                1,
                                                                          }).then((value) {
                                                                            const snackBar =
                                                                                SnackBar(
                                                                              backgroundColor: Colors.green,
                                                                              content: Text('تمت إضافة المنتج للسلة'),
                                                                              duration: Duration(seconds: 1),
                                                                            );
                                                                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                                                          });
                                                                        }
                                                                        BlocProvider.of<CartBloc>(context)
                                                                            .add(UpdateCartIconCount());
                                                                        setState(
                                                                            () {});
                                                                      },
                                                                      child:
                                                                          const Icon(
                                                                        Icons
                                                                            .add,
                                                                        color: Colors
                                                                            .white,
                                                                      ),
                                                                    ),
                                                            )
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ])));
                              })),
                    ],
                  ),
          ]),
        ),
      ),

    );
  }
}
