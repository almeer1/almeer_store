import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';

import 'package:provider/provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../bloc/cart_bloc.dart';
import '../../../core/constant/bottom_bar.dart';
import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/DBHelper.dart';

import '../../../data/data_source/api_controller.dart';
import '../../../data/model/categoryproduct_model.dart';

import '../../widget/auth/medium_text.dart';
import '../details/details_page.dart';

import 'package:dio_http_cache/dio_http_cache.dart';

class TypeCateg extends StatefulWidget {
  String id;
  TypeCateg({Key? key, required this.id}) : super(key: key);

  @override
  _TypeCategState createState() => _TypeCategState();
}

class _TypeCategState extends State<TypeCateg> {
  ApiController api = new ApiController();
  DatabaseHelper dbHelper = DatabaseHelper();
  final scrollerController = ScrollController();
  var dio = Dio();

  List<Map<String, dynamic>> _getproduct = [];
  // List<categoryProduct> _categoryPro = [];
  bool _isloading = true;
  bool _isFavorit = false;
  bool isloading = true;

  int page = 1;
  final int per_page = 10;
  bool isLoadingMore = false;

  List _listFavoriteproduct = [];

  readData() async {
    _getfavorite = await dbHelper.queryAllRowsf();
  }

  List<Map<String, dynamic>> _getfavorite = [];
  List _categoryPro = [];

  // fetch Categoryproduct from Api
  Future<void> getCategoryProduct({required String categoryId}) async {
    DioCacheManager dioCacheManager = DioCacheManager(CacheConfig());
    Options myOptions =
        buildCacheOptions(const Duration(days: 30), forceRefresh: true);
    dio.interceptors.add(dioCacheManager.interceptor);
    // print(categoryId);
    String url =
        'https://mapi.orasys.xyz/api/products/categoryProducts/$categoryId?&page=$page';
    // 'https://mapi.orasys.org/public/api/products/categoryProducts/$categoryId?&page=$page';
    final response = await dio.get(url, options: myOptions);
    if (response.statusCode == 200) {
      setState(() {
        _categoryPro = _categoryPro + response.data['data'];
        _isloading = false;
      });
    }

    // for (var post in response.data) {
    //   // print(post);
    //   setState(() {
    //     _categoryPro.add(categoryProduct.fromJson(post));
    //     _isloading=false;
    //   });
    //
    //   print(_categoryPro.length);
    // }
  }

  Future<void> _scorllListener() async {
    if (isLoadingMore) return;
    if (scrollerController.position.pixels ==
        scrollerController.position.maxScrollExtent) {
      setState(() {
        isLoadingMore = true;
      });
      page = page + 1;
      await getCategoryProduct(categoryId: widget.id);
      setState(() {
        isLoadingMore = false;
      });
    }
    // fulttergetAllProduct();
    // print('scorlercontroller called');
  }

  @override
  void initState() {
    scrollerController.addListener(_scorllListener);
    getCategoryProduct(categoryId: widget.id);
    readData();
    super.initState();
  }

  @override
  void dispose() {
    scrollerController.addListener(_scorllListener);
    getCategoryProduct(categoryId: widget.id);
    readData();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);

    return SingleChildScrollView(
        controller: scrollerController,
        scrollDirection: Axis.vertical,
        child: Column(children: [
          _isloading
              ? SizedBox(
                  height: 200,
                  child: Center(
                    child: LoadingAnimationWidget.threeRotatingDots(
                      size: 64,
                      color: ColorManager.grey1,
                    ),
                  ),
                )
              : Center(
                  child: GridView.builder(
                      shrinkWrap: true,
                      primary: false,
                      padding: const EdgeInsets.all(16),
                      gridDelegate: (mq.size.width < 370)
                          ? const SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 0.70, //heigh of container
                              crossAxisSpacing: 10,
                              mainAxisSpacing: 10,
                              crossAxisCount: 2)
                          : const SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 0.60,
                              crossAxisSpacing: 5,
                              mainAxisSpacing: 10,
                              crossAxisCount: 2),
                      itemCount: isLoadingMore
                          ? _categoryPro.length + 1
                          : _categoryPro.length,
                      itemBuilder: (BuildContext context, int index) {
                        // print( 'https://mapi.orasys.org/images/${_products[index].image}');
                        if (index < _categoryPro.length) {
                          return GestureDetector(
                              child: InkWell(
                            onTap: () {},
                               child: Container(
                                width: MediaQuery.of(context).size.width * 0.3,
                                 height: MediaQuery.of(context).size.height * 0.7,
                                 decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.grey, width: 0.2),
                                  borderRadius: BorderRadius.circular(0),
                                  color: Colors.white),
                                child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Stack(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailsPage(
                                                        id: _categoryPro[index]
                                                                ['productId']
                                                            .toString(),
                                                      )));
                                        },
                                        child: Center(
                                            child: _categoryPro[index]
                                                        ['image'] ==
                                                    ''
                                                ? Image.asset(
                                                    'assets/images/juices.jpg',
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.5,
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.140,
                                                    // width: 350,
                                                    // height: 120,
                                                    fit: BoxFit.fill)
                                                : Image.network(
                                                    "https://mapi.orasys.org/images/512/${_categoryPro[index]['image']}",

                                                    // width: 350,
                                                    // height: 120,
                                                    fit: BoxFit.fill)),
                                      ),

                                      //addfavourit
                                      Positioned(
                                        right: 0.1,
                                        bottom: 90,
                                        child: IconButton(
                                          icon: Icon(Icons.favorite,
                                              color: ((_getfavorite.where((e) =>
                                                          e['p_id'] ==
                                                          _categoryPro[index]
                                                              ['productId']))
                                                      .map((e) => e['favorite'])
                                                      .any((e) => e == "True"))
                                                  ? Colors.red
                                                  : Colors.white),
                                          onPressed: () async {
                                            // get data from database
                                            _getfavorite =
                                                await dbHelper.queryAllRowsf();
                                            bool k = false;
                                            if (_getfavorite.length > 0) {
                                              for (int i = 0;
                                                  i < _getfavorite.length;
                                                  i++) {
                                                if (_categoryPro[index]
                                                        ['productId'] ==
                                                    _getfavorite[i]["p_id"]) {
                                                  k = true;
                                                  if (_getfavorite[i]
                                                          ["favorite"] ==
                                                      'True') {
                                                    await dbHelper.updatef(
                                                      {
                                                        "id": _getfavorite[i]
                                                            ["id"],
                                                        "p_id": _getfavorite[i]
                                                            ["p_id"],
                                                        "productName":
                                                            _getfavorite[i]
                                                                ["productName"],
                                                        "description":
                                                            _getfavorite[i]
                                                                ['description'],
                                                        "image": _getfavorite[i]
                                                            ["image"],
                                                        "price": _getfavorite[i]
                                                            ["price"],
                                                        "Amount": 1,
                                                        "favorite": 'False',
                                                        "color": "white"
                                                      },
                                                    ).then((value) {
                                                      const snackBar = SnackBar(
                                                        backgroundColor:
                                                            Colors.red,
                                                        content: Text(
                                                            'تمت إزالة المنتج من المفضلة',
                                                            style: TextStyle(
                                                                fontSize: 18,
                                                                fontFamily:
                                                                    'Cairo')),
                                                        duration: Duration(
                                                            milliseconds: 1),
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            10))),
                                                      );

                                                      ScaffoldMessenger.of(
                                                              context)
                                                          .showSnackBar(
                                                              snackBar);
                                                    });
                                                    await readData();
                                                    break;
                                                  } else {
                                                    k = true;
                                                    await dbHelper.updatef({
                                                      "id": _getfavorite[i]
                                                          ["id"],
                                                      "p_id": _getfavorite[i]
                                                          ["p_id"],
                                                      "productName":
                                                          _getfavorite[i]
                                                              ["productName"],
                                                      "description":
                                                          _getfavorite[i]
                                                              ['description'],
                                                      "image": _getfavorite[i]
                                                          ["image"],
                                                      "price": _getfavorite[i]
                                                          ["price"],
                                                      "Amount": 1,
                                                      "favorite": 'True',
                                                      "color": "white"
                                                    }).then((value) {
                                                      const snackBar = SnackBar(
                                                        backgroundColor:
                                                            Colors.green,
                                                        content: Text(
                                                          'تمت إضافة المنتج للمفضلة ',
                                                          style: TextStyle(
                                                              fontSize: 18,
                                                              fontFamily:
                                                                  'Cairo'),
                                                        ),
                                                        duration: Duration(
                                                            milliseconds: 1),
                                                      );
                                                      ScaffoldMessenger.of(
                                                              context)
                                                          .showSnackBar(
                                                              snackBar);
                                                    });
                                                    await readData();
                                                    setState(() {});
                                                    break;
                                                  }
                                                }
                                              }
                                            }
                                            if (k == false) {
                                              await dbHelper.insertF({
                                                "p_id": _categoryPro[index]
                                                    ['productId'],
                                                "productName":
                                                    _categoryPro[index]
                                                        ['productAr'],
                                                "description":
                                                    _categoryPro[index]
                                                        ['description'],
                                                "image":
                                                    "https://mapi.orasys.org/images/512/${_categoryPro[index]['image']}",
                                                "price": _categoryPro[index]
                                                    ['price'],
                                                "Amount": 1,
                                                "favorite": "True",
                                                "color": "red"
                                              }).then((value) {
                                                const snackBar = SnackBar(
                                                  backgroundColor: Colors.green,
                                                  content: Text(
                                                    'تمت إضافة المنتج للمفضلة',
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontFamily: 'Cairo'),
                                                  ),
                                                  duration:
                                                      Duration(milliseconds: 1),
                                                );
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(snackBar);
                                              });
                                              await readData();
                                              setState(() {});
                                            }
                                            setState(() {});
                                          },
                                        ),
                                      ),
                                      Positioned(
                                        left: 0,
                                        // top: 5,
                                        bottom: 85,
                                        // right: 58,
                                        // top:0.2,
                                        child: RotationTransition(
                                          turns: const AlwaysStoppedAnimation(
                                              -43 / 360),
                                          child: Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.03,
                                            // width: MediaQuery.of(context).size.width*0.01,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(2),
                                                color: Colors.blue),
                                            child: _categoryPro[index]
                                                        ['amount'] ==
                                                    '0'
                                                ? Center(
                                                    child: MediumText(
                                                    text: 'Out of stock',
                                                    color: Colors.white70,
                                                  ))
                                                : null,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Flex(direction: Axis.vertical, children: [
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                          padding:
                                              const EdgeInsets.only(right:8),
                                          child: Column(

                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              // for(int i=0; i<products.length; i++)
                                              Text(
                                                _categoryPro[index]![
                                                        'productAr']
                                                    .toString(),
                                                style: TextStyle(
                                                    color: ColorManager.grey1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    fontSize: 15.sp,
                                                    fontWeight: FontWeight.w700,
                                                    fontFamily: 'Cairo'),
                                              ),

                                              _categoryPro[index]
                                                          ['description'] ==
                                                      null
                                                  ? Text('')
                                                  : Text(
                                                      _categoryPro[index]
                                                              ['description']
                                                          .toString(),
                                                      style: TextStyle(
                                                          color: ColorManager
                                                              .grey1,
                                                          fontSize: 15.sp,

                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          fontFamily: 'Cairo'),
                                                   softWrap: false,
                                                   maxLines: 1,
                                                    ),

                                              Row(
                                                crossAxisAlignment: CrossAxisAlignment.end,
                                                mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,

                                                children: [
                                                  MediumText(
                                                      text:
                                                      NumberFormat.currency(locale: 'ar_SY',symbol: '',decimalDigits: 0).format(_categoryPro[index]['price'] ) + ' ل س',
                                                      color:
                                                          ColorManager.grey1),

                                                  Padding(
                                                    padding: const EdgeInsets.only(left:5.0,bottom: 5),
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                2),
                                                        color:
                                                            ColorManager.primary,
                                                      ),
                                                      child:
                                                          _categoryPro[index][
                                                                      'amount'] ==
                                                                  '0'
                                                              ? InkWell(
                                                                  onTap: () {
                                                                    const snackBar =
                                                                        SnackBar(
                                                                      backgroundColor:
                                                                          Colors
                                                                              .green,
                                                                      content:
                                                                          Text(
                                                                        ' عذراً غير متوفرة الآن',
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                18,
                                                                            fontFamily:
                                                                                'Cairo'),
                                                                      ),
                                                                      duration: Duration(
                                                                          seconds:
                                                                              1),
                                                                    );
                                                                    ScaffoldMessenger.of(
                                                                            context)
                                                                        .showSnackBar(
                                                                            snackBar);
                                                                  },
                                                                  child:
                                                                      const Icon(
                                                                    Icons.add,
                                                                    color: Colors
                                                                        .white,
                                                                  ),
                                                                )
                                                              : InkWell(
                                                                  onTap:
                                                                      () async {
                                                                    // get data from database
                                                                    _getproduct =
                                                                        await dbHelper
                                                                            .queryAllRows();
                                                                    bool k =
                                                                        false;
                                                                    if (_getproduct
                                                                            .length >
                                                                        0) {
                                                                      for (int i =
                                                                              0;
                                                                          i < _getproduct.length;
                                                                          i++) {
                                                                        if (_categoryPro[index]
                                                                                [
                                                                                'productId'] ==
                                                                            _getproduct[i]
                                                                                [
                                                                                "p_id"]) {
                                                                          k = true;
                                                                          await dbHelper
                                                                              .update(
                                                                            {
                                                                              "id":
                                                                                  _getproduct[i]["id"],
                                                                              "p_id":
                                                                                  _getproduct[i]["p_id"],
                                                                              "productName":
                                                                                  _getproduct[i]["productName"],
                                                                              "image":
                                                                                  _getproduct[i]["image"],
                                                                              "price":
                                                                                  _getproduct[i]["price"],
                                                                              "Amount":
                                                                                  int.parse(_getproduct[i]["Amount"]) + 1,
                                                                              "MinAmount":
                                                                                  _getproduct[index]['MinAmount'],
                                                                              "Quantety":
                                                                                  _getproduct[index]['Quantety']
                                                                            },
                                                                          ).then((value) {
                                                                            const snackBar = SnackBar(
                                                                                backgroundColor: Colors.red,
                                                                                content: Text('تمت زيادة كمية المنتج', style: TextStyle(fontSize: 18, fontFamily: 'Cairo')),
                                                                                duration: Duration(milliseconds: 250));
                                                                            ScaffoldMessenger.of(context)
                                                                                .showSnackBar(snackBar);
                                                                          });
                                                                        }
                                                                      }
                                                                    }
                                                                    if (k ==
                                                                        false) {
                                                                      await dbHelper
                                                                          .insert({
                                                                        "p_id": _categoryPro[
                                                                                index]
                                                                            [
                                                                            'productId'],
                                                                        "productName":
                                                                            _categoryPro[index]
                                                                                [
                                                                                'productAr'],
                                                                        "image":
                                                                            "https://mapi.orasys.org/images/512/${_categoryPro[index]['image']}",
                                                                        "price": _categoryPro[
                                                                                index]
                                                                            [
                                                                            'price'],
                                                                        "Amount":
                                                                            1,
                                                                        "MinAmount":
                                                                            _categoryPro[index]
                                                                                [
                                                                                'minAmount'],
                                                                        "Quantety":
                                                                            _categoryPro[index]
                                                                                [
                                                                                'amount']
                                                                      }).then((value) {
                                                                        const snackBar =
                                                                            SnackBar(
                                                                          backgroundColor:
                                                                              Colors.green,
                                                                          content: Text(
                                                                              'تمت إضافة المنتج للسلة ',
                                                                              style:
                                                                                  TextStyle(fontSize: 18, fontFamily: 'Cairo')),
                                                                          duration:
                                                                              Duration(milliseconds: 250),
                                                                        );
                                                                        ScaffoldMessenger.of(
                                                                                context)
                                                                            .showSnackBar(
                                                                                snackBar);
                                                                      });
                                                                    }
                                                                    BlocProvider.of<
                                                                                CartBloc>(
                                                                            context)
                                                                        .add(
                                                                            UpdateCartIconCount());
                                                                  },
                                                                  child:
                                                                      const Icon(
                                                                    Icons.add,
                                                                    color: Colors
                                                                        .white,
                                                                  ),
                                                                ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ])
                                ],
                              ),
                            ),
                          ));
                        } else {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                      }))
        ]));
  }
}
