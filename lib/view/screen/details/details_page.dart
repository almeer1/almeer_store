import 'package:badges/badges.dart' as badges;
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:meerstore/core/constant/bottom_bar.dart';

import 'package:meerstore/data/model/product.dart';
import 'package:meerstore/view/screen/cart/cart_screen.dart';
import 'package:meerstore/view/widget/auth/big_text.dart';
import 'package:meerstore/view/widget/auth/medium_text.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/home/recommend.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';
import '../../../bloc/cart_bloc.dart';
import '../../../core/app_bar.dart';
import '../../../core/constant/bottomnav_bar.dart';
import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/DBHelper.dart';

import 'package:dio_http_cache/dio_http_cache.dart';

import '../../../data/data_source/api_controller.dart';
import '../favorite/favourit_iem.dart';
import '../home/home_product.dart';

class DetailsPage extends StatefulWidget {
  final String id;
  const DetailsPage({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  var dio = Dio();
  DatabaseHelper dbHelper = DatabaseHelper();
  ApiController api = new ApiController();

  product? Products;

  int tappedIndex = 0;
  bool choose_amount = false;
  String amount_selected = '1';
  String amount_new = '1';
  var formatter = NumberFormat('#,###.00', 'en_US');
  List<Map<String, dynamic>> _getproduct = [];
  List<Map<String, dynamic>> _getfavorite = [];

  // get product details
  Future<product?> getdetailbyid({required String productId}) async {
    var response = await api.getdetailbyid(productId: productId);
    setState(() {
      Products = product.fromJson(response.data);
    });
  }

  readData() async {
    _getfavorite = await dbHelper.queryAllRowsf();
  }



  @override
  void initState() {
    getdetailbyid(productId: widget.id);
    readData();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    getdetailbyid(productId: widget.id);
    readData();
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      centerTitle: true,
      actions: [

        Row(
          children: [
            // Padding(
            //   padding: const EdgeInsets.only(bottom: 6.0),
            //   child: InkWell(
            //     onTap: () {
            //       Navigator.push(
            //           context,
            //           MaterialPageRoute(
            //               builder: (context) => BottomNav(
            //                     id: 1,
            //                   )));
            //     },
            //     child: Icon(
            //       FontAwesomeIcons.heart,
            //       color: ColorManager.grey1,
            //       size: 30,
            //     ),
            //   ),
            // ),
            badges.Badge(
              badgeContent: BlocBuilder<CartBloc, CartState>(
                builder: (context, state) {
                  if (state is CartInitial) {
                    return const Text(
                      '0',
                      style: TextStyle(color: Colors.blueGrey, fontSize: 20),
                    );
                  } else if (state is CounterValueChangeState) {
                    return Text(
                      state.counter.toString(),
                      style: const TextStyle(color: Colors.blueGrey, fontSize: 20),
                    );
                  } else {
                    return const SizedBox(
                      height: 2,
                    );
                  }
                },
              ),
              badgeStyle: badges.BadgeStyle(
                badgeColor: ColorManager.primary,
              ),
              position: badges.BadgePosition.topEnd(top: 0, end: 0),
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => const CartScreen()));
                },
                child: Icon(
                  Icons.shopping_cart,
                  color: ColorManager.grey1,
                  size: 40,
                ),
              ),
            ),
          ],
        ),

      ],
      leading: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Icon(
          Icons.arrow_back,
          color: ColorManager.grey1,
          size: 45,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarDetWidget(
        appBar: AppBar(
          elevation: 10,
        ),
      ),
      body: Products == null
          ? Center(
              child: SizedBox(
                height: 140,
                child: Center(
                  child: LoadingAnimationWidget.threeRotatingDots(
                    size: 64,
                    color: ColorManager.grey1,
                  ),
                ),
              ),
            )
          : Column(children: [
              SizedBox(
                height: MediaQuery.of(context).size.height -
                    180-
                    MediaQuery.of(context).padding.top,
                child: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: Stack(
                            children:[
                              Container(
                                width: double.infinity,
                                color: Colors.grey,
                                child: Image.network(
                                    "https://mapi.orasys.org/images/512/${Products!.image}",
                                    width: 300,
                                    fit: BoxFit.cover),
                              ),
                              Positioned(
                                right: 16.sp,
                                top: 16.sp,

                                child: InkWell(



                                onTap: () async {
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) => BottomNav(
                                  //           id: 1,
                                  //         )));

                                  // get data from database
                                  _getfavorite = await dbHelper
                                      .queryAllRowsf();
                                  bool k = false;
                                  if (_getfavorite.length > 0) {
                                    for (int i = 0;
                                    i < _getfavorite.length;
                                    i++) {
                                      if (Products!
                                          .productId ==
                                          _getfavorite[i]
                                          ["p_id"]) {
                                        k = true;
                                        if (_getfavorite[i]
                                        ["favorite"] ==
                                            'True') {
                                          await dbHelper.updatef(
                                            {
                                              "id":
                                              _getfavorite[i]
                                              ["id"],
                                              "p_id":
                                              _getfavorite[i]
                                              ["p_id"],
                                              "productName":
                                              _getfavorite[i][
                                              "productName"],
                                              "description":
                                              _getfavorite[i][
                                              'description'],
                                              "image":
                                              _getfavorite[i]
                                              ["image"],
                                              "price":
                                              _getfavorite[i]
                                              ["price"],
                                              "Amount": 1,
                                              "favorite": 'False',
                                              "color": "white"
                                            },
                                          ).then((value) {
                                            const snackBar =
                                            SnackBar(
                                              backgroundColor:
                                              Colors.red,
                                              content: Text(
                                                  'تمت إزالة المنتج من المفضلة',
                                                  style: TextStyle(
                                                      fontSize:
                                                      18,
                                                      fontFamily:
                                                      'Cairo')),
                                              duration: Duration(
                                                  seconds: 1),
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius
                                                      .all(Radius
                                                      .circular(
                                                      10))),
                                            );

                                            ScaffoldMessenger.of(
                                                context)
                                                .showSnackBar(
                                                snackBar);
                                          });
                                          await readData();
                                          break;
                                        } else {
                                          k = true;
                                          await dbHelper.updatef({
                                            "id": _getfavorite[i]
                                            ["id"],
                                            "p_id":
                                            _getfavorite[i]
                                            ["p_id"],
                                            "productName":
                                            _getfavorite[i][
                                            "productName"],
                                            "description":
                                            _getfavorite[i][
                                            'description'],
                                            "image":
                                            _getfavorite[i]
                                            ["image"],
                                            "price":
                                            _getfavorite[i]
                                            ["price"],
                                            "Amount": 1,
                                            "favorite": 'True',
                                            "color": "white"
                                          }).then((value) {
                                            const snackBar =
                                            SnackBar(
                                              backgroundColor:
                                              Colors.green,
                                              content: Text(
                                                'تمت إضافة المنتج للمفضلة ',
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontFamily:
                                                    'Cairo'),
                                              ),
                                              duration: Duration(
                                                  seconds: 1),
                                            );
                                            ScaffoldMessenger.of(
                                                context)
                                                .showSnackBar(
                                                snackBar);
                                          });
                                          await readData();
                                          setState(() {});
                                          break;
                                        }
                                      }
                                    }
                                  }
                                  if (k == false) {
                                    await dbHelper.insertF({
                                      "p_id": Products!
                                          .productId,
                                      "productName":
                                      Products!
                                          .productAr,
                                      "description":
                                      Products!
                                          .description,
                                      "image":
                                      "https://mapi.orasys.org/images/512/${Products!.image}",
                                      "price":
                                      Products!
                                          .price,
                                      "Amount": 1,
                                      "favorite": "True",
                                      "color": "red"
                                    }).then((value) {
                                      const snackBar = SnackBar(
                                        backgroundColor:
                                        Colors.green,
                                        content: Text(
                                          'تمت إضافة المنتج للمفضلة',
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontFamily:
                                              'Cairo'),
                                        ),
                                        duration:
                                        Duration(seconds: 1),
                                      );
                                      ScaffoldMessenger.of(
                                          context)
                                          .showSnackBar(snackBar);
                                    });
                                    await readData();
                                    setState(() {});
                                  }
                                  setState(() {});
                                },

                                child: Icon(Icons.favorite,size: 40,
                                    color: ((_getfavorite.where(
                                            (e) =>
                                        e['p_id'] ==
                                            Products!.productId))
                                        .map((e) =>
                                    e['favorite'])
                                        .any((e) =>
                                    e == "True"))
                                        ? Colors.red
                                        : Colors.white),


                              ),)
                            ]

                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 16, top: 8, right: 16),
                          child: Row(
                            children: [
                              Text(
                                Products!.productAr.toString(),
                                style: TextStyle(
                                    color: ColorManager.grey1,
                                    fontSize: 30,
                                    fontFamily: 'Cairo',
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 16, right: 16),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            // mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                NumberFormat.currency(locale: 'ar_SY',symbol: '',decimalDigits: 0).format(Products!.price ) + ' ل س',

                                // '${Products!.price }' + ' ل س',
                                style: TextStyle(
                                    color: ColorManager.grey1,
                                    fontSize: 25,
                                    fontFamily: 'Cairo',
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 16,left: 16),
                          child: ElevatedButton(
                            onPressed: () async {
                              _getproduct = await dbHelper.queryAllRows();
                              bool k = false;
                              if (_getproduct.length > 0) {
                                for (int i = 0; i < _getproduct.length; i++) {
                                  if (Products!.productId == _getproduct[i]["p_id"]) {
                                    k = true;
                                    await dbHelper.update(
                                      {
                                        "id": _getproduct[i]["id"],
                                        "p_id": _getproduct[i]["p_id"],
                                        "productName": _getproduct[i]["productName"],
                                        "image": _getproduct[i]["image"],
                                        "price": _getproduct[i]["price"],
                                        "Amount": int.parse(_getproduct[i]["Amount"]) + 1,
                                        "MinAmount": _getproduct[i]['MinAmount'],
                                        "Quantety": _getproduct[i]["Quantety"],
                                      },
                                    ).then((value) {
                                      const snackBar = SnackBar(
                                        backgroundColor: Colors.red,
                                        content: Text(
                                            'تمت إضافة المنتج بالفعل في سلة التسوق',
                                            style: TextStyle(
                                                fontSize: 18, fontFamily: 'Cairo')),
                                        duration: Duration(seconds: 1),
                                      );
                                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                    });
                                  }
                                }
                              }
                              if (k == false) {
                                await dbHelper.insert({
                                  "p_id": Products!.productId,
                                  "productName": Products!.productAr,
                                  "image":
                                  "https://mapi.orasys.org/images/512/${Products!.image}",
                                  "price": Products!.price,
                                  "Amount": 1,
                                  "MinAmount": Products!.minAmount,
                                  "Quantety": Products!.amount,
                                }).then((value) {
                                  const snackBar = SnackBar(
                                    backgroundColor: Colors.green,
                                    content: Text('تمت إضافة المنتج للسلة',
                                        style:
                                        TextStyle(fontSize: 18, fontFamily: 'Cairo')),
                                    duration: Duration(seconds: 1),
                                  );
                                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                });
                              }
                              BlocProvider.of<CartBloc>(context).add(UpdateCartIconCount());
                            },
                            child: Text('إضافة  المنتج الى السلة',style: TextStyle(fontFamily: 'Cairo',fontSize: 20,color:Colors.white),),
                            style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(ColorManager.primary) ),

                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 8, right: 16, left: 16),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 1,
                            //   width: 400,

                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'المواصفات',
                                    style: TextStyle(
                                        fontSize: 25,
                                        fontFamily: 'Cairo',
                                        color: ColorManager.grey1),
                                  ),
                                  const Divider(
                                    color: Colors.black,
                                    height: 1.0,
                                  ),
                                  MediumTextt(
                                      text: Products!.description.toString(),
                                      color: ColorManager.grey1),
                                  SizedBox(
                                    height: 32,
                                  )


                                ]),
                            //ProductDetails(),
                          ),
                        ),
                        const SizedBox(
                          height: 2,
                        ),
                        StickyHeader(
                          header: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                color: ColorManager.grey1,
                                alignment: Alignment.centerRight,
                                width: MediaQuery.of(context).size.width,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 16),
                                  child: BigText(text: 'قد يعجبك أيضاً'),
                                ),
                              )
                            ],
                          ),
                          content: Container(
                              color: const Color(0xFFf7f7f7),
                              child: HomePagesc()),
                        ),
                        // const SizedBox(
                        //   height: 0.09,
                        // ),
                        // Container(
                        //   height: MediaQuery.of(context).size.height * 0.5,
                        //   padding: const EdgeInsets.all(2.0),
                        //   child: const HomeReco(),
                        // ),
                      ]),
                ),
              ),

            ]),
      bottomNavigationBar: BottomNavBa(
        pageIndex: 6,
      ),

      // add product to cart
      /*persistentFooterButtons: [
      InkWell(
           onTap: () async {
             _getproduct = await dbHelper.queryAllRows();
             bool k = false;
             if (_getproduct.length > 0) {
               for (int i = 0; i < _getproduct.length; i++) {
                 if (Products!.productId == _getproduct[i]["p_id"]) {
                   k = true;
                   await dbHelper.update(
                     {
                       "id": _getproduct[i]["id"],
                       "p_id": _getproduct[i]["p_id"],
                       "productName": _getproduct[i]["productName"],
                       "image": _getproduct[i]["image"],
                       "price": _getproduct[i]["price"],
                       "Amount": int.parse(_getproduct[i]["Amount"]) + 1
                     },
                   ).then((value) {
                     const snackBar = SnackBar(
                       backgroundColor: Colors.green,
                       content: Text('Product is already added to cart'),
                       duration: Duration(seconds: 1),
                     );
                     ScaffoldMessenger.of(context).showSnackBar(snackBar);
                   });
                 }
               }
             }
             if (k == false) {
               await dbHelper.insert({
                 "p_id": Products!.productId,
                 "productName": Products!.productAr,
                 "image": "https://mapi.orasys.org/images/${Products!.image}",
                 "price": Products!.price,
                 "Amount": 1
               }).then((value) {
                 const snackBar = SnackBar(
                   backgroundColor: Colors.green,
                   content: Text('Product is added to cart'),
                   duration: Duration(seconds: 1),
                 );
                 ScaffoldMessenger.of(context).showSnackBar(snackBar);
               });
             }
             BlocProvider.of<CartBloc>(context).add(UpdateCartIconCount());
           },
           child: Container(
             height: 50,
             width: double.infinity,

             color:ColorManager.grey1,
             child: Center(
                 child: MediumText(
                   text: 'إضافة الى عربة التسوق',
                   color: Colors.white,
                 )),
           ),
         ),
      ],*/

      //
      //  bottomNavigationBar:
      //    InkWell(
      //      onTap: () async {
      //        _getproduct = await dbHelper.queryAllRows();
      //        bool k = false;
      //        if (_getproduct.length > 0) {
      //          for (int i = 0; i < _getproduct.length; i++) {
      //            if (Products!.productId == _getproduct[i]["p_id"]) {
      //              k = true;
      //              await dbHelper.update(
      //                {
      //                  "id": _getproduct[i]["id"],
      //                  "p_id": _getproduct[i]["p_id"],
      //                  "productName": _getproduct[i]["productName"],
      //                  "image": _getproduct[i]["image"],
      //                  "price": _getproduct[i]["price"],
      //                  "Amount": int.parse(_getproduct[i]["Amount"]) + 1
      //                },
      //              ).then((value) {
      //                const snackBar = SnackBar(
      //                  backgroundColor: Colors.green,
      //                  content: Text('Product is already added to cart'),
      //                  duration: Duration(seconds: 1),
      //                );
      //                ScaffoldMessenger.of(context).showSnackBar(snackBar);
      //              });
      //            }
      //          }
      //        }
      //        if (k == false) {
      //          await dbHelper.insert({
      //            "p_id": Products!.productId,
      //            "productName": Products!.productAr,
      //            "image": "https://mapi.orasys.org/images/${Products!.image}",
      //            "price": Products!.price,
      //            "Amount": 1
      //          }).then((value) {
      //            const snackBar = SnackBar(
      //              backgroundColor: Colors.green,
      //              content: Text('Product is added to cart'),
      //              duration: Duration(seconds: 1),
      //            );
      //            ScaffoldMessenger.of(context).showSnackBar(snackBar);
      //          });
      //        }
      //        BlocProvider.of<CartBloc>(context).add(UpdateCartIconCount());
      //      },
      //      child: Container(
      //        height: 50,
      //        width: double.infinity,
      //
      //        color:ColorManager.grey1,
      //        child: Center(
      //            child: MediumText(
      //              text: 'إضافة الى عربة التسوق',
      //              color: Colors.white,
      //            )),
      //      ),
      //    ),
      //
    );
  }
}
