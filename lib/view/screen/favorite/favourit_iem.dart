
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../core/app_bar.dart';

import '../../../core/constant/bottomnav_bar.dart';
import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/DBHelper.dart';


import '../../widget/auth/medium_txt_blod.dart';
import '../details/details_page.dart';

class FavouritItem extends StatefulWidget {

  FavouritItem({Key? key,}) : super(key: key);

  @override
  State<FavouritItem> createState() => _FavouritItemState();
}

class _FavouritItemState extends State<FavouritItem> {

  List _listFavoriteproduct = [];
  bool isloading = true;


  DatabaseHelper dbHelper = DatabaseHelper();


  //read data from database
  Future readData() async{
    List<Map<String,dynamic>> responce = await dbHelper.queryAllRowsf();
    _listFavoriteproduct.addAll(responce.where((e) => e['favorite']=="True"));
    isloading = false;
    if(this.mounted){
      setState(() {});
    }

  }
  //
  @override
  void initState() {
    readData();
    // TODO: implement initState
    super.initState();
  }
@override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    readData();
  }
  @override
  Widget build(BuildContext context) {

    var mq = MediaQuery.of(context);

    return Scaffold(
        appBar: AppBarWidget(appBar: AppBar(),) ,
      body:  _listFavoriteproduct.isEmpty? Center(
        child: Align(
          alignment: Alignment.center,
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Image(
                  image: AssetImage(
                      'assets/images/empty_cart.png'),
                ),
                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.01,),
                MediumTextBold(text: 'المفضلة فارغة 😌',
                    color: ColorManager.grey1),
                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.01,),
                MediumTextBold(
                  text: 'استكشف المنتجات واضف العناصر للمفضلة لديك',
                  color:  ColorManager.grey1,
                )

              ],
            ),
          ),
        ),
      ) : SingleChildScrollView(
        child: Column(
            children: [
              Center(
                  child: GridView.builder(
                      shrinkWrap: true,
                      primary: false,
                      padding: const EdgeInsets.all(20),
                      gridDelegate: (mq.size.width < 370)
                          ? const SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 0.60, //heigh of container
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                          crossAxisCount: 2):
                      const SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio:0.64,
                          crossAxisSpacing: 5,
                          mainAxisSpacing:10,
                          crossAxisCount: 2),
                      itemCount:_listFavoriteproduct.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                            child:
                            InkWell(onTap: () {
                               Navigator.push(context, MaterialPageRoute(
                                   builder: (context) => DetailsPage(id: _listFavoriteproduct[index]['p_id'],)));
                            },

                              child:
                              Container(
                                width: MediaQuery.of(context).size.width*0.4,
                                height: MediaQuery.of(context).size.height*0.3,

                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey,width: 0.2),
                                  borderRadius: BorderRadius.circular(0),
                                  color: Colors.white,

                            ),
                            child: Column(
                              children: [
                                Stack(children: [
                                Center(
                                      child: Image.network(
                                          _listFavoriteproduct[index]['image'], width: MediaQuery
                                          .of(context)
                                          .size
                                          .width * 0.5,
                                          height: MediaQuery
                                              .of(context)
                                              .size
                                              .height * 0.2, fit: BoxFit.fill)
                                  ),
                                  Positioned(
                                    right: 2.sp,
                                    top: 2.sp,
                                    child: InkWell(
                                      child:  Icon(
                                        Icons.favorite,
                                        color: _listFavoriteproduct[index]['favorite'] == 'True'? Colors.red:Colors.white),
                                      onTap: () async {

                                              int delete =  await  dbHelper.deletef(_listFavoriteproduct[index]['id']);
                                              if(delete > 0){

                                                _listFavoriteproduct.removeWhere((element) => element['id'] == _listFavoriteproduct[index]['id'] );
                                                setState(() {});
                                              }
                                      },
                                    ),
                                  ),

                                    ],),

                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          SizedBox( height: MediaQuery.of(context).size.height*0.04,
                                            child: Text(_listFavoriteproduct[index]['productName'], style:   TextStyle(
                                                color:  ColorManager.grey1,
                                                fontSize: 17,
                                                fontFamily: 'Cairo'),),
                                          ),
                                          // SizedBox( height: MediaQuery.of(context).size.height*0.04,
                                          //   child: Text(widget.description, style: TextStyle(
                                          //       color: Colors.black45,
                                          //       fontSize: 17,
                                          //       fontFamily: 'Cairo'),),
                                          // ),
                                          Row(
                                            children: [
                                              Text(_listFavoriteproduct[index]['description'], style:  TextStyle(fontSize: 14,
                                                  fontFamily: 'Cairo',
                                                color:  ColorManager.grey1,
                                                  // decoration: TextDecoration.lineThrough
                                              ),),
                                              // Text(widget, style: TextStyle(fontSize: 15,
                                              //   fontFamily: 'Cairo', color: Colors.green,
                                              // ),)
                                            ],
                                          ),
                                          const SizedBox(height:2),
                                          Text(
                                         ' ${_listFavoriteproduct[index]['price']}  ل س', style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color:  ColorManager.grey1,
                                              fontSize: 16,
                                              fontFamily: 'Cairo'),),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),)


                        );
                      })
              )

            ]),
      ),
      // bottomNavigationBar: BottomNav(pageIndex: 1,),

    );

  }
}