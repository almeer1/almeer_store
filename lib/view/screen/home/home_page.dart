import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:meerstore/core/constant/color_manger.dart';

import 'package:meerstore/view/screen/category/all_catgory.dart';

import '../../../bloc/cart_bloc.dart';
import '../../../core/app_bar.dart';
import '../../../core/constant/assets_Widget.dart';

import '../../../data/data_source/DBHelper.dart';
import '../../../data/data_source/api_controller.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomePage extends StatefulWidget {
  HomePage({
    Key? key,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  AssetWidget _assetWidget = AssetWidget();
  //
  //    PageController _pagecontroller=PageController();
  TextEditingController textController = TextEditingController();
  DatabaseHelper dbHelper = DatabaseHelper();
  var dio = Dio();

  ApiController api = new ApiController();

  int tappedIndex = 0;
  int activeIndex = 0;

  @override
  void initState() {
    BlocProvider.of<CartBloc>(context).add(UpdateCartIconCount());
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Locale _locale = Locale('ar', '');
    var lang = AppLocalizations.of(context);
    return Scaffold(
      backgroundColor: ColorManager.white,
      appBar: PreferredSize(
        preferredSize:
             Size.fromHeight(122),
        child: AppBarWidget(
          appBar: AppBar(),
        ),
      ),
      body:
      AllCatgory(),

    );
  }
}


