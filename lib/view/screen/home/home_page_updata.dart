import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meerstore/view/screen/home/home_product.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';

import '../../../core/app_bar.dart';
import '../../../core/constant/assets_Widget.dart';
import '../../../core/constant/bottomnav_bar.dart';
import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/DBHelper.dart';
import '../../../data/data_source/api_controller.dart';
import '../../notification/notification.dart';
import '../../widget/auth/big_text.dart';
import '../../widget/home/recommend.dart';
import '../category/all_catgory.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:firebase_messaging/firebase_messaging.dart';


class HomePageUpdata extends StatefulWidget {
  @override
  _HomePageUpdataState createState() => _HomePageUpdataState();
}

class _HomePageUpdataState extends State<HomePageUpdata> {

  var dio = Dio();

  DatabaseHelper dbHelper = DatabaseHelper();
  AssetWidget _assetWidget = AssetWidget();
  ApiController api = new ApiController();
  final ScrollController scrollcontroller = ScrollController();
  var fbm = FirebaseMessaging.instance;

  bool scroll_visibility = true;
  bool isLoadingMore = false;
  bool isFavorit = false;
  int number = 0;
  Color? color;
  String? isColor;
  double offset = 0.0;
  // List<productPag> productData = [];
  List productData = [];
  List Favorite = [];

  //database
  List<Map<String, dynamic>> _getproduct = [];
  List<Map<String, dynamic>> _getfavorite = [];

  // fetch product from Api

  Future<void> _scorllListener() async {
    if (scrollcontroller.position.pixels ==
        scrollcontroller.position.maxScrollExtent) {}

  }

  initalMessage() async{
    var message = await FirebaseMessaging.instance.getInitialMessage();
    if(message!= null){
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Notifcation()));
    }

  }
  @override
  void initState() {
    initalMessage();
    fbm.getToken().then((value) {
      print(value);
    });
    scrollcontroller.addListener(_scorllListener);
    FirebaseMessaging.onMessage.listen((event) {
      print("====== notification ================");
      print("${event.notification!.body}");
      print("====== notification ================");
    });
    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Notifcation()));
    });


    // SystemChannels.platform.invokeMethod<void>('SystemNavigator.pop');
    // SystemNavigator.pop();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery
        .of(context)
        .size;


    var mq = MediaQuery.of(context);
    var lang = AppLocalizations.of(context);
    int? idroute= ModalRoute.of(context)?.settings.arguments as int? ;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarWidget(
        appBar: AppBar(
          elevation: 10,

        ),
      ),
      body: ListView(controller: scrollcontroller, children: [
        Container(
          child: AllCatgory(),
        ),
        StickyHeader(
            header: Container(
              color: ColorManager.grey1,
              width: size.width,
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              alignment: Alignment.centerRight,
              child: BigText(text: lang!.hom_text),
            ),
            content: Column(
              children: [
                Container(
                    color: const Color(0xFFf7f7f7), child: HomePagesc()),
                Stack(
                  children:[InkWell(
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomeReco()));
                    },
                    child: Text("عرض المزيد",style: TextStyle(fontSize: 24,fontFamily: 'Cairo',color: ColorManager.primary),),
                  ),
                  ]

                )
              ],
            )),
      ]),
      //
      // bottomNavigationBar: BottomNav(pageIndex: 0,),
    );
  }
  }

