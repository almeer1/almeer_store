import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

import '../../../bloc/cart_bloc.dart';
import '../../../core/constant/assets_Widget.dart';
import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/DBHelper.dart';
import '../../../data/data_source/api_controller.dart';
import '../../widget/auth/medium_text.dart';
import '../../widget/home/recommend.dart';
import '../details/details_page.dart';

class HomePagesc extends StatefulWidget {
  @override
  _HomePagescState createState() => _HomePagescState();
}

class _HomePagescState extends State<HomePagesc> with TickerProviderStateMixin {
  late bool _isLastPage;
  late int page;
  late bool _error;
  late bool _loading;
  final int _numberOfPostsPerRequest = 3000;
  late List postList;
  final int _nextPageTrigger = 1;
  bool isf = true;

  void _isLiked() {}

  DatabaseHelper dbHelper = DatabaseHelper();
  AssetWidget _assetWidget = AssetWidget();
  ApiController api = new ApiController();
  final scrollerController = ScrollController();
  late AnimationController _favoriteController;

  List<Map<String, dynamic>> _getfavorite = [];
  List<Map<String, dynamic>> _getproduct = [];
  int go = 1500;

  @override
  void initState() {
    super.initState();
    page = 1;
    postList = [];
    _isLastPage = false;
    _loading = true;
    _error = false;

    fetchData();
    readData();
    _favoriteController =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
  }

  @override
  void dispose() {
    _favoriteController.dispose();
    fetchData();
    super.dispose();
  }

  readData() async {
    _getfavorite = await dbHelper.queryAllRowsf();
  }

  readDataCart() async {
    _getproduct = await dbHelper.queryAllRows();
  }

  Future<void> fetchData() async {
    try {
      DioCacheManager dioCacheManager = DioCacheManager(CacheConfig());
      Options myOption =
          buildCacheOptions(const Duration(days: 30), forceRefresh: true);
      Dio().interceptors.add(dioCacheManager.interceptor);
      // String url = "https://mapi.orasys.org/public/api/products/all?page=$page";
      String url = "https://mapi.orasys.xyz/api/products/all?page=$page";
      final response = await Dio().get(url, options: myOption);
      postList = postList + response.data['data'];

      setState(() {
        _isLastPage = postList.length < _numberOfPostsPerRequest;
        _loading = true;
        page = page + 1;
        go += go;
      });
    } catch (e) {
      print("error --> $e");
      setState(() {
        _loading = false;
        _error = true;
      });
    }
  }

  Widget errorDialog({required double size}) {
    return SizedBox(
      height: 180,
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'An error occurred when fetching the posts.',
            style: TextStyle(
                fontSize: size,
                fontWeight: FontWeight.w500,
                color: Colors.black),
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(
              onTap: () {
                setState(() {
                  _loading = true;
                  _error = false;
                  fetchData();
                });
              },
              child: const Text(
                "Retry",
                style: TextStyle(fontSize: 20, color: Colors.purpleAccent),
              )),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildPostsView();
  }

  Widget buildPostsView() {
    var mq = MediaQuery.of(context);

    if (postList.isEmpty) {
      if (_loading) {
        return Center(
            child: Padding(
                padding: EdgeInsets.all(8),
                child: SizedBox(
                  height: 140,
                  child: Center(
                    child: LoadingAnimationWidget.threeRotatingDots(
                      size: 64,
                      color: ColorManager.grey1,
                    ),
                  ),
                )));
      } else if (_error) {
        return Center(child: errorDialog(size: 20));
      }
    }

    // final Post post = postList[index];
    return GridView.builder(
        controller: scrollerController,
        shrinkWrap: true,
        primary: false,
        padding: const EdgeInsets.all(16),
        gridDelegate: (mq.size.width < 370)
            ? const SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 0.60, //heigh of container
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 2)
            : const SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 0.64, //heigh of container
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 2),
        itemCount: postList.length,
        itemBuilder: (BuildContext context, int index) {
          if (index == postList.length - _nextPageTrigger && index < 40) {
            fetchData();
            if (_error) {
              return Center(child: errorDialog(size: 15));
            } else {
              return Center(
                  child: Padding(
                padding: EdgeInsets.all(8),
                child: SizedBox(
                  height: 200,
                  child: Center(
                    child: LoadingAnimationWidget.threeRotatingDots(
                      size: 64,
                      color: ColorManager.grey1,
                    ),
                  ),
                ),
              ));
            }
          }
          return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 0.2),
                  borderRadius: BorderRadius.circular(0),
                  color: Colors.white),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Stack(
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailsPage(
                                          id: postList[index]['productId']
                                              .toString(),
                                        )));
                          },
                          child: Center(
                              child: Image.network(
                            "https://mapi.orasys.org/images/512/${postList[index]['image']}",
                            fit: BoxFit.fill,
                            height: (mq.size.width - 42) / 2,
                          )),
                        ),
                        Positioned(
                          right: 2.sp,
                          top: 2.sp,
                          child: IconButton(
                            icon: Icon(Icons.favorite,
                                size: 30,
                                color: ((_getfavorite.where((e) =>
                                            e['p_id'] ==
                                            postList[index]['productId']))
                                        .map((e) => e['favorite'])
                                        .any((e) => e == "True"))
                                    ? Colors.red
                                    : Colors.white),
                            onPressed: () async {
                              // get data from database
                              _getfavorite = await dbHelper.queryAllRowsf();
                              bool k = false;
                              if (_getfavorite.length > 0) {
                                for (int i = 0; i < _getfavorite.length; i++) {
                                  if (postList[index]['productId'] ==
                                      _getfavorite[i]["p_id"]) {
                                    k = true;
                                    if (_getfavorite[i]["favorite"] == 'True') {
                                      await dbHelper.updatef(
                                        {
                                          "id": _getfavorite[i]["id"],
                                          "p_id": _getfavorite[i]["p_id"],
                                          "productName": _getfavorite[i]
                                              ["productName"],
                                          "description": _getfavorite[i]
                                              ['description'],
                                          "image": _getfavorite[i]["image"],
                                          "price": _getfavorite[i]["price"],
                                          "Amount": 1,
                                          "favorite": 'False',
                                          "color": "white"
                                        },
                                      ).then((value) {
                                        const snackBar = SnackBar(
                                          backgroundColor: Colors.red,
                                          content: Text(
                                              'تمت إزالة المنتج من المفضلة',
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontFamily: 'Cairo')),
                                          duration: Duration(seconds: 1),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                        );

                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(snackBar);
                                      });
                                      await readData();
                                      break;
                                    } else {
                                      k = true;
                                      await dbHelper.updatef({
                                        "id": _getfavorite[i]["id"],
                                        "p_id": _getfavorite[i]["p_id"],
                                        "productName": _getfavorite[i]
                                            ["productName"],
                                        "description": _getfavorite[i]
                                            ['description'],
                                        "image": _getfavorite[i]["image"],
                                        "price": _getfavorite[i]["price"],
                                        "Amount": 1,
                                        "favorite": 'True',
                                        "color": "white"
                                      }).then((value) {
                                        const snackBar = SnackBar(
                                          backgroundColor: Colors.green,
                                          content: Text(
                                            'تمت إضافة المنتج للمفضلة ',
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontFamily: 'Cairo'),
                                          ),
                                          duration: Duration(milliseconds: 1),
                                        );
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(snackBar);
                                      });
                                      await readData();
                                      setState(() {});
                                      break;
                                    }
                                  }
                                }
                              }
                              if (k == false) {
                                await dbHelper.insertF({
                                  "p_id": postList[index]['productId'],
                                  "productName": postList[index]['productAr'],
                                  "description": postList[index]['description'],
                                  "image":
                                      "https://mapi.orasys.org/images/512/${postList[index]['image']}",
                                  "price": postList[index]['price'],
                                  "Amount": 1,
                                  "favorite": "True",
                                  "color": "red"
                                }).then((value) {
                                  const snackBar = SnackBar(
                                    backgroundColor: Colors.green,
                                    content: Text(
                                      'تمت إضافة المنتج للمفضلة',
                                      style: TextStyle(
                                          fontSize: 18, fontFamily: 'Cairo'),
                                    ),
                                    duration: Duration(milliseconds: 1),
                                  );
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(snackBar);
                                });
                                await readData();
                                setState(() {});
                              }
                              setState(() {});
                            },
                          ),
                        ),
                        Positioned(
                            left: 0,
                            bottom: 130.sp,
                            child: RotationTransition(
                                turns: const AlwaysStoppedAnimation(-43 / 360),
                                child: Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.02,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(2),
                                      color: Colors.blue),
                                  child: postList[index]['amount'] == '0'
                                      ? Center(
                                          child: MediumText(
                                          text: 'Out of stock',
                                          color: Colors.white70,
                                        ))
                                      : postList[index]['amount'] ==
                                              postList[index]['minAmount']
                                          ? Center(
                                              child: MediumText(
                                              text: 'Out of stock',
                                              color: Colors.white70,
                                            ))
                                          : null,
                                ))),
                      ],
                    ),
                    Flex(direction: Axis.vertical, children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  postList[index]['productAr'].toString(),
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Cairo',
                                    color: ColorManager.grey1,
                                    fontWeight: FontWeight.w700,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),

                                Text(
                                  postList[index]['description'].toString(),
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'Cairo',
                                    color: ColorManager.grey1,
                                    // overflow: TextOverflow.values[],
                                  ),
                                  softWrap: false,
                                  maxLines: 1,
                                ),

                                // SizedBox(height:0.1),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      NumberFormat.currency(locale: 'ar_SY',symbol: '',decimalDigits: 0).format(postList[index]['price'] ) + ' ل س',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: 'Cairo',
                                        color: ColorManager.grey1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(2),
                                        color: ColorManager.primary,
                                      ),
                                      child: postList[index]['amount'] == '0'
                                          ? InkWell(
                                              onTap: () {
                                                const snackBar = SnackBar(
                                                  backgroundColor: Colors.green,
                                                  content: Text(
                                                    ' عذراً غير متوفرة الآن',
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontFamily: 'Cairo'),
                                                  ),
                                                  duration:
                                                      Duration(seconds: 1),
                                                );
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(snackBar);
                                              },
                                              child: const Icon(
                                                Icons.add,
                                                color: Colors.white,
                                              ),
                                            )
                                          : InkWell(
                                              onTap: () async {
                                                // get data from database
                                                _getproduct = await dbHelper
                                                    .queryAllRows();
                                                bool k = false;
                                                if (_getproduct.length > 0) {
                                                  for (int i = 0;
                                                      i < _getproduct.length;
                                                      i++) {
                                                    if (postList[index]
                                                            ['productId'] ==
                                                        _getproduct[i]
                                                            ["p_id"]) {
                                                      k = true;
                                                      await dbHelper.update(
                                                        {
                                                          "id": _getproduct[i]
                                                              ["id"],
                                                          "p_id": _getproduct[i]
                                                              ["p_id"],
                                                          "productName":
                                                              _getproduct[i][
                                                                  "productName"],
                                                          "image":
                                                              _getproduct[i]
                                                                  ["image"],
                                                          "price":
                                                              _getproduct[i]
                                                                  ["price"],
                                                          "Amount": int.parse(
                                                                  _getproduct[i]
                                                                      [
                                                                      "Amount"]) +
                                                              1,
                                                          "MinAmount":
                                                              _getproduct[i]
                                                                  ['MinAmount'],
                                                          "Quantety":
                                                              _getproduct[i]
                                                                  ['Quantety']
                                                        },
                                                      ).then((value) {
                                                        const snackBar = SnackBar(
                                                            backgroundColor:
                                                                Colors.red,
                                                            content: Text(
                                                                'تمت زيادة كمية المنتج',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        18,
                                                                    fontFamily:
                                                                        'Cairo')),
                                                            duration: Duration(
                                                                milliseconds:250));
                                                        ScaffoldMessenger.of(
                                                                context)
                                                            .showSnackBar(
                                                                snackBar);
                                                      });
                                                    }
                                                  }
                                                }
                                                if (k == false) {
                                                  await dbHelper.insert({
                                                    "p_id": postList[index]
                                                        ['productId'],
                                                    "productName":
                                                        postList[index]
                                                            ['productAr'],
                                                    "image":
                                                        "https://mapi.orasys.org/images/512/${postList[index]['image']}",
                                                    "price": postList[index]
                                                        ['price'],
                                                    "Amount": 1,
                                                    "MinAmount": postList[index]
                                                        ['minAmount'],
                                                    "Quantety": postList[index]
                                                        ['amount']
                                                  }).then((value) {
                                                    const snackBar = SnackBar(
                                                      backgroundColor:
                                                          Colors.green,
                                                      content: Text(
                                                          'تمت إضافة المنتج للسلة ',
                                                          style: TextStyle(
                                                              fontSize: 18,
                                                              fontFamily:
                                                                  'Cairo')),
                                                      duration:
                                                          Duration(milliseconds: 250),
                                                    );
                                                    ScaffoldMessenger.of(
                                                            context)
                                                        .showSnackBar(snackBar);
                                                  });
                                                }
                                                BlocProvider.of<CartBloc>(
                                                        context)
                                                    .add(UpdateCartIconCount());
                                                // await CartFunctions.updateCartIconCount();
                                                setState(() {});
                                              },
                                              child: const Icon(
                                                Icons.add,
                                                color: Colors.white,
                                              ),
                                            ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ]),
                  ]));
        });
  }
}
