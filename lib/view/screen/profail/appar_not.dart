import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/view/screen/profail/profail.dart';

import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/api_controller.dart';

class ApparNotif extends StatefulWidget {
  const ApparNotif({Key? key}) : super(key: key);

  @override
  State<ApparNotif> createState() => _ApparNotifState();
}



class _ApparNotifState extends State<ApparNotif> {
  ApiController apiController = ApiController();
  late String _base64;
  late Future getImage;

  List _listCartProduct = [];

// get Image logo
  Future<String> _getImage() async {
    var data = await apiController.getImage();
    return data;
  }

  @override
  void initState() {
    // getImage = _getImage();

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,

      child: Row(
        children: [
          InkWell(
            onTap: (){ Navigator.pop(context);},
            child: Icon(FontAwesomeIcons.chevronRight,size: 25,color: ColorManager.grey1,),
          ),
          const SizedBox(width:8),
          Image(
            width: 200,

            image:  AssetImage(
              'assets/images/logo (1).png',
            ),),
          // FutureBuilder<dynamic>(
          //   future: getImage,
          //   builder: (context, snapshot) {
          //     if (snapshot.hasData) {
          //       _base64 = snapshot.data!;
          //       final Uint8List bytes = base64Decode(_base64);
          //       return Image.memory(
          //         bytes,
          //         width: MediaQuery.of(context).size.width / 2,
          //         //height: MediaQuery.of(context).size.height,
          //         fit: BoxFit.fitWidth,
          //       );
          //     }
          //     return const Center(child: CircularProgressIndicator());
          //   },
          // ),

        ],
      ),
    );
  }
}
