import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';

class ConnectUs extends StatelessWidget {
  const ConnectUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height:32,
      color: const Color(0xFFF1F4FD),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,

        children: [

          Padding(
            padding: const EdgeInsets.only( right: 16, ),
            child: Row(
              children: [
                const  SizedBox(width: 8,),
                MediumTextBold(text: 'تواصل معنا', color:const Color(0xFF9DA3BB)),
              ],
            ),
          ),
          const SizedBox(height:64,),
          SizedBox(
            child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              InkWell(
                child: const Icon(FontAwesomeIcons.facebookF,size:30,color: Colors.grey,),
                onTap: (){},
              ),
                InkWell(
                  child: const Icon(FontAwesomeIcons.twitter,size:30,color: Colors.grey,),
                  onTap: (){},
                ),
                InkWell(
                  child: const Icon(FontAwesomeIcons.instagram,size:30,color: Colors.grey,),
                  onTap: (){},
                ),
                InkWell(
                  child: const Icon(FontAwesomeIcons.whatsapp,size:30,color: Colors.grey,),
                  onTap: (){},
                ),
            ],),
          ),
        ],
      ),

    );
  }
}
