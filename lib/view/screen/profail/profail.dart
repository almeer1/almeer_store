import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/view/screen/auth/login.dart';

import 'package:meerstore/view/screen/profail/connect.dart';
import 'package:meerstore/view/notification/notification.dart';

import 'package:meerstore/view/widget/auth/medium_text.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../bloc/cart_bloc.dart';
import '../../../bloc/profail_bloc.dart';
import '../../../data/data_source/DBHelper.dart';
import '../../../data/data_source/login _servece.dart';
import '../../widget/auth/medium_textt.dart';
import '../../../core/constant/assets_Widget.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class Profail extends StatefulWidget {
  const Profail({Key? key}) : super(key: key);

  @override
  _ProfailState createState() => _ProfailState();
}

class _ProfailState extends State<Profail> {
  bool is_user_login_loading = true;
  String firstName = "";
  String lastName = "";
  String userName = "";
  String address = '';
  String mobile = '';
  String login = '';
  String type = '';
  bool is_login_p = false;

  DatabaseHelper dbHelper = DatabaseHelper();
  AssetWidget asss = AssetWidget();
  User_controller usercontroller = new User_controller();
  final AssetWidget _assetWidget = AssetWidget();

  check_login() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    const key = 'token';
    final isLogin = prefs.get(key) ?? 0;
    final isSignup = prefs.get(key) ?? 0;
    if (isLogin != 0 || isSignup != 0) {
      final _firstName = prefs.getString('firstName') ?? 0;
      final _lastName = prefs.getString('lastName') ?? 0;
      final _userName = prefs.getString('username') ?? 0;
      final _mobile = prefs.getString('mobile') ?? 0;
      final _address = prefs.getString('address') ?? 0;
      final _type = prefs.getString('type') ?? 0;
      setState(() {
        firstName = _firstName.toString();
        lastName = _lastName.toString();
        userName = _userName.toString();
        mobile = _mobile.toString();
        address = _address.toString();
        type = _type.toString();
        is_login_p = true;
      });
    } else {
      setState(() {
        userName = '*******';
        mobile = '***********';
        is_login_p = false;
        login = 'login here';
      });
    }
  }

  File? image;
  String? imagePath;
  final imagePicker = ImagePicker();

  uploadImage(ImageSource source) async {
    //The value that will be returned will be stored in this variable
    var pickedImage = await imagePicker.getImage(source: source);
    if (pickedImage != null) {
      saveData(pickedImage.path.toString());
      setState(() async {
        image = File(pickedImage.path);
        image = await _crropImage(imageFile: image as File);
      });
    } else {}
  }

  Future<void> deleteImage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('path');
    setState(() {});
  }

  void saveData(String val) async {
    final sharedpref = await SharedPreferences.getInstance();
    sharedpref.setString('path', val);
    getData();
  }

  void getData() async {
    final sharedPref = await SharedPreferences.getInstance();
    setState(() {
      imagePath = sharedPref.getString('path');
    });
  }

  Future<File?> _crropImage({required File imageFile}) async {
    CroppedFile? croppesImage =
        await ImageCropper().cropImage(sourcePath: imageFile.path);
    if (croppesImage == null) return null;
    return File(croppesImage.path);
  }

  @override
  void initState() {
    // TODO: implement initState
    check_login();
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.only( top:MediaQuery.of(context).padding.top),
        child: Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            imageProfile(),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                MediumTexttBold(
                                    text: userName.toString(),
                                    color: ColorManager.grey1),
                                MediumText(
                                    text: mobile.toString(),
                                    color: ColorManager.grey1),
                              ],
                            ),
                          ],
                        ),
                      ),
                      // Setting
                      Container(
                        // width: 500,
                        height: 60,
                        color: const Color(0xFFF1F4FD),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const SizedBox(
                              width: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.only( right: 16, ),
                              child: MediumTextBold(
                                  text: 'الإعدادات', color: ColorManager.grey1),
                            ),
                          ],
                        ),
                      ), //setting
                      // Container(
                      //
                      //   // width: 500,
                      //   height: 50,
                      //   decoration: const BoxDecoration(
                      //     color:Color(0xFFf7f7f7),
                      //     border: Border(top: BorderSide(width: 0.2, color: Colors.grey),
                      //         bottom: BorderSide(width: 0.2, color: Colors.grey)
                      //     ),
                      //   ),
                      //   child: Row(
                      //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //     crossAxisAlignment: CrossAxisAlignment.center,
                      //
                      //     children: [
                      //       Row(
                      //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //         children: [
                      //           const  SizedBox(width: 8,),
                      //           InkWell(onTap: (){}, child: Icon(FontAwesomeIcons.globe,size:20 ,color:ColorManager.primary,)),
                      //           const  SizedBox(width: 8,),
                      //           Opacity(opacity: 0.4,
                      //               child: MediumTextBold(text: 'اللغة', color:ColorManager.grey1)),
                      //         ],
                      //       ),
                      //       Row(
                      //         children: [
                      //           Opacity(opacity:0.4,
                      //               child: MediumTextBold(text: 'العربية', color:ColorManager.grey1)),
                      //           const  SizedBox(width: 8,),
                      //           // InkWell(onTap: (){
                      //           //   // Navigator.push(context, MaterialPageRoute(builder:(context)=>const Login() ));
                      //           // }, child: Icon(FontAwesomeIcons.chevronLeft,size:20.sp ,color:const Color(0xFF8A8B90)) ),
                      //         ],
                      //       )
                      //     ],
                      //   ),
                      // ),
                      //language
                      Container(
                        // width:500,
                        height: 50,
                        decoration: BoxDecoration(
                          color: const Color(0xFFf7f7f7),
                          border: Border(
                              top:
                                  BorderSide(width: 0.7.sp, color: Colors.grey),
                              bottom: BorderSide(
                                  width: 0.7.sp, color: Colors.grey)),
                        ),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Notifcation()),
                            );
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only( right: 16, ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    InkWell(
                                        splashColor: Colors.white,
                                        onTap: () {},
                                        child: Icon(
                                          FontAwesomeIcons.bell,
                                          size: 20.sp,
                                          color: ColorManager.primary,
                                        )),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    MediumTextBold(
                                      text: 'الإشعارات',
                                      color: ColorManager.grey1,
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                // height:MediaQuery.of(context).size.height*0.03,
                                padding:   const EdgeInsets.only( left: 16, ),
                                child: Row(
                                  children: [
                                    InkWell(
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const Notifcation()),
                                          );
                                        },
                                        child: Icon(
                                            FontAwesomeIcons.chevronLeft,
                                            size: 20.sp,
                                            color: ColorManager.grey1)),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      is_login_p == false
                          ? Padding(
                            padding: const EdgeInsets.only( right: 16, ),
                            child: InkWell(
                                splashColor: Colors.white,
                                onTap: () async {
                                  await dbHelper.deleteAll();
                                  await dbHelper.deleteAllf();
                                  BlocProvider.of<CartBloc>(context)
                                      .add(UpdateCartIconCount());
                                  setState(() {});
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => const Login()));
                                },
                                child: Row(
                                  children: [
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Icon(
                                      Icons.login_outlined,
                                      color: ColorManager.primary,
                                      size: 25.sp,
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                      "تسجيل الدخول",
                                      style: TextStyle(
                                          color: ColorManager.grey1,
                                          fontSize: 16,
                                          fontFamily: 'Cairo',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )),
                          )
                          : InkWell(
                              splashColor: Colors.white,
                            onTap: (){
                              AlertDia(context,"هل تريد  تسجل خروج");
                            },
                              child: Padding(
                                padding:const EdgeInsets.only( right: 16, ),
                                child: Row(
                                  children: [
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Icon(
                                      Icons.exit_to_app,
                                      color: ColorManager.primary,
                                      size: 25.sp,
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                      "تسجيل الخروج",
                                      style: TextStyle(
                                          color: ColorManager.grey1,
                                          fontSize: 24,
                                          fontFamily: 'Cairo'),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                    ],
                  ),
                ),

                // SizedBox(height: MediaQuery.of(context).size.height*0.3),

                const SizedBox(
                  height: 190,
                  // width: 500,
                  child: ConnectUs(),
                )
              ]),
        ),
      ),
      // bottomNavigationBar: BottomNav(pageIndex: 3,),
    );
  }

  Widget imageProfile() {
    return Center(
      child: Stack(children: <Widget>[
        imagePath != null
            ? Container(
                child: CircleAvatar(
                    radius: 80.0,
                    backgroundImage: Image.file(File(imagePath!)).image))
            : Container(
                child: const CircleAvatar(
                    radius: 80.0,
                    backgroundImage: AssetImage("assets/images/profail.jpg"))),
        Positioned(
          bottom: 20.0,
          right: 20.0,
          child: InkWell(
            onTap: () {
              // uploadImage();
              showModalBottomSheet(
                context: context,
                builder: ((builder) => bottomSheet()),
              );
            },
            child: Icon(Icons.camera_alt,
                color: ColorManager.primary, size: 28.0.sp),
          ),
        ),
      ]),
    );
  }

  //

  Widget bottomSheet() {
    return Container(
      height: 200,
      width: 100.sp,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MediumText(
                    text: 'اختر الصورة الشخصية', color: ColorManager.grey1),
                imagePath == null? Center() :InkWell(
                    child: Icon(Icons.delete,
                        color: ColorManager.primary, size: 28.0.sp),
                    onTap: () async {
                      SharedPreferences preferences =
                          await SharedPreferences.getInstance();

                      preferences.remove('path');
                      getData();
                      Navigator.pop(context);
                      setState(() {});
                    }
                    //     // onTap: () async {
                    //     //   if (imagePath != null) {
                    //     //     SharedPreferences preferences =
                    //     //         await SharedPreferences.getInstance();
                    //     //     preferences.remove('path');
                    //     //     setState(() {});
                    //     //   }
                    //     // }
                    ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                TextButton.icon(
                  onPressed: () {
                    Navigator.pop(context);
                    uploadImage(ImageSource.camera);
                  },
                  icon: Icon(
                    Icons.camera,
                    color: ColorManager.grey1,
                  ),
                  label:
                      MediumText(text: 'الكاميرا', color: ColorManager.grey1),
                ),
                const SizedBox(
                  width: 20,
                ),
                TextButton.icon(
                  onPressed: () {
                    Navigator.pop(context);
                    uploadImage(ImageSource.gallery);
                  },
                  icon: Icon(Icons.image, color: ColorManager.grey1),
                  label:
                      MediumText(text: 'الاستديو', color: ColorManager.grey1),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  AlertDia(BuildContext context, String title, ) {
    return showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text(
            title,
            style: TextStyle(
                fontFamily: 'Cairo',
                color: ColorManager.grey1,
                fontSize: 18),
          ),


          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(onPressed: (){
                  Navigator.pop(context);
                }, child: Text('رجوع',   style: TextStyle(
                    fontFamily: 'Cairo',
                    color: ColorManager.primary,
                    fontSize: 18),)),
                TextButton(
                  onPressed: () async{
                     SharedPreferences preferences =
                     await SharedPreferences.getInstance();
                      preferences.remove('username');
                         preferences.remove('mobile');
                     preferences.remove('token');
                        preferences.remove('path');
                   await dbHelper.deleteAll();
                     await dbHelper.deleteAllf();
                       BlocProvider.of<CartBloc>(context)
                  .add(UpdateCartIconCount());
                       setState(() {});
                         Navigator.push(
                      context,
                   MaterialPageRoute(
                      builder: (context) => const Login()));
},
                  child: Text('نعم',
                      style: TextStyle(
                          fontFamily: 'Cairo',
                          color: ColorManager.primary,
                          fontSize: 18)),
                ),
              ],
            ),
          ],
        ));
  }


  }
  // void takephoto(ImageSource source) async{
  //   final pickedFile = await _picker.pickImage(source: source);
  //
  //
  //   setState(() {
  //     _imageFile = pickedFile as PickedFile?;
  //   });
  //
  // }

  //
  // onTap: () async {
  // SharedPreferences preferences =
  //     await SharedPreferences.getInstance();
  // preferences.remove('username');
  // preferences.remove('mobile');
  // preferences.remove('token');
  // preferences.remove('path');
  // await dbHelper.deleteAll();
  // await dbHelper.deleteAllf();
  // BlocProvider.of<CartBloc>(context)
  //     .add(UpdateCartIconCount());
  // setState(() {});
  // Navigator.push(
  // context,
  // MaterialPageRoute(
  // builder: (context) => const Login()));
// },


