// ignore_for_file: public_member_api_docs, sort_constructors_first
//
// import 'package:dio/dio.dart';
// import 'package:dio_http_cache/dio_http_cache.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
//
// import '../../../bloc/cart_bloc.dart';
// import '../../../core/constant/color_manger.dart';
// import '../../../data/data_source/DBHelper.dart';
//
//
// import '../../../data/model/product.dart';
// import '../../../data/model/searchproduct_model.dart';
// import '../../widget/auth/medium_text.dart';
// import '../details/details_page.dart';
//
// class SearchPage extends StatefulWidget {
//   // final List<product> posts;
//   //
//   //
//   // SearchPage({required this.posts});
//
//   @override
//   _SearchPageState createState() => _SearchPageState();
// }
//
// class _SearchPageState extends State<SearchPage> {
//
//   DatabaseHelper dbHelper = DatabaseHelper();
//   bool isloading = true;
//   Dio dio =Dio();
//   List<Map<String, dynamic>> _getfavorite = [];
//   List   _searchedPost = [];
//   List<Map<String,dynamic>> _getproduct = [];
// late String productArn ;
//   List<searchProduct> posts =[];
//   Future<void> getAllProduct( String  productAr)async {
//     try{
//       final response = await dio.get ('https://mapi.orasys.org/public/api/products/searchProducts', queryParameters: {'searchText': productAr},);
//       for( var post in response.data){
//         setState(() {
//           posts.add(searchProduct.fromJson(post));
//           isloading = false;
//
//         });
//         // print(_searchP);
//       } }
//     catch(e){
//       print(e);
//
//     } }
//
//   @override
//   void dispose() {
//     super.dispose();
//
//   }
//   @override
//   void initState() {
//     _searchedPost.clear();
//
//     // TODO: implement initState
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//
//     return Scaffold(
//
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         title: TextField(
//           style: TextStyle(color: ColorManager.grey1,fontSize: 20,fontFamily: 'Cairo'),
//           decoration:  InputDecoration(
//               hintText: 'انقر للكتابة',
//               hintStyle: TextStyle(color: ColorManager.grey1,fontSize: 20,fontFamily: 'Cairo'),
//               border: InputBorder.none
//           ),
//           onChanged: (val){
//
//             setState(() {
//               productArn = val;
//               getAllProduct(productArn);
//               isloading = false;
//               _searchedPost = posts.where((el)=>el.productAr!.contains(val) || el.description!.contains(val) || el.productEn!.contains(val)
//               ).toList();
//             });
//           },
//         ),
//         leading: IconButton(
//           iconSize: 20.sp,
//           onPressed: () {
//             Navigator.pop(context);
//           },
//           icon: const Icon(
//             Icons.arrow_back,
//             color: Colors.black,
//           ),),
//       ),
//
//       body:isloading ? const Center(child: SizedBox(height: 50,
//           child: CircularProgressIndicator()),):
//       GridView.builder(
//           shrinkWrap: true,
//           primary: false,
//           padding: const EdgeInsets.all(20),
//           gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
//               childAspectRatio: 0.6,
//               crossAxisSpacing: 5,
//               mainAxisSpacing:10,
//               crossAxisCount: 2 ),
//           itemCount:_searchedPost.length ,
//           itemBuilder: (BuildContext context, int index) {
//             // print( 'https://mapi.orasys.org/images/${_searchedPost[index].image}');
//             return  GestureDetector(
//                 child: Container(
//                   // width: MediaQuery.of(context).size.width*0.5,
//                   // height: MediaQuery.of(context).size.height*0.04,
//                   // width: 250,
//                   // height: 100,
//                     decoration: BoxDecoration(
//                         border: Border.all(
//                             color: Colors.grey, width: 0.2),
//                         borderRadius: BorderRadius.circular(0),
//                         color: Colors.white),
//                     child: Column(
//                         mainAxisAlignment:
//                         MainAxisAlignment.spaceBetween,
//                         children: [
//                           Stack(
//                             children: [
//                               InkWell(
//                                 onTap: () {
//                                   Navigator.push(
//                                       context,
//                                       MaterialPageRoute(
//                                           builder: (context) =>
//                                               DetailsPage(
//                                                 id: _searchedPost[index].productId.toString(),
//                                               )));
//                                   // print(__products[index].productId);
//                                 },
//                                 child: Center(
//                                     child: Image.network(
//                                         "https://mapi.orasys.org/images/${_searchedPost[index].image}",
//                                         // width: MediaQuery.of(context).size.width * 0.5,
//                                         // height: MediaQuery.of(context).size.height * 0.2,
//                                         // width: 200,
//                                         // height: 160,
//                                         fit: BoxFit.fitWidth)),
//                               ),
//
//                               Positioned(
//                                 right: 2.sp,
//                                 top: 2.sp,
//                                 child: IconButton(
//                                   icon: Icon(Icons.favorite,
//                                       color:Colors.white),
//                                   onPressed: ()
//                                   async {
//                                     // get data from database
//                                     _getfavorite = await dbHelper.queryAllRowsf();
//                                     bool k = false;
//                                     if (_getfavorite.length > 0) {
//                                       for (int i = 0; i < _getfavorite.length; i++) {
//                                         if (_searchedPost[index].productId == _getfavorite[i]["p_id"]) {
//                                           k = true;
//                                           await dbHelper
//                                               .updatef(
//                                             {
//                                               "id": _getfavorite[i]["id"],
//                                               "p_id": _getfavorite[i]["p_id"],
//                                               "productName": _getfavorite[i]["productName"],
//                                               "description":_getfavorite[i]['description'],
//                                               "image": _getfavorite[i]["image"],
//                                               "price": _getfavorite[i]["price"],
//                                               "Amount":1,
//                                               "favorite":"False",
//                                               "color":"white"
//                                             },
//                                           ).then((value) {
//                                             const snackBar = SnackBar(
//                                                 backgroundColor: Colors.red,
//                                                 content: Text('Product is already added to Favorite'),
//                                                 duration: Duration(seconds: 1));
//                                             ScaffoldMessenger.of(context).showSnackBar(snackBar);
//                                           });
//                                         }
//                                       }
//                                     }
//                                     if (k == false) {
//                                       await dbHelper.insertF({
//                                         "p_id":_searchedPost[index].productId,
//                                         "productName":_searchedPost[index].productAr,
//                                         "description":_searchedPost[index].description,
//                                         "image":
//                                         "https://mapi.orasys.org/images/${_searchedPost[index].image}",
//                                         "price":
//                                         _searchedPost[index].price,
//                                         "Amount":
//                                         1,
//                                         "favorite": "True",
//                                         "color":"red"
//                                       }).then((value) {
//                                         const snackBar =
//                                         SnackBar(
//                                           backgroundColor:
//                                           Colors.green,
//                                           content:
//                                           Text('Product is added to Favorite'),
//                                           duration:
//                                           Duration(seconds: 1),
//                                         );
//                                         ScaffoldMessenger.of(context)
//                                             .showSnackBar(snackBar);
//                                       });
//                                     }
//
//                                     setState(() {
//                                     });
//                                   },
//                                 ),
//                               ),
//                               Positioned(
//                                   left: 0,
//                                   // top: 5,
//                                   bottom: 130.sp,
//                                   // right: 58,
//                                   // top:0.2,
//                                   child: RotationTransition(
//                                       turns:
//                                       const AlwaysStoppedAnimation(
//                                           -43 / 360),
//                                       child: Container(
//                                         height:
//                                         MediaQuery.of(context)
//                                             .size
//                                             .height *
//                                             0.02,
//                                         // width: MediaQuery.of(context).size.width*0.01,
//                                         decoration: BoxDecoration(
//                                             borderRadius:
//                                             BorderRadius
//                                                 .circular(2),
//                                             color: Colors.blue),
//                                         child: _searchedPost[index]
//                                         .amount ==
//                                             '0'
//                                             ? Center(
//                                             child: MediumText(
//                                               text:
//                                               'Out of stock',
//                                               color:
//                                               Colors.white70,
//                                             ))
//                                             : _searchedPost[index]
//                                         .amount ==
//                                             _searchedPost[
//                                             index]
//                                           .minAmount
//                                             ? Center(
//                                             child:
//                                             MediumText(
//                                               text:
//                                               'Out of stock',
//                                               color: Colors
//                                                   .white70,
//                                             ))
//                                             : null,
//                                       ))),
//                             ],
//                           ),
//                           Column(
//                             mainAxisAlignment:
//                             MainAxisAlignment.end,
//                             children: [
//                               Container(
//                                 padding: const EdgeInsets.all(8),
//                                 child: Column(
//                                   crossAxisAlignment:
//                                   CrossAxisAlignment.start,
//                                   mainAxisAlignment:
//                                   MainAxisAlignment.end,
//                                   children: [
//                                     Text(
//                                       _searchedPost[index]
//                                      .productAr
//                                           .toString(),
//                                       style:  TextStyle(
//                                         fontSize: 16,
//                                         fontFamily: 'Cairo',
//                                         color: ColorManager.grey1,
//                                         overflow:
//                                         TextOverflow.ellipsis,
//                                       ),
//                                     ),
//
//                                     _searchedPost[index]
//                                         .description ==null?Text(''):Text(
//                                       _searchedPost[index]
//                                       .description
//                                           .toString(),
//                                       style:  TextStyle(
//                                         fontSize: 16,
//                                         fontFamily: 'Cairo',
//                                         color: ColorManager.grey1,
//                                         overflow:
//                                         TextOverflow.ellipsis,
//                                       ),
//                                     ),
//
//                                     // SizedBox(height:0.1),
//                                     Row(
//                                       crossAxisAlignment:
//                                       CrossAxisAlignment.end,
//                                       mainAxisAlignment:
//                                       MainAxisAlignment
//                                           .spaceBetween,
//                                       children: [
//                                         MediumText(
//                                           text: _searchedPost[index]
//                                           .price
//                                               .toString(),
//                                           color: ColorManager.grey1,
//                                         ),
//                                         Container(
//                                           decoration: BoxDecoration(
//                                               borderRadius:
//                                               BorderRadius
//                                                   .circular(2),
//                                               color:ColorManager.primary,),
//                                           child:
//                                           _searchedPost[index]
//                                           .amount==
//                                               '0'
//                                               ? null
//                                               : InkWell(
//                                             onTap:
//                                                 () async {
//                                               // get data from database
//                                               _getproduct = await dbHelper.queryAllRows();
//
//                                               bool k = false;
//                                               if (_getproduct.length > 0) {
//                                                 for (int i = 0; i < _getproduct.length; i++) {
//                                                   if (_searchedPost[index].productId == _getproduct[i]["p_id"]) {
//                                                     k = true;
//                                                     await dbHelper
//                                                         .update(
//                                                       {
//                                                         "id": _getproduct[i]["id"],
//                                                         "p_id": _getproduct[i]["p_id"],
//                                                         "productName": _getproduct[i]["productName"],
//                                                         "image": _getproduct[i]["image"],
//                                                         "price": _getproduct[i]["price"],
//                                                         "Amount": int.parse(_getproduct[i]["Amount"]) + 1,
//                                                       },
//                                                     ).then((value) {
//                                                       const snackBar = SnackBar(
//                                                           backgroundColor: Colors.red,
//                                                           content: Text('Product is already added in cart'),
//                                                           duration: Duration(seconds: 1));
//                                                       ScaffoldMessenger.of(context).showSnackBar(snackBar);
//                                                     });
//                                                   }
//                                                 }
//                                               }
//                                               if (k ==
//                                                   false) {
//                                                 await dbHelper.insert({
//                                                   "p_id":
//                                                   _searchedPost[index].productId,
//                                                   "productName":
//                                                   _searchedPost[index].productAr,
//                                                   "image":
//                                                   "https://mapi.orasys.org/images/${_searchedPost[index].image}",
//                                                   "price":
//                                                   _searchedPost[index].price,
//                                                   "Amount":
//                                                   1,
//                                                 }).then((value) {
//                                                   const snackBar =
//                                                   SnackBar(
//                                                     backgroundColor:
//                                                     Colors.green,
//                                                     content:
//                                                     Text('Product is added to cart'),
//                                                     duration:
//                                                     Duration(seconds: 1),
//                                                   );
//                                                   ScaffoldMessenger.of(context)
//                                                       .showSnackBar(snackBar);
//                                                 });
//                                               }
//                                               BlocProvider.of<CartBloc>(
//                                                   context)
//                                                   .add(
//                                                   UpdateCartIconCount());
//
//                                               // await CartFunctions.updateCartIconCount();
//                                               setState(
//                                                       () {});
//                                             },
//                                             child:
//                                                const Icon(
//                                               Icons.add,
//                                               color: Colors
//                                                   .white,
//                                             ),
//                                           ),
//                                         )
//                                       ],
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ],
//                           )
//                         ]))
//
//
//             );
//           })
//     );
//   }
// }
//

import 'dart:convert';
import 'package:badges/badges.dart' as badges;
import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:http/http.dart' as http;
import '../../../bloc/cart_bloc.dart';
import '../../../core/constant/bottom_bar.dart';
import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/DBHelper.dart';
import '../../../data/model/product.dart';
import '../../../data/model/searchproduct_model.dart';
import '../../widget/auth/medium_text.dart';
import '../cart/cart_screen.dart';
import '../details/details_page.dart';

class SearchPage extends StatefulWidget {
  final String query;
  const SearchPage({
    Key? key,
    required this.query,
  }) : super(key: key);
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  DatabaseHelper dbHelper = DatabaseHelper();
  bool _isSearching = false;

  Dio dio = Dio();
  List<Map<String, dynamic>> _getfavorite = [];
  List<searchProduct> _searchedPost = [];
  List<Map<String, dynamic>> _getproduct = [];
  String? productna;
  List<searchProduct> _searchP = [];
  bool _isloading = true;
  TextEditingController _searchQueryController = TextEditingController();

  readData() async {
    _getfavorite = await dbHelper.queryAllRowsf();
  }

  @override
  void initState() {
    readData();
    getProductSuggestions(widget.query);
    // TODO: implement initState
    super.initState();
  }

  getProductSuggestions(String query) async {
    final url =
        'https://mapi.orasys.org/public/api/products/searchProducts?searchText=$query';
    final response = await dio.get(url);
    print(response.data);
    if (response.statusCode == 200) {
      for (var post in response.data) {
        print(response.data);
        setState(() {
          _searchedPost.add(searchProduct.fromJson(post));
        });
        setState(() {
          _isloading = false;
        });
      }
      print("++++++++++++++++${_searchedPost.length}+++++++++++++++++++");
    } else {
      throw Exception();
    }
  }

  @override
  void dispose() {
    readData();
    // TODO: implement dispose
    super.dispose();
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      centerTitle: true,
      actions: [
        Padding(
          padding: const EdgeInsets.only(bottom: 6.0),
          child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BottomNav(
                            id: 1,
                          )));
            },
            child: Icon(
              FontAwesomeIcons.heart,
              color: ColorManager.grey1,
              size: 30,
            ),
          ),
        ),
        badges.Badge(
          badgeContent: BlocBuilder<CartBloc, CartState>(
            builder: (context, state) {
              if (state is CartInitial) {
                return const Text(
                  '0',
                  style: TextStyle(color: Colors.blueGrey, fontSize: 20),
                );
              } else if (state is CounterValueChangeState) {
                return Text(
                  state.counter.toString(),
                  style: const TextStyle(color: Colors.blueGrey, fontSize: 20),
                );
              } else {
                return const SizedBox(
                  height: 2,
                );
              }
            },
          ),
          badgeStyle: badges.BadgeStyle(
            badgeColor: ColorManager.primary,
          ),
          position: badges.BadgePosition.topEnd(top: 0, end: 0),
          child: InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const CartScreen()));
            },
            child: Icon(
              Icons.shopping_cart,
              color: ColorManager.grey1,
              size: 40,
            ),
          ),
        ),
      ],
      leading: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Icon(
          Icons.arrow_back,
          color: ColorManager.grey1,
          size: 45,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0), // here the desired height
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16),
          child: buildAppBar(context),
        ),
      ),
      // appBar: AppBar(
      //   backgroundColor: Colors.white,
      //   title: TextField(
      //     controller: _searchQueryController,
      //     keyboardType: TextInputType.text,
      //     // textInputAction: TextInputAction.search,
      //     style: TextStyle(
      //         color: ColorManager.grey1, fontSize: 20, fontFamily: 'Cairo'),
      //     decoration: InputDecoration(
      //         hintText: 'انقر للكتابة',
      //         hintStyle: TextStyle(
      //             color: ColorManager.grey1, fontSize: 20, fontFamily: 'Cairo'),
      //         border: InputBorder.none),
      //     onChanged: (val) async {
      //       setState(() {
      //         productna = val;
      //         // _isSearching=true;
      //       });
      //       // print(productna);
      //       //  final response = await dio.get ('https://mapi.orasys.org/public/api/products/searchProducts',queryParameters: {'searchText': productna},);
      //       // for( var post in response.data){
      //       //   setState(() {
      //       //     _searchedPost.add(searchProduct.fromJson(post));
      //       //   });
      //       //
      //       // }
      //       // setState(() {
      //       //   _searchedPost = _searchP.where((element) => element.productAr!.contains(val)||element.productEn!.contains(val)||element.description!.contains(val)).toList();
      //       // });
      //     },
      //   ),
      //   leading: IconButton(
      //     iconSize: 20.sp,
      //     onPressed: () {
      //       Navigator.pop(context);
      //     },
      //     icon: const Icon(
      //       Icons.arrow_back,
      //       color: Colors.black,
      //     ),
      //   ),
      //   actions: [
      //     Container(
      //       margin: EdgeInsets.symmetric(vertical: 8),
      //       width: 60,
      //       decoration: BoxDecoration(
      //         borderRadius: BorderRadius.circular(20),
      //         color: ColorManager.grey1,
      //       ),
      //       child: InkWell(
      //         onTap: () async {
      //           print(productna);
      //           setState(() {
      //             _searchedPost.clear();
      //           });
      //           final response = await dio.get(
      //             'https://mapi.orasys.org/public/api/products/searchProducts',
      //             queryParameters: {'searchText': productna},
      //           );
      //           for (var post in response.data) {
      //             setState(() {
      //               _searchedPost.add(searchProduct.fromJson(post));
      //             });
      //           }
      //           _isSearching = true;
      //         },
      //         // _isSearching?InkWell(onTap:(){
      //         //   setState(() {
      //         //     _searchQueryController.clear();
      //         //     _searchedPost.clear();
      //         //     _isSearching=false;
      //         //   });
      //         //   _searchedPost.clear();
      //         //   _isSearching=false;
      //         // },child: Icon(Icons.clear,color:ColorManager.grey1,size: 20,)):
      //         child: Icon(
      //           Icons.search,
      //           color: Colors.white,
      //           size: 30,
      //         ),
      //       ),
      //     ),
      //     SizedBox(
      //       width: 16,
      //     )
      //   ],
      // ),
      body: _isloading
          ? SizedBox(
              height: 200,
              child: Center(
                child: LoadingAnimationWidget.threeRotatingDots(
                  size: 64,
                  color: ColorManager.grey1,
                ),
              ),
            )
          : GridView.builder(
              shrinkWrap: true,
              primary: false,
              padding: const EdgeInsets.all(20),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: 0.6,
                  crossAxisSpacing: 5,
                  mainAxisSpacing: 10,
                  crossAxisCount: 2),
              itemCount: _searchedPost.length,
              itemBuilder: (BuildContext context, int index) {
                // print( 'https://mapi.orasys.org/images/${_searchedPost[index].image}');
                return GestureDetector(
                    child: Container(
                        // width: MediaQuery.of(context).size.width*0.5,
                        // height: MediaQuery.of(context).size.height*0.04,
                        // width: 250,
                        // height: 100,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 0.2),
                            borderRadius: BorderRadius.circular(0),
                            color: Colors.white),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Stack(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => DetailsPage(
                                                    id: _searchedPost[index]
                                                        .productId
                                                        .toString(),
                                                  )));
                                      // print(__products[index].productId);
                                    },
                                    child: Center(
                                        child: Image.network(
                                            "https://mapi.orasys.org/images/512/${_searchedPost[index].image}",
                                            // width: MediaQuery.of(context).size.width * 0.5,
                                            // height: MediaQuery.of(context).size.height * 0.2,
                                            // width: 200,
                                            // height: 160,
                                            fit: BoxFit.cover)),
                                  ),
                                  Positioned(
                                    right: 2.sp,
                                    top: 2.sp,
                                    child: IconButton(
                                      icon: Icon(Icons.favorite,
                                          size: 30,
                                          color: ((_getfavorite.where((e) =>
                                                      e['p_id'] ==
                                                      _searchedPost[index]
                                                          .productId))
                                                  .map((e) => e['favorite'])
                                                  .any((e) => e == "True"))
                                              ? Colors.red
                                              : Colors.white),
                                      onPressed: () async {
                                        // get data from database
                                        _getfavorite =
                                            await dbHelper.queryAllRowsf();
                                        bool k = false;
                                        if (_getfavorite.length > 0) {
                                          for (int i = 0;
                                              i < _getfavorite.length;
                                              i++) {
                                            if (_searchedPost[index]
                                                    .productId ==
                                                _getfavorite[i]["p_id"]) {
                                              k = true;
                                              if (_getfavorite[i]["favorite"] ==
                                                  'True') {
                                                await dbHelper.updatef(
                                                  {
                                                    "id": _getfavorite[i]["id"],
                                                    "p_id": _getfavorite[i]
                                                        ["p_id"],
                                                    "productName":
                                                        _getfavorite[i]
                                                            ["productName"],
                                                    "description":
                                                        _getfavorite[i]
                                                            ['description'],
                                                    "image": _getfavorite[i]
                                                        ["image"],
                                                    "price": _getfavorite[i]
                                                        ["price"],
                                                    "Amount": 1,
                                                    "favorite": 'False',
                                                    "color": "white"
                                                  },
                                                ).then((value) {
                                                  const snackBar = SnackBar(
                                                    backgroundColor: Colors.red,
                                                    content: Text(
                                                        'تمت إزالة المنتج من المفضلة',
                                                        style: TextStyle(
                                                            fontSize: 18,
                                                            fontFamily:
                                                                'Cairo')),
                                                    duration:
                                                        Duration(seconds: 1),
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    10))),
                                                  );

                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(snackBar);
                                                });
                                                await readData();
                                                break;
                                              } else {
                                                k = true;
                                                await dbHelper.updatef({
                                                  "id": _getfavorite[i]["id"],
                                                  "p_id": _getfavorite[i]
                                                      ["p_id"],
                                                  "productName": _getfavorite[i]
                                                      ["productName"],
                                                  "description": _getfavorite[i]
                                                      ['description'],
                                                  "image": _getfavorite[i]
                                                      ["image"],
                                                  "price": _getfavorite[i]
                                                      ["price"],
                                                  "Amount": 1,
                                                  "favorite": 'True',
                                                  "color": "white"
                                                }).then((value) {
                                                  const snackBar = SnackBar(
                                                    backgroundColor:
                                                        Colors.green,
                                                    content: Text(
                                                      'تمت إضافة المنتج للمفضلة ',
                                                      style: TextStyle(
                                                          fontSize: 18,
                                                          fontFamily: 'Cairo'),
                                                    ),
                                                    duration:
                                                        Duration(seconds: 1),
                                                  );
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(snackBar);
                                                });
                                                await readData();
                                                setState(() {});
                                                break;
                                              }
                                            }
                                          }
                                        }
                                        if (k == false) {
                                          await dbHelper.insertF({
                                            "p_id":
                                                _searchedPost[index].productId,
                                            "productName":
                                                _searchedPost[index].productAr,
                                            "description": _searchedPost[index]
                                                .description,
                                            "image":
                                                "https://mapi.orasys.org/images/512/${_searchedPost[index].image}",
                                            "price": _searchedPost[index].price,
                                            "Amount": 1,
                                            "favorite": "True",
                                            "color": "red"
                                          }).then((value) {
                                            const snackBar = SnackBar(
                                              backgroundColor: Colors.green,
                                              content: Text(
                                                'تمت إضافة المنتج للمفضلة',
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontFamily: 'Cairo'),
                                              ),
                                              duration: Duration(seconds: 1),
                                            );
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(snackBar);
                                          });
                                          await readData();
                                          setState(() {});
                                        }
                                        setState(() {});
                                      },
                                    ),
                                  ),
                                  Positioned(
                                      left: 0,
                                      // top: 5,
                                      bottom: 130.sp,
                                      // right: 58,
                                      // top:0.2,
                                      child: RotationTransition(
                                          turns: const AlwaysStoppedAnimation(
                                              -43 / 360),
                                          child: Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.02,
                                            // width: MediaQuery.of(context).size.width*0.01,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(2),
                                                color: Colors.blue),
                                            child: _searchedPost[index]
                                                        .amount ==
                                                    '0'
                                                ? Center(
                                                    child: MediumText(
                                                    text: 'Out of stock',
                                                    color: Colors.white70,
                                                  ))
                                                : _searchedPost[index].amount ==
                                                        _searchedPost[index]
                                                            .minAmount
                                                    ? Center(
                                                        child: MediumText(
                                                        text: 'Out of stock',
                                                        color: Colors.white70,
                                                      ))
                                                    : null,
                                          ))),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(8),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          _searchedPost[index]
                                              .productAr
                                              .toString(),
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: 'Cairo',
                                            color: ColorManager.grey1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),

                                        _searchedPost[index].description == null
                                            ? Text('')
                                            : Text(
                                                _searchedPost[index]
                                                    .description
                                                    .toString(),
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  fontFamily: 'Cairo',
                                                  color: ColorManager.grey1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ),

                                        // SizedBox(height:0.1),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            MediumText(
                                              text: _searchedPost[index]
                                                  .price
                                                  .toString(),
                                              color: ColorManager.grey1,
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(2),
                                                color: ColorManager.primary,
                                              ),
                                              child:
                                                  _searchedPost[index].amount ==
                                                          '0'
                                                      ? InkWell(
                                                          onTap: () {
                                                            const snackBar =
                                                                SnackBar(
                                                              backgroundColor:
                                                                  Colors.green,
                                                              content: Text(
                                                                ' عذراً غير متوفرة الآن',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        18,
                                                                    fontFamily:
                                                                        'Cairo'),
                                                              ),
                                                              duration:
                                                                  Duration(
                                                                      seconds:
                                                                          1),
                                                            );
                                                            ScaffoldMessenger
                                                                    .of(context)
                                                                .showSnackBar(
                                                                    snackBar);
                                                          },
                                                          child: const Icon(
                                                            Icons.add,
                                                            color: Colors.white,
                                                          ),
                                                        )
                                                      : InkWell(
                                                          onTap: () async {
                                                            // get data from database
                                                            _getproduct =
                                                                await dbHelper
                                                                    .queryAllRows();

                                                            bool k = false;
                                                            if (_getproduct
                                                                    .length >
                                                                0) {
                                                              for (int i = 0;
                                                                  i <
                                                                      _getproduct
                                                                          .length;
                                                                  i++) {
                                                                if (_searchedPost[
                                                                            index]
                                                                        .productId ==
                                                                    _getproduct[
                                                                            i][
                                                                        "p_id"]) {
                                                                  k = true;
                                                                  await dbHelper
                                                                      .update(
                                                                    {
                                                                      "id": _getproduct[
                                                                              i]
                                                                          [
                                                                          "id"],
                                                                      "p_id": _getproduct[
                                                                              i]
                                                                          [
                                                                          "p_id"],
                                                                      "productName":
                                                                          _getproduct[i]
                                                                              [
                                                                              "productName"],
                                                                      "image": _getproduct[
                                                                              i]
                                                                          [
                                                                          "image"],
                                                                      "price": _getproduct[
                                                                              i]
                                                                          [
                                                                          "price"],
                                                                      "Amount":
                                                                          int.parse(_getproduct[i]["Amount"]) +
                                                                              1,
                                                                      "MinAmount":
                                                                          _getproduct[i]
                                                                              [
                                                                              'minAmount'],
                                                                      "Quantety":
                                                                          _getproduct[i]
                                                                              [
                                                                              'amount']
                                                                    },
                                                                  ).then((value) {
                                                                    const snackBar = SnackBar(
                                                                        backgroundColor:
                                                                            Colors
                                                                                .red,
                                                                        content: Text(
                                                                            'تمت زيادة كمية المنتج',
                                                                            style:
                                                                                TextStyle(fontSize: 18, fontFamily: 'Cairo')),
                                                                        duration: Duration(milliseconds: 1));
                                                                    ScaffoldMessenger.of(
                                                                            context)
                                                                        .showSnackBar(
                                                                            snackBar);
                                                                  });
                                                                }
                                                              }
                                                            }
                                                            if (k == false) {
                                                              await dbHelper
                                                                  .insert({
                                                                "p_id": _searchedPost[
                                                                        index]
                                                                    .productId,
                                                                "productName":
                                                                    _searchedPost[
                                                                            index]
                                                                        .productAr,
                                                                "image":
                                                                    "https://mapi.orasys.org/images/512/${_searchedPost[index].image}",
                                                                "price":
                                                                    _searchedPost[
                                                                            index]
                                                                        .price,
                                                                "Amount": 1,
                                                                "MinAmount":
                                                                    _searchedPost[
                                                                            index]
                                                                        .minAmount,
                                                                "Quantety":
                                                                    _searchedPost[
                                                                            index]
                                                                        .amount
                                                              }).then((value) {
                                                                const snackBar =
                                                                    SnackBar(
                                                                  backgroundColor:
                                                                      Colors
                                                                          .green,
                                                                  content: Text(
                                                                      'تمت إضافة المنتج للسلة',
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              18,
                                                                          fontFamily:
                                                                              'Cairo')),
                                                                  duration:
                                                                      Duration(
                                                                          seconds:
                                                                              1),
                                                                );
                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                        snackBar);
                                                              });
                                                            }
                                                            BlocProvider.of<
                                                                        CartBloc>(
                                                                    context)
                                                                .add(
                                                                    UpdateCartIconCount());

                                                            // await CartFunctions.updateCartIconCount();
                                                            setState(() {});
                                                          },
                                                          child: const Icon(
                                                            Icons.add,
                                                            color: Colors.white,
                                                          ),
                                                        ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              )
                            ])));
              }),
    );
  }
}
