// import 'dart:convert';

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:meerstore/view/screen/search/search_page.dart';

// import '../../../data/model/prduct_model.dart' show Data, productPag;
import '../../../bloc/cart_bloc.dart';
import '../../../core/constant/bottom_bar.dart';
import '../../../core/constant/bottomnav_bar.dart';
import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/DBHelper.dart';
import '../../../data/data_source/endpoints_api.dart';
import '../../../data/model/product.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import '../../../data/model/searchproduct_model.dart';
import '../../widget/auth/medium_text.dart';
import '../cart/cart_screen.dart';
import '../details/details_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:badges/badges.dart' as badges;
// import '../../../data/module/user_module.dart' show Data, userModel;

class AutocompleteExample extends StatefulWidget {
  @override
  _AutocompleteExampleState createState() => _AutocompleteExampleState();
}

class _AutocompleteExampleState extends State<AutocompleteExample> {
  // userModel? _userModel;
  List<product> _productPag = [];
  Dio dio = Dio();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllProduct();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    getAllProduct();
  }

  Future getAllProduct() async {
    try {
      DioCacheManager dioCacheManager = DioCacheManager(CacheConfig());
      Options myoptions = buildCacheOptions(
        const Duration(days: 30),
        forceRefresh: true,
      );
      dio.options.connectTimeout = 20000;
      dio.options.baseUrl = Endpoints.baseUrl;

      dio.interceptors.add(dioCacheManager.interceptor);
      final response = await dio.get(Endpoints.products, options: myoptions);
      for (var post in response.data)
        setState(() {
          _productPag.add(post);
        });

      print(_productPag.length);
    } catch (e) {
      print(e);
    }
  }
  // void _getDataFromApi() async {
  //   var responce =
  //       // await http.get(Uri.parse('https://reqres.in/api/users?page=2'));
  //       await http.get(Uri.parse(
  //           "https://mapi.orasys.org/public/api/products"));

  //   setState(() {
  //     _productPag = product.fromJson(jsonDecode(responce.body))
  //     // _userModel = userModel.fromJson(jsonDecode(responce.body));
  //   });
  // }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: EdgeInsets.all(16.0),
          color: Colors.white,
          child: Autocomplete<product>(
            optionsBuilder: (TextEditingValue value) {
              if (value.text.isEmpty) {
                return List.empty();
              }
              return _productPag!
                  .where((e) => e.productEn!
                      .toLowerCase()
                      .contains(value.text.toLowerCase()))
                  .toList();
            },
            fieldViewBuilder: (BuildContext context,
                    TextEditingController controller,
                    FocusNode node,
                    Function onSubmint) =>
                TextField(

              controller: controller,
              focusNode: node,
              decoration: InputDecoration(hintText: 'kkkk'),
            ),
            optionsViewBuilder: (BuildContext contect, Function onSelected,
                Iterable<product> dataList) {
              return Material(
                child: ListView.builder(
                    itemCount: dataList.length,
                    itemBuilder: (context, index) {
                      product d = dataList.elementAt(index);
                      return ListTile(
                        title: Text(d.productAr!),
                        // leading: Image.network(
                        //   d.avatar!,
                        //   width: 50,
                        //   height: 50,
                        //   fit: BoxFit.fill,
                        // ),
                      );
                    }),
              );
            },
            onSelected: (value) => print(value.productAr),
            displayStringForOption: (product d) => d.productAr!,
          )),
    );
  }
}

class SearchPageF extends StatefulWidget {
  @override
  _SearchPageFState createState() => _SearchPageFState();
}

class _SearchPageFState extends State<SearchPageF> {
  TextEditingController _searchController = TextEditingController();
  List<searchProductt> _searchResults = [];
  List<Map<String, dynamic>> _getproduct = [];
  List<Map<String, dynamic>> _getfavorite = [];

  Future<List<searchProductt>> _getSearchResults(String query) async {
    final response = await http.get(Uri.parse(
        'https://mapi.orasys.org/public/api/products/searchProducts?searchText=$query'));
    if (response.statusCode == 200) {
      final results = json.decode(response.body);
      return results.where((result) => result.startsWith(query)).toList();
    } else {
      throw Exception('Failed to load search results');
    }
  }

  void _onSearchTextChanged(String query) async {
    if (query.isEmpty) {
      setState(() {
        _searchResults = [];
      });
      return;
    }
    final results = await _getSearchResults(query);
    setState(() {
      _searchResults = results;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          controller: _searchController,
          onChanged: _onSearchTextChanged,
          decoration: const InputDecoration(
            hintText: 'Search...',
            border: InputBorder.none,
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: _searchResults.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(_searchResults[index].productAr),
            subtitle: Text(_searchResults[index].description),
          );
        },
      ),
    );
  }
}

class NetworkTypeAheadPage extends StatefulWidget {
  @override
  State<NetworkTypeAheadPage> createState() => _NetworkTypeAheadPageState();
}

class _NetworkTypeAheadPageState extends State<NetworkTypeAheadPage> {
  TextEditingController _textFieldController = TextEditingController();
  Dio dio = Dio();
  List<searchProduct> _searchedPost = [];
  List<Map<String, dynamic>> _getfavorite = [];
  List<Map<String, dynamic>> _getproduct = [];
  DatabaseHelper dbHelper = DatabaseHelper();
  bool _isloading = true;


  var formatter = NumberFormat('#,##,000');



  getProductSuggestions(String query) async {
    final url =
        'https://mapi.orasys.xyz/api/products/searchProducts?searchText=$query';
    // 'https://mapi.orasys.org/public/api/products/searchProducts?searchText=$query';
    final response = await dio.get(url);
    // print(response.data);
    if (response.statusCode == 200) {
      for (var post in response.data) {
        setState(() {
          _searchedPost.add(searchProduct.fromJson(post));
        });
        setState(() {
          _isloading = false;
        });
      }
      // print("++++++++++++++++${_searchedPost.length}+++++++++++++++++++");
    } else {
      throw Exception();
    }
  }

  readData() async {
    _getfavorite = await dbHelper.queryAllRowsf();
  }

  @override
  void initState() {
    readData();
    // getProductSuggestions();

    ProductApi.getProductSuggestions;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          centerTitle: true,
          toolbarHeight: 80,
          elevation: 0,
          backgroundColor: Color.fromARGB(239, 252, 247, 247),
          automaticallyImplyLeading: false,
          title: Container(
            child: TypeAheadField<searchProductt?>(
              debounceDuration: const Duration(milliseconds: 800),
              hideSuggestionsOnKeyboardHide: true,
              textFieldConfiguration: TextFieldConfiguration(
                onSubmitted: (value) {
                  getProductSuggestions(value);
                  setState(() {
                    _searchedPost.clear();
                  });
                },
                // onTap:(){
                //   if(_textFieldController.selection == TextSelection.fromPosition(TextPosition(offset: _textFieldController.text.length -1))){
                //     setState(() {
                //       _textFieldController.selection = TextSelection.fromPosition(TextPosition(offset: _textFieldController.text.length));
                //     });
                //   }
                // },
                onTap: () {

                  if (_textFieldController
                          .text[_textFieldController.text.length -1] !=
                      ' ') {
                    _textFieldController.text =
                        (_textFieldController.text + ' ');
                  }
                  if (_textFieldController.selection ==
                      TextSelection.fromPosition(TextPosition(
                          offset: _textFieldController.text.length - 1))) {
                    setState(() {
                      _searchedPost.clear();
                    });
                  }
                },

                // onChanged: (value) {
                //   getProductSuggestions(value);
                //   _isloading = true;
                // },
                textInputAction: TextInputAction.search,
                controller: _textFieldController,
                // enableInteractiveSelection:false,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    contentPadding: EdgeInsets.all(16.0),
                    hintText: 'انقر للكتابة',
                    hintStyle: TextStyle(
                        color: ColorManager.grey1,
                        fontFamily: 'Cairo',
                        decoration: TextDecoration.none,
                        fontSize: 18)),
                style: const TextStyle(
                    fontSize: 16,
                    // letterSpacing: 1,
                     fontFamily: 'Cairo',
                    // decorationStyle: TextDecorationStyle.dotted,
                    decorationColor: Colors.white),
              ),
              suggestionsCallback: ProductApi.getProductSuggestions,
              itemBuilder: (context, searchProductt? suggestion) {
                final product = suggestion!;
                return ListTile(
                  // isThreeLine: true,
                  title: Flex(direction: Axis.horizontal, children: [
                    Row(
                      children: [
                        Text(
                          product.productAr,
                          style: TextStyle(
                              color: ColorManager.grey1,
                              fontFamily: 'Cairo',
                              fontSize: 16,
                              fontWeight: FontWeight.w300),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Text(
                          product.description,
                          style: TextStyle(
                            color: ColorManager.grey1,
                            fontFamily: 'Cairo',
                            fontSize: 11,
                          ),
                        ),
                      ],
                    ),
                  ]),
                );
              },
              noItemsFoundBuilder: (context) => Container(
                height: 100,
                child: Center(
                  child: Text(
                    'عذرا المنتج غير متوفر حالياً',
                    style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'Cairo',
                        color: ColorManager.grey1),
                  ),
                ),
              ),
              errorBuilder: (BuildContext context, error) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'ابحث عن شيءً ما',
                    style: TextStyle(
                        fontFamily: 'Cairo',
                        fontSize: 18,
                        color: ColorManager.grey1),
                  ),
                );
              },
              onSuggestionSelected: (searchProductt? suggestion) {
                final _product = suggestion!;
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            DetailsPage(id: _product.productId)));
              },
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              _isloading
                  ? const SizedBox(
                      height: 600,
                      child: Center(
                        child: Image(
                          image:
                              AssetImage('assets/images/Search Icon (3D).png'),
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  : GridView.builder(
                      shrinkWrap: true,
                      primary: false,
                      padding: const EdgeInsets.all(20),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 0.6,
                              crossAxisSpacing: 5,
                              mainAxisSpacing: 10,
                              crossAxisCount: 2),
                      itemCount: _searchedPost.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                            child: Container(
                                // width: MediaQuery.of(context).size.width*0.5,
                                // height: MediaQuery.of(context).size.height*0.04,
                                // width: 250,
                                // height: 100,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.grey, width: 0.2),
                                    borderRadius: BorderRadius.circular(0),
                                    color: Colors.white),
                                child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Stack(
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          DetailsPage(
                                                            id: _searchedPost[
                                                                    index]
                                                                .productId
                                                                .toString(),
                                                          )));
                                            },
                                            child: Center(
                                                child: Image.network(
                                                    "https://mapi.orasys.org/images/512/${_searchedPost[index].image}",
                                                    // width: MediaQuery.of(context).size.width * 0.5,
                                                    // height: MediaQuery.of(context).size.height * 0.2,
                                                    width: 200,
                                                    height: 200,
                                                    fit: BoxFit.cover)),
                                          ),
                                          Positioned(
                                            right: 2.sp,
                                            top: 2.sp,
                                            child: IconButton(
                                              icon: Icon(Icons.favorite,
                                                  size: 30,
                                                  color: ((_getfavorite.where(
                                                              (e) =>
                                                                  e['p_id'] ==
                                                                  _searchedPost[
                                                                          index]
                                                                      .productId))
                                                          .map((e) =>
                                                              e['favorite'])
                                                          .any((e) =>
                                                              e == "True"))
                                                      ? Colors.red
                                                      : Colors.white),
                                              onPressed: () async
                                              {
                                                // get data from database
                                                _getfavorite = await dbHelper
                                                    .queryAllRowsf();
                                                bool k = false;
                                                if (_getfavorite.length > 0) {
                                                  for (int i = 0;
                                                      i < _getfavorite.length;
                                                      i++) {
                                                    if (_searchedPost[index]
                                                            .productId ==
                                                        _getfavorite[i]
                                                            ["p_id"]) {
                                                      k = true;
                                                      if (_getfavorite[i]
                                                              ["favorite"] ==
                                                          'True') {
                                                        await dbHelper.updatef(
                                                          {
                                                            "id":
                                                                _getfavorite[i]
                                                                    ["id"],
                                                            "p_id":
                                                                _getfavorite[i]
                                                                    ["p_id"],
                                                            "productName":
                                                                _getfavorite[i][
                                                                    "productName"],
                                                            "description":
                                                                _getfavorite[i][
                                                                    'description'],
                                                            "image":
                                                                _getfavorite[i]
                                                                    ["image"],
                                                            "price":
                                                                _getfavorite[i]
                                                                    ["price"],
                                                            "Amount": 1,
                                                            "favorite": 'False',
                                                            "color": "white"
                                                          },
                                                        ).then((value) {
                                                          const snackBar =
                                                              SnackBar(
                                                            backgroundColor:
                                                                Colors.red,
                                                            content: Text(
                                                                'تمت إزالة المنتج من المفضلة',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        18,
                                                                    fontFamily:
                                                                        'Cairo')),
                                                            duration: Duration(
                                                                seconds: 1),
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            10))),
                                                          );

                                                          ScaffoldMessenger.of(
                                                                  context)
                                                              .showSnackBar(
                                                                  snackBar);
                                                        });
                                                        await readData();
                                                        break;
                                                      } else {
                                                        k = true;
                                                        await dbHelper.updatef({
                                                          "id": _getfavorite[i]
                                                              ["id"],
                                                          "p_id":
                                                              _getfavorite[i]
                                                                  ["p_id"],
                                                          "productName":
                                                              _getfavorite[i][
                                                                  "productName"],
                                                          "description":
                                                              _getfavorite[i][
                                                                  'description'],
                                                          "image":
                                                              _getfavorite[i]
                                                                  ["image"],
                                                          "price":
                                                              _getfavorite[i]
                                                                  ["price"],
                                                          "Amount": 1,
                                                          "favorite": 'True',
                                                          "color": "white"
                                                        }).then((value) {
                                                          const snackBar =
                                                              SnackBar(
                                                            backgroundColor:
                                                                Colors.green,
                                                            content: Text(
                                                              'تمت إضافة المنتج للمفضلة ',
                                                              style: TextStyle(
                                                                  fontSize: 18,
                                                                  fontFamily:
                                                                      'Cairo'),
                                                            ),
                                                            duration: Duration(
                                                                seconds: 1),
                                                          );
                                                          ScaffoldMessenger.of(
                                                                  context)
                                                              .showSnackBar(
                                                                  snackBar);
                                                        });
                                                        await readData();
                                                        setState(() {});
                                                        break;
                                                      }
                                                    }
                                                  }
                                                }
                                                if (k == false) {
                                                  await dbHelper.insertF({
                                                    "p_id": _searchedPost[index]
                                                        .productId,
                                                    "productName":
                                                        _searchedPost[index]
                                                            .productAr,
                                                    "description":
                                                        _searchedPost[index]
                                                            .description,
                                                    "image":
                                                        "https://mapi.orasys.org/images/512/${_searchedPost[index].image}",
                                                    "price":
                                                        _searchedPost[index]
                                                            .price,
                                                    "Amount": 1,
                                                    "favorite": "True",
                                                    "color": "red"
                                                  }).then((value) {
                                                    const snackBar = SnackBar(
                                                      backgroundColor:
                                                          Colors.green,
                                                      content: Text(
                                                        'تمت إضافة المنتج للمفضلة',
                                                        style: TextStyle(
                                                            fontSize: 18,
                                                            fontFamily:
                                                                'Cairo'),
                                                      ),
                                                      duration:
                                                          Duration(seconds: 1),
                                                    );
                                                    ScaffoldMessenger.of(
                                                            context)
                                                        .showSnackBar(snackBar);
                                                  });
                                                  await readData();
                                                  setState(() {});
                                                }
                                                setState(() {});
                                              },
                                            ),
                                          ),
                                          Positioned(
                                              left: 0,
                                              // top: 5,
                                              bottom: 130.sp,
                                              // right: 58,
                                              // top:0.2,
                                              child: RotationTransition(
                                                  turns:
                                                      const AlwaysStoppedAnimation(
                                                          -43 / 360),
                                                  child: Container(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.02,
                                                    // width: MediaQuery.of(context).size.width*0.01,
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(2),
                                                        color: Colors.blue),
                                                    child: _searchedPost[index]
                                                                .amount ==
                                                            '0'
                                                        ? Center(
                                                            child: MediumText(
                                                            text:
                                                                'Out of stock',
                                                            color:
                                                                Colors.white70,
                                                          ))
                                                        : _searchedPost[index]
                                                                    .amount ==
                                                                _searchedPost[
                                                                        index]
                                                                    .minAmount
                                                            ? Center(
                                                                child:
                                                                    MediumText(
                                                                text:
                                                                    'Out of stock',
                                                                color: Colors
                                                                    .white70,
                                                              ))
                                                            : null,
                                                  ))),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Container(
                                            padding: const EdgeInsets.all(8),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                Text(
                                                  _searchedPost[index]
                                                      .productAr
                                                      .toString(),
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'Cairo',
                                                    color: ColorManager.grey1,
                                                    fontWeight: FontWeight.w700,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),

                                                _searchedPost[index]
                                                            .description ==
                                                        null
                                                    ? Text('')
                                                    : Text(
                                                        _searchedPost[index]
                                                            .description
                                                            .toString(),
                                                        style: TextStyle(
                                                          fontSize: 16,
                                                          fontFamily: 'Cairo',
                                                          color: ColorManager
                                                              .grey1,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      ),

                                                // SizedBox(height:0.1),
                                                Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    MediumText(
                                                      text: NumberFormat.currency(locale: 'ar_SY',symbol: '',decimalDigits: 0).format(_searchedPost[index].price ) + ' ل س',

                                                      color: ColorManager.grey1,
                                                    ),
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(2),
                                                        color: ColorManager
                                                            .primary,
                                                      ),
                                                      child:
                                                          _searchedPost[index]
                                                                      .amount ==
                                                                  '0'
                                                              ? InkWell(
                                                                  onTap: () {
                                                                    const snackBar =
                                                                        SnackBar(
                                                                      backgroundColor:
                                                                          Colors
                                                                              .green,
                                                                      content:
                                                                          Text(
                                                                        ' عذراً غير متوفرة الآن',
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                18,
                                                                            fontFamily:
                                                                                'Cairo'),
                                                                      ),
                                                                      duration: Duration(
                                                                          milliseconds:
                                                                              1),
                                                                    );
                                                                    ScaffoldMessenger.of(
                                                                            context)
                                                                        .showSnackBar(
                                                                            snackBar);
                                                                  },
                                                                  child:
                                                                      const Icon(
                                                                    Icons.add,
                                                                    color: Colors
                                                                        .white,
                                                                  ),
                                                                )
                                                              : InkWell(
                                                                  onTap:
                                                                      () async {
                                                                    // get data from database
                                                                    _getproduct =
                                                                        await dbHelper
                                                                            .queryAllRows();

                                                                    bool k =
                                                                        false;
                                                                    if (_getproduct
                                                                            .length >
                                                                        0) {
                                                                      for (int i =
                                                                              0;
                                                                          i < _getproduct.length;
                                                                          i++) {
                                                                        if (_searchedPost[index].productId ==
                                                                            _getproduct[i]["p_id"]) {
                                                                          k = true;
                                                                          await dbHelper
                                                                              .update(
                                                                            {
                                                                              "id": _getproduct[i]["id"],
                                                                              "p_id": _getproduct[i]["p_id"],
                                                                              "productName": _getproduct[i]["productName"],
                                                                              "image": _getproduct[i]["image"],
                                                                              "price": _getproduct[i]["price"],
                                                                              "Amount": int.parse(_getproduct[i]["Amount"]) + 1,
                                                                              "MinAmount": _getproduct[i]['MinAmount'],
                                                                              "Quantety": _getproduct[i]['Quantety']
                                                                            },
                                                                          ).then((value) {
                                                                            const snackBar = SnackBar(
                                                                                backgroundColor: Colors.red,
                                                                                content: Text('تمت إضافة المنتج بالفعل في سلة التسوق', style: TextStyle(fontSize: 18, fontFamily: 'Cairo')),
                                                                                duration: Duration(milliseconds: 250));
                                                                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                                                          });
                                                                        }
                                                                      }
                                                                    }
                                                                    if (k ==
                                                                        false) {
                                                                      await dbHelper
                                                                          .insert({
                                                                        "p_id":
                                                                            _searchedPost[index].productId,
                                                                        "productName":
                                                                            _searchedPost[index].productAr,
                                                                        "image":
                                                                            "https://mapi.orasys.org/images/512/${_searchedPost[index].image}",
                                                                        "price":
                                                                            _searchedPost[index].price,
                                                                        "Amount":
                                                                            1,
                                                                        "MinAmount":
                                                                            _searchedPost[index].minAmount,
                                                                        "Quantety":
                                                                            _searchedPost[index].amount
                                                                      }).then((value) {
                                                                        const snackBar =
                                                                            SnackBar(
                                                                          backgroundColor:
                                                                              Colors.green,
                                                                          content: Text(
                                                                              'تمت إضافة المنتج للسلة',
                                                                              style: TextStyle(fontSize: 18, fontFamily: 'Cairo')),
                                                                          duration:
                                                                              Duration(milliseconds: 250),
                                                                        );
                                                                        ScaffoldMessenger.of(context)
                                                                            .showSnackBar(snackBar);
                                                                      });
                                                                    }
                                                                    BlocProvider.of<CartBloc>(
                                                                            context)
                                                                        .add(
                                                                            UpdateCartIconCount());

                                                                    // await CartFunctions.updateCartIconCount();
                                                                    setState(
                                                                        () {});
                                                                  },
                                                                  child:
                                                                      const Icon(
                                                                    Icons.add,
                                                                    color: Colors
                                                                        .white,
                                                                  ),
                                                                ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ])));
                      }),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavBa(
          pageIndex: 7,
        ),
      );
}

class ProductApi {
  static Future<List<searchProductt>> getProductSuggestions(String query) async {
    // 'https://mapi.orasys.org/public/api/products/searchProducts?searchText=$query'
    final url = Uri.parse(
        'https://mapi.orasys.xyz/api/products/searchProducts?searchText=$query');
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final List searchP = json.decode(response.body);

      return searchP.map((json) => searchProductt.fromJson(json)).where((se) {
        final nameLower = se.productAr.toLowerCase();
        final nameEn = se.productEn.toLowerCase();
        final description = se.description.toLowerCase();
        final queryLower = query;

        return nameLower.contains(queryLower) ||
            nameEn.contains(queryLower) ||
            description.contains(queryLower);
      }).toList();
    } else {
      throw Exception();
    }
  }
}
