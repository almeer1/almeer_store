import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/auth/small_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
class Cardd extends StatefulWidget {
  const Cardd({Key? key}) : super(key: key);

  @override
  _CarddState createState() => _CarddState();
}

class _CarddState extends State<Cardd> {
  @override
  Widget build(BuildContext context) {
    var mq=MediaQuery.of(context);
    var lang=AppLocalizations.of(context);
    return Container(
      height:  mq.size.height *0.1,
      width:  mq.size.width *0.5,


      child: Card(


        color: ColorManager.primary,
        child: Padding(padding: EdgeInsets.only(left: 25.0,top: 10,right: 5,),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(

                child: CircleAvatar(

                  backgroundColor: ColorManager.primary,
                  radius: 15,
                  backgroundImage: AssetImage("assets/images/splash screen.png"),
                ),
              ),


              MediumTextBold(text:'بقالة', color: ColorManager.black),



            ],
          ),),
      ),
    );


   }
  }

