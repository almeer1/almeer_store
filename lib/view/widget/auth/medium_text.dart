import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/text_style.dart';
class MediumText extends StatelessWidget {
  final String text;
  final Color color;




  MediumText({Key? key, required this.text, required this.color,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: getMediumStyle(color:color,)
    );
  }
}

class MediumTextt extends StatelessWidget {
  final String text;
  final Color color;




  MediumTextt({Key? key, required this.text, required this.color,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: getMediumStylle(color:color,)
    );
  }
}