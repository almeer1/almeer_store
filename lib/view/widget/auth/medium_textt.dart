import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/text_style.dart';
class MediumTexttBold extends StatelessWidget {
  final String text;
  final Color color;




  MediumTexttBold({Key? key, required this.text, required this.color,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: getMediumStylle(color: ColorManager.grey1)
    );
  }
}

