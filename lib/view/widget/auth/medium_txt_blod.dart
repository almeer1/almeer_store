import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/text_style.dart';
class MediumTextBold extends StatelessWidget {
  final String text;
  final Color color;




  MediumTextBold({Key? key, required this.text, required this.color,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: getBoldStyle(color: ColorManager.grey1)
    );
  }
}

