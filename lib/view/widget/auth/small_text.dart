import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/font_manger.dart';

import 'package:meerstore/core/constant/text_style.dart';
class SmallText extends StatelessWidget {
  final String text;




  SmallText({Key? key, required this.text,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: getLightStyle(color:ColorManager.grey1,)
    );
  }
}
class SmallTextt extends StatelessWidget {
  final String text;




  SmallTextt({Key? key, required this.text,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: getMediumStyle(color:ColorManager.grey1,)
    );
  }
}
