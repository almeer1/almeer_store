import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../../../data/model/list.dart';

class HomeBanner extends StatefulWidget {
  const HomeBanner({Key? key}) : super(key: key);

  @override
  State<HomeBanner> createState() => _HomeBannerState();
}

class _HomeBannerState extends State<HomeBanner> {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
      itemCount: image_List.length,
      itemBuilder: (BuildContext context, int index, realindex) {
        return Image.asset(
          image_List[index], width: double.infinity,
          // width: 500,
          fit: BoxFit.cover,
        );
      },
      options: CarouselOptions(
        viewportFraction: 1,
        // enlargeCenterPage: true,
        initialPage: 0,

        aspectRatio: 12 / 8,
        enableInfiniteScroll: true,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 3),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,

        //enlargeCenterPage: true,
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
