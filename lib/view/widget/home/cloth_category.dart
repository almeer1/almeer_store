import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:meerstore/view/widget/home/small_text.dart';


Widget ClothCategory(BuildContext context) {
  final ScrollController controllerOne = ScrollController();

  return  GridView(
    physics: const ClampingScrollPhysics(),
    scrollDirection: Axis.horizontal,
    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent:500,
        childAspectRatio: 2/ 1,
        crossAxisSpacing: 2,
        mainAxisSpacing:9
    ),
    /* shrinkWrap: true,
       crossAxisCount:2,
       crossAxisSpacing: 90,*/

    children: [
      for(int i=1;i<10;i++)
        Padding(
          padding: const EdgeInsets.only(top:23,right:0.01),
          child: Container(
            width: MediaQuery
                .of(context)
                .size
                .width * 0.45,

            child: Center(
              child: Column(
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        CircleAvatar(
                          radius: MediaQuery.of(context).size.width * 0.1,

                          backgroundImage: const AssetImage('assets/images/Kids.jpeg',),
                          backgroundColor: Colors.transparent,

                        ),

                      ]
                  ),

                  SizedBox(
                      height: MediaQuery.of(context).size.height*0.03,
                      child: HomeSmallText(text:'أزياء الاطفال')),



                ],
              ),
            ),
          ),
        ),

    ],
  );
}
//
//   Scrollbar(
//   child:GridView(
//     physics: const ClampingScrollPhysics(),
//     scrollDirection: Axis.horizontal,
//     gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
//         maxCrossAxisExtent: 200,
//         childAspectRatio:1/2,
//         crossAxisSpacing: 1,
//         mainAxisSpacing: 5
//     ),
//     /* shrinkWrap: true,
//        crossAxisCount:2,
//        crossAxisSpacing: 90,*/
//
//     children: [
//       for(int i=1;i<15;i++)
//         Container(
//           child: Column(
//             children: [
//               Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceAround,
//                   children: [
//                     CircleAvatar(
//                       radius: MediaQuery.of(context).size.width * 0.1,
//                       backgroundImage: AssetImage('assets/images/Kids.jpeg'),
//                       backgroundColor: Colors.transparent,
//
//                     ),
//
//                   ]
//               ),
//               SizedBox(height:2),
//               HomeSmallText(text:'أزياء الاطفال'),
//
//
//
//             ],
//           ),
//         ),
//
//     ],
//   ),
//
// );
