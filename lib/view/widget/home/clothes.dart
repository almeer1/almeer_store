import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../data/model/list.dart';


Widget Clothing(BuildContext context) {
  return SingleChildScrollView(
    scrollDirection: Axis.vertical,
    child: Row(
      children:  recommend_list.map((rec) {
        return Padding(
          padding: const EdgeInsetsDirectional.all(4),
          child: GestureDetector(
            child: Row(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  //mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsetsDirectional.only(bottom:5),
                      child:ClipRRect(
                        borderRadius: BorderRadius.circular(5),

                        child: Container(
                            width: MediaQuery.of(context).size.width*0.4,
                            height: MediaQuery.of(context).size.height*0.4,

                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey,),
                              borderRadius: BorderRadius.circular(5),

                            ),

                            padding: EdgeInsets.all(2),

                            //color: Colors.grey,
                            child: Column(

                              //mainAxisSize: MainAxisSize.min,

                              //crossAxisAlignment: CrossAxisAlignment.center,

                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 4,top: 3),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Icon(Icons.favorite_border,color: Colors.grey,),
                                      ],
                                    ),
                                  ),

                                  InkWell(onTap: (){
                                  /*  Navigator.push(context, MaterialPageRoute(builder:(context)=>DetailsPage()));*/
                                  },
                                      child: Image.asset(rec.image, width: MediaQuery.of(context).size.width*0.2, height: MediaQuery.of(context).size.height*0.2, fit: BoxFit.contain)),
                                  const SizedBox(height:0.01),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 3,top: 0.01),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [Text( rec.name, style: TextStyle( color: Colors.black45, fontSize: 17,fontFamily: 'Cairo' ), ),

                                        const SizedBox(height:0.1),

                                        Text( rec.description, style: TextStyle(color: Colors.black45, fontSize: 17,overflow: TextOverflow.ellipsis,fontFamily: 'Cairo'), ),

                                        const SizedBox(height: 10),

                                        Text( rec.price, style: TextStyle( fontWeight: FontWeight.bold, color: Colors.black45, fontSize: 16,fontFamily: 'Cairo' ),),

                                        const SizedBox(height: 10),
                                        Row(

                                          children: [
                                            Text(rec.text1,style: TextStyle(fontSize: 15,
                                                fontFamily: 'Cairo',
                                                decoration: TextDecoration.lineThrough),),
                                            SizedBox(width: 8,),
                                            Text(rec.text2,style: TextStyle(fontSize: 15,
                                              fontFamily: 'Cairo',color: Colors.green,
                                            ),)
                                          ],
                                        ),],
                                    ),
                                  )




                                ])

                        ),

                      ),
                    ),

                  ],
                ),
              ],
            ),
          ),
        );
      }).toList(),




    ),
  );
  /*return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child:


           Row(
              children:[
               for(int i=1;i<8;i++)
                 Row( children:[
                      Container(
                      width: 200,
                      height: 00,
                      padding: EdgeInsets.only(left: 10,right: 10,top: 10),
                           margin: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                            decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey,width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
                      child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,

                         children: [
                           Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                           children: [
                             Container(
                               padding: EdgeInsets.only(bottom:0.1),
                               decoration: BoxDecoration(
                                 color: Colors.black,
                                 borderRadius: BorderRadius.circular(200),
                               ),

                               child: Text('احسن المبيعات',style: TextStyle(color: Colors.yellow,fontFamily: 'Cairo',
                                 fontSize: 17,),),
                             ),
                             Icon(Icons.favorite_border,
                               color: Colors.grey,),

                           ],
                      ),
                        InkWell(onTap: (){}
                          ,child: Container(
                            margin: EdgeInsets.all(5),
                            child: Image.asset('assets/images/2.png',height: 120,width: 250,),

                          ),),
                        SizedBox(height: 5,),
                        Text('T500 ساعة ذكية مزودة',style: TextStyle(fontSize: 15,
                            fontFamily: 'Cairo'),),
                        SizedBox(height: 0.1,),
                        Text('بشاشة تعمل باللمس با',style: TextStyle(fontSize: 15,
                          fontFamily: 'Cairo',overflow:TextOverflow.ellipsis,),),

                        Container(
                          padding: EdgeInsets.only(top:4),
                          alignment: Alignment.centerRight,
                          child: Text('\$24.50',style: TextStyle(fontSize: 15,
                              fontWeight: FontWeight.bold,fontFamily: 'Cairo'),),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 2),
                          alignment: Alignment.centerRight,
                          child: Row(
                            children: [
                              Text('89.00 د.إ',style: TextStyle(fontSize: 15,
                                  fontFamily: 'Cairo',
                                  decoration: TextDecoration.lineThrough),),
                              SizedBox(width: 5,),
                              Text('خصم 72%',style: TextStyle(fontSize: 15,
                                fontFamily: 'Cairo',color: Colors.green,
                              ),)
                            ],
                          ),

                        ),









                        */
  /* Padding(
                padding: const EdgeInsets.only(top: 50),
                child: Row(

                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(bottom: 5),
                      decoration: BoxDecoration(
                        color: Colors.yellow,
                        borderRadius: BorderRadius.circular(30),
                      ),

                      child: Text('إكسبريس',style: TextStyle(color: Colors.black,fontFamily: 'Cairo',
                        fontSize: 10,),),
                    ),
                    Container(
                      child: Row(children: [
                        Icon(Icons.star,color: Colors.orange,),
                      Text('4',style: TextStyle(fontSize: 16,
                          color:Colors.orange,fontFamily: 'Cairo'),),
                        Text('(1.7k)',style: TextStyle(fontSize: 16,
                        color:Colors.grey,fontFamily: 'Cairo'),),
                      ],),
                    )


                  ],
                ),
              )*//**//*

            ],
          ),
        ),*//*

          ],
                 ),
              )]


          )]),


    );*/


//       GridView.count(
//       shrinkWrap: true,
//       crossAxisCount: 1,
//
//       physics: const ClampingScrollPhysics(),
//       scrollDirection: Axis.horizontal,
//       crossAxisSpacing:0.01 ,
//
//       mainAxisSpacing:0.1,
//       children:  recommend_list.map((rec) {
//         return Padding(
//           padding: const EdgeInsetsDirectional.all(10),
//           child: GestureDetector(
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//
//               children: [
//                 Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     mainAxisSize: MainAxisSize.min,
//                     children: <Widget>[
//                 Padding(
//                 padding: const EdgeInsetsDirectional.only(bottom: 5.0),
//                 child:ClipRRect(
//                   borderRadius: BorderRadius.circular(5),
//
//                   child: Container(
//                       width: MediaQuery.of(context).size.width*0.5,
//                       height: MediaQuery.of(context).size.height*0.3,
//
//                        decoration: BoxDecoration(
//                            border: Border.all(color: Colors.grey)
//                        ),
//
//                     padding: EdgeInsets.all(2),
//
//                     //color: Colors.grey,
//                       child: Column(
//
//                       mainAxisSize: MainAxisSize.min,
//
//                       crossAxisAlignment: CrossAxisAlignment.center,
//
//                       children: [
//
//                       Image.asset(rec.image, width: 100, height: 90, fit: BoxFit.contain),
//                         const SizedBox(height: 8),
//                     Text( rec.name, style: TextStyle( fontWeight: FontWeight.bold, color: Colors.black45, fontSize: 20,fontFamily: 'Cairo' ), ),
//
//                     const SizedBox(height: 8),
//
//                     Text( rec.description, style: TextStyle(color: Colors.black45, fontSize: 16,overflow: TextOverflow.ellipsis,fontFamily: 'Cairo'), ),
//
//                     const SizedBox(height: 8),
//
//                     Text( rec.price, style: TextStyle( fontWeight: FontWeight.bold, color: Colors.black45, fontSize: 16,fontFamily: 'Cairo' ),
// )])
//
//                     ),
//
//                 ),
//           ),
//
//         ],
//         ),
//               ],
//             ),
//         ),
//         );
//       }).toList(),
//
//
//
//
//     );
}
