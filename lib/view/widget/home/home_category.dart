import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:meerstore/data/model/category_model.dart';
import 'package:meerstore/view/widget/home/shimmer_loading.dart';
import 'package:meerstore/view/widget/home/small_text.dart';


import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/api_controller.dart';
import '../../screen/category/category_product.dart';
import '../../screen/category/parent_category.dart';

class HomeCategory extends StatefulWidget {
  const HomeCategory({Key? key}) : super(key: key);

  @override
  State<HomeCategory> createState() => _HomeCategoryState();
}

class _HomeCategoryState extends State<HomeCategory> {
  var dio = Dio();
  ApiController api = new ApiController();
  bool isloading = true;

  List <category> categoryData=[];




  // fetch category from Api
   Future<void> getCategory()async {
     var response =await api.getCategory();
        for( var post in response.data){
          setState(() {
            categoryData.add(category.fromJson(post));
            isloading = false;
          });
    } }
  @override
  void initState() {
    getCategory();
    // TODO: implement initState
    super.initState();

  }


  @override
  Widget build(BuildContext context) {

    final ScrollController controllerOne = ScrollController();

    return isloading?Center(
        child: Padding(
            padding: EdgeInsets.all(8),
            child:SizedBox(
              height: 140,
              child: Center(
                child: LoadingAnimationWidget.threeRotatingDots(
                  size: 64, color: ColorManager.grey1,
                ),
              ),
            )
        )):
      GridView(
        physics: const ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 0,
            mainAxisSpacing: 12),
        /* shrinkWrap: true,
         crossAxisCount:2,
         crossAxisSpacing: 90,*/

        children: [
          for (int i = 0; i < categoryData.length; i++)
           // if(categoryData[i].icon !='')
           if(categoryData[i].parent == categoryData[i].categoryId)

            Container(

              //   width:MediaQuery.of(context).size.width ,
              // height: MediaQuery.of(context).size.height * 0.4,
              height: 200,

              child:   Column(
              mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  //for(int i = 1; i <_category.length; i++)
                  InkWell(
                    onTap: () {
                      Navigator.push(context, PageRouteBuilder(pageBuilder: (_,__,___)=> ParentCategories(
                                      id: categoryData[i]
                                          .categoryId
                                          ),
                        transitionDuration: Duration(seconds: 2),

                      ));
                      // Navigator.push(
                      //     context,
                      //
                      //     MaterialPageRoute(
                      //         builder: (context) => ParentCategories(
                      //             id: categoryData[i]
                      //                 .categoryId
                      //                 )));

                    },
                    child:ShimmerLoading(
                      
                      isLoading: isloading,
                      child: Container(
                        height: 90,
                        width: 88,
                        // height: MediaQuery.of(context).size.height * 0.1,
                        // width: MediaQuery.of(context).size.width * 0.2,
                        decoration:  BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          image: DecorationImage(
                           image:NetworkImage('https://mapi.orasys.org/images/${categoryData[i].icon}'),
                            fit: BoxFit.fill,
                          )
                        ),
                      ),
                    )
                  ),
                  // SizedBox(height: MediaQuery.of(context).size.height*0.02),

                  Container(
                    child: FittedBox(
                     fit: BoxFit.fitWidth,
                      child: HomeSmallText(
                        text: categoryData[i].title.toString(),
                      ),
                    ),
                  ),
                ],
              ),
            ),

        ],
    
    );
  }
}
