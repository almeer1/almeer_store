import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:meerstore/core/constant/color_manger.dart';

import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/auth/small_text.dart';


Widget HomeOffer(BuildContext context) {
  return  Container(
    width:MediaQuery.of(context).size.width,
    height: MediaQuery.of(context).size.height,
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(right:20.0),
          child: Row(
            children: [
              SizedBox(//height: MediaQuery.of(context).size.height*0.03,
                  child: MediumTextBold(text: 'عروض ميجا', color:ColorManager.black)),
              const SizedBox(width: 10,),
              const Icon(FontAwesomeIcons.clock,color: Colors.grey,),

            ],
          ),
        ),//
        Row(
          children: [
            SizedBox(
              width:MediaQuery.of(context).size.width*0.5,
              height:MediaQuery.of(context).size.height*0.3,
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0),
                  ),
                  elevation: 0.0,
                  margin: const EdgeInsets.all(5),
                  child: Column(
                    //  mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height:MediaQuery.of(context).size.height*0.2,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(0),topRight:Radius.circular(0)),
                          image: DecorationImage(
                              image: AssetImage('assets/images/photo2.png',), fit: BoxFit.fill,),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: MediaQuery.of(context).size.height*0.03,
                                  child: MediumTextBold(color: ColorManager.black,text:'كرت هدية ')),
                              SizedBox(height: MediaQuery.of(context).size.height*0.03,
                                  child: SmallText(text:' تسليم الهدية')),
                            ]),
                      ),
                    ],
                  )),
            ),
            SizedBox(
              width:MediaQuery.of(context).size.width*0.5,
              height:MediaQuery.of(context).size.height*0.3,
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0),
                  ),
                  elevation: 0.0,
                  margin: const EdgeInsets.all(5),
                  child: Column(
                    //  mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Container(
                        height:MediaQuery.of(context).size.height*0.2,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(0),topRight:Radius.circular(0)),
                          image: DecorationImage(
                              image: AssetImage('assets/images/photo2.png'), fit: BoxFit.fill),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: MediaQuery.of(context).size.height*0.03,
                                  child: MediumTextBold(color: ColorManager.black,text:'كرت هدية ')),
                              SizedBox(height: MediaQuery.of(context).size.height*0.03,
                                  child: SmallText(text:' تسليم الهدية')),
                            ]),
                      ),
                    ],
                  )),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(
              width:MediaQuery.of(context).size.width*0.5,
              height:MediaQuery.of(context).size.height*0.3,
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0),
                  ),
                  elevation: 0.0,
                  margin: const EdgeInsets.all(5),
                  child: Column(
                    //  mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Container(
                        height:MediaQuery.of(context).size.height*0.2,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(0),topRight:Radius.circular(0)),
                          image: DecorationImage(
                              image: AssetImage('assets/images/photo2.png'), fit: BoxFit.fill),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: MediaQuery.of(context).size.height*0.03,
                                  child: MediumTextBold(color: ColorManager.black,text:'كرت هدية ')),
                              SizedBox(height: MediaQuery.of(context).size.height*0.03,
                                  child: SmallText(text:' تسليم الهدية')),
                            ]),
                      ),
                    ],
                  )),
            ),
            SizedBox(
              width:MediaQuery.of(context).size.width*0.5,
              height:MediaQuery.of(context).size.height*0.3,
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0),
                  ),
                  elevation: 0.0,
                  margin: const EdgeInsets.all(5),
                  child: Column(
                    //  mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Container(
                        height:MediaQuery.of(context).size.height*0.2,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(0),topRight:Radius.circular(0)),
                          image: DecorationImage(
                              image: AssetImage('assets/images/photo2.png'), fit: BoxFit.fill),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                  height: MediaQuery.of(context).size.height*0.03,
                                  child: MediumTextBold(color: ColorManager.black,text:'كرت هدية ')),
                              SizedBox(
                                  height: MediaQuery.of(context).size.height*0.03,
                                  child: SmallText(text:' تسليم الهدية')),
                            ]),
                      ),
                    ],
                  )),
            ),
          ],
        ),
      ],
    ),
  );
    /* Container(

       child: Padding(
         padding: const EdgeInsets.all(8.0),
         child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,

            children: [
              Row(

                children: [
                  MediumTextBold(text: 'عروض ميجا', color:ColorManager.black),
                  SizedBox(width: 10,),
                  Icon(FontAwesomeIcons.clock,color: Colors.grey,),
                  SizedBox(width: 10,),

                  MediumText(text: ' 24 ساعة', color:Colors.red)
                ],
              ),
              Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      children: [
                        Container(

                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),



                            ),

                            padding: EdgeInsets.only(right: 30,top: 5,left: 8),
                            child:Column(
                              children: [

                                Image.asset('assets/images/images.jpg',cacheWidth: 170,cacheHeight: 135,fit: BoxFit.fill,),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(0),
                                    color: ColorManager.white,
                                  ),
                                  width: MediaQuery.of(context).size.width*0.4 ,
                                  height: MediaQuery.of(context).size.height*0.06,
                                  child: Column(
//
                                    children: [
                                      MediumTextBold(color: ColorManager.black,text:'كرت هدية '),

                                      SmallText(text:' تسليم الهدية'),
                                    ],
                                  ),
                                )

                              ],
                            )
                        ),
                      ],
                    ),

                    Container(
                        padding: EdgeInsets.only(right: 8,top:5 ,left: 8),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),

                        ),

                        child:Column(
                          children: [
                            Image.asset('assets/images/images.jpg',cacheWidth: 170,cacheHeight: 135,fit: BoxFit.fill,),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(0),
                                color: ColorManager.white,
                              ),
                              width: MediaQuery.of(context).size.width*0.4 ,
                              height: MediaQuery.of(context).size.height*0.06,
                              child: Column(
                                children: [
                                  MediumTextBold(color: ColorManager.black,text:'نون كرت هدية '),
                                  SmallText(text:' تسليم الهدية '),
                                ],
                              ),
                            )

                          ],
                        )
                    ),
                  ],

                ),

              SizedBox(height: 9,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(

                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),



                      ),

                      padding: EdgeInsets.only(right: 30,top: 5,left: 8),
                      child:Column(
                        children: [

                          Image.asset('assets/images/images.jpg',cacheWidth: 170,cacheHeight: 135,fit: BoxFit.fill,),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(0),
                              color: ColorManager.white,
                            ),
                            width: MediaQuery.of(context).size.width*0.4 ,
                            height: MediaQuery.of(context).size.height*0.06,
                            child: Column(
//
                              children: [
                                MediumTextBold(color: ColorManager.black,text:'كرت هدية '),

                                SmallText(text:' تسليم الهدية'),
                              ],
                            ),
                          )

                        ],
                      )
                  ),

                  Container(
                      padding: EdgeInsets.only(right: 8,top:5 ,left: 8),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),

                      ),

                      child:Column(
                        children: [
                          Image.asset('assets/images/images.jpg',cacheWidth: 170,cacheHeight: 135,fit: BoxFit.fill,),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(0),
                              color: ColorManager.white,
                            ),
                            width: MediaQuery.of(context).size.width*0.4 ,
                            height: MediaQuery.of(context).size.height*0.06,
                            child: Column(
                              children: [
                                MediumTextBold(color: ColorManager.black,text:'نون كرت هدية '),
                                SmallText(text:' تسليم الهدية '),
                              ],
                            ),
                          )

                        ],
                      )
                  ),
                ],

              ),

            ],

    ),
       ),
     );*/
   /* GridView.count(
    //shrinkWrap: true,
    crossAxisCount: 2,
    physics: const ClampingScrollPhysics(),
    //scrollDirection: Axis.horizontal,
    crossAxisSpacing: 10,
    mainAxisSpacing:1,

    children:  offers_list.map((rec) {

      return Padding(
        padding: const EdgeInsetsDirectional.all(8),
        child: GestureDetector(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsetsDirectional.only(bottom: 5.0),
                child:ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Container(
                      width: MediaQuery.of(context).size.width*0.5,
                      height: MediaQuery.of(context).size.height*0.2,

                      padding: EdgeInsets.all(2),

                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey)
                      ),
                      child: Column(

                      mainAxisSize: MainAxisSize.min,

                      crossAxisAlignment: CrossAxisAlignment.center,

                      children: [

                        Image.asset(rec.image, width: 100, height:60, fit: BoxFit.contain),
                        const SizedBox(height: 0.1),
                        Text( rec.name, style: TextStyle( fontWeight: FontWeight.bold,fontFamily: "Cairo",color: Colors.black45, fontSize: 20, ), ),



                        Text( rec.description, style: TextStyle(color: Colors.black45, fontFamily: 'Cairo',fontSize: 16,overflow: TextOverflow.ellipsis), ),



                        Text( rec.price, style: TextStyle( fontWeight: FontWeight.bold, fontFamily: 'Cairo',color: Colors.black45, fontSize: 16, ),
                        )])

                  ),

                ),
              ),

            ],
          ),
        ),
      );
    }).toList(),




  );*/
}










