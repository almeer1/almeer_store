import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/view/widget/auth/medium_txt_blod.dart';
import 'package:meerstore/view/widget/auth/small_text.dart';

Widget HomePro(BuildContext context){
  return  Container(
    width:MediaQuery.of(context).size.width,
    height: MediaQuery.of(context).size.height*0.1,

    child: Row(
              children: [
                   Container(
                     width:MediaQuery.of(context).size.width*0.5,

                       child: Card(
                               shape: RoundedRectangleBorder(
                               borderRadius: BorderRadius.circular(0.0),
                  ),
                               elevation: 0.0,
                               margin: const EdgeInsets.all(3),
                     child: Column(
                    //  mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.start,
                           children: [

                                 Container(
                                       height:MediaQuery.of(context).size.height*0.2,
                                        decoration: const BoxDecoration(
                                         borderRadius: BorderRadius.only(topLeft: Radius.circular(0),topRight:Radius.circular(0)),
                                          image: DecorationImage(
                                              image: AssetImage('assets/images/photo2.png'),fit: BoxFit.fill),
                        ),
                      ),
                                 Padding(
                                     padding: const EdgeInsets.all(5.0),
                                      child: Column(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                                         children: [
                                             MediumTextBold(color: ColorManager.black,text:'كرت هدية '),
                                             SmallText(text:' تسليم الهدية'),
                              ]),
                        ),

                    ],
                  )),
            ),
                  Container(
                       width:MediaQuery.of(context).size.width*0.5,
                         child: Card(
                           shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(0.0),
                  ),
                           elevation: 0.0,
                           margin: const EdgeInsets.all(5),
                              child: Column(
                    //  mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.start,
                                     children: [
                                          Container(
                                               height:MediaQuery.of(context).size.height*0.2,
                                               decoration: const BoxDecoration(
                                               borderRadius: BorderRadius.only(topLeft: Radius.circular(0),topRight:Radius.circular(0)),
                                                 image: DecorationImage(
                                                    image: AssetImage('assets/images/photo2.png'),fit: BoxFit.fill,),
                        ),
                      ),
                                          Padding(
                                               padding: const EdgeInsets.all(3.0),
                                               child: Column(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                                                 children: [
                                                        MediumTextBold(color: ColorManager.black,text:'كرت هدية '),
                                                        SmallText(text:' تسليم الهدية'),
                            ]),
                      ),
                    ],
                  )),
            ),
          ],
        ),
  );






    /*Container(

    width: 500,
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 210,
          decoration: BoxDecoration(
           borderRadius: BorderRadius.circular(10)
          ),

            padding: EdgeInsets.only(right:5,top: 9),
        child:Column(
          children: [
            Image.asset('assets/images/images.jpg',cacheWidth: 198,cacheHeight: 100,fit: BoxFit.fill,),
            Container(
                                 color: ColorManager.white,
                                   width:198,
                                 height: MediaQuery.of(context).size.height*0.06,


                                 child: Column(

                                   children: [
                                     MediumTextBold(color: ColorManager.black,text:'كرت هدية '),

                                    SmallText(text:' تسليم الهدية'),
                                   ],
                                 ),
                               )

          ],
        )
        ),

        Container(
          width: 210,
            padding: EdgeInsets.only(right: 5,top: 9 ),
            child:Column(
              children: [
                Image.asset('assets/images/images.jpg',cacheWidth: 198,cacheHeight: 100,fit: BoxFit.fill,),
                Container(
                  color: ColorManager.white,
                  width:198,
                  height: MediaQuery.of(context).size.height*0.06,

                  child: Column(
                    children: [
                      MediumTextBold(color: ColorManager.black,text:'كرت هدية '),
                      SmallText(text:' تسليم الهدية '),
                    ],
                  ),
                )

              ],
            )
        ),
      ],

    ),
  );*/



  //     ListView(
  //         physics: NeverScrollableScrollPhysics(),
  //
  //       children: [
  //        for (int i=0;i < pro_list.length;i++)
  //          Container(
  //            decoration: BoxDecoration(borderRadius: BorderRadius.circular(9.0)),
  //
  //            padding: const EdgeInsets.all(3.0),
  //            child: Container(
  //              height: MediaQuery.of(context).size.height*2 ,
  //              decoration: BoxDecoration(borderRadius: BorderRadius.circular(9.0)),
  //              margin: EdgeInsets.all(2),
  //              width: MediaQuery.of(context).size.width*0.5 ,
  //
  //              child: Column(
  //                children: [
  //                  Image.asset(pro_list[i].image,height: 200,width: 212,fit: BoxFit.fill,),
  //                  Container(
  //                    color: ColorManager.white,
  //                      width: MediaQuery.of(context).size.width*0.5 ,
  //                    height: MediaQuery.of(context).size.height*0.09,
  //                    padding: EdgeInsets.all(8),
  //
  //                    child: Column(
  //                      children: [
  //                        MediumTextBold(color: ColorManager.black,text: pro_list[i].txt1,),
  //                        MediumText(color: ColorManager.black,text: pro_list[i].txt2,),
  //                      ],
  //                    ),
  //                  )
  //                ],
  //              ),
  //            ),
  //          ),
  // ]
  // )   );

}