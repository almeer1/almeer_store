class Product {
  int? id;
  String? productId;
  String? source;
  int? categoryId;
  String? productAr;
  String? productEn;
  String? description;
  String? company;
  String? distributor;
  int? minAmount;
  int? unit;
  int? price;
  int? amount;
  String? barcode;
  String? image;
  String? createdAt;
  String? updatedAt;
  String? expireDate;

  Product({
    this.id,
    this.productId,
    this.source,
    this.categoryId,
    this.productAr,
    this.productEn,
    this.description,
    this.company,
    this.distributor,
    this.minAmount,
    this.unit,
    this.price,
    this.amount,
    this.barcode,
    this.image,
    this.createdAt,
    this.updatedAt,
    this.expireDate,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json['id'],
      productId: json['productId'],
      source: json['source'],
      categoryId: json['categoryId'],
      productAr: json['productAr'],
      productEn: json['productEn'],
      description: json['description'],
      company: json['company'],
      distributor: json['distributor'],
      minAmount: json['minAmount'],
      unit: json['unit'],
      price: json['price'],
      amount: json['amount'],
      barcode: json['barcode'],
      image: json['image'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      expireDate: json['expireDate'],
    );
  }
}
