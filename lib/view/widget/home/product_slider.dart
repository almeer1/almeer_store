
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../data/model/list.dart';
class ProductSlider extends StatefulWidget {
  const ProductSlider({Key? key}) : super(key: key);

  @override
  _ProductSliderState createState() => _ProductSliderState();
}

class _ProductSliderState extends State<ProductSlider> {
  int activeIndex=0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [

        CarouselSlider.builder(
          options: CarouselOptions(height:MediaQuery.of(context).size.height*0.35,
          autoPlay: true,
           reverse: true,
            autoPlayAnimationDuration: const Duration(seconds: 1),
            viewportFraction: 1,
            enlargeCenterPage: true,
            enlargeStrategy: CenterPageEnlargeStrategy.height,
            pageSnapping: false,
            enableInfiniteScroll: false,
            onPageChanged: (index, reason) => setState(()=>activeIndex=index)

          ),

          itemCount:imagepro_list.length,
          itemBuilder: (BuildContext context, int index, int realIndex) {
          final imagepros=imagepro_list[index];
          return buildImage(imagepros,index);
          } ,


        ),
        const SizedBox(height: 3,),

        AnimatedSmoothIndicator(
     count:imagepro_list.length, activeIndex: activeIndex,
      effect:ScrollingDotsEffect(
        activeStrokeWidth: 2.6,
        activeDotScale: 1.3,
        maxVisibleDots: 5,
        radius: 8,
        spacing: 10,
        dotHeight: 10,
        dotWidth: 10,
      )

    )
      ],
    );

  }
}



Widget buildImage(String imagepros, int index) => Container(
margin: EdgeInsets.symmetric(horizontal: 0.2,),
  color: Colors.grey,
  child: Image.asset(imagepros,
    fit: BoxFit.fill,),
);

