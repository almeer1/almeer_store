import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:meerstore/core/constant/assets_Widget.dart';

import 'package:meerstore/view/screen/details/details_page.dart';

import '../../../bloc/cart_bloc.dart';

import '../../../core/app_bar.dart';
import '../../../core/constant/bottomnav_bar.dart';
import '../../../core/constant/color_manger.dart';
import '../../../data/data_source/DBHelper.dart';
import '../../../data/data_source/api_controller.dart';

import '../auth/medium_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeReco extends StatefulWidget {
  const HomeReco({
    Key? key,
  }) : super(key: key);

  @override
  State<HomeReco> createState() => _HomeRecoState();
}

class _HomeRecoState extends State<HomeReco> {
  var dio = Dio();

  DatabaseHelper dbHelper = DatabaseHelper();
  AssetWidget _assetWidget = AssetWidget();
  ApiController api = new ApiController();
  final scrollerController = ScrollController();
  bool isLoading = true;
  bool isLoadingMore = false;
  bool isFavorit = false;
  int number = 0;
  int page = 3;
  Color? color;
  String? isColor;

  // List<productPag> productData = [];
  List productData = [];
  List Favorite = [];

  //database
  List<Map<String, dynamic>> _getproduct = [];
  List<Map<String, dynamic>> _getfavorite = [];

  // fetch product from Api
  Future<void> fulttergetAllProduct() async {
    DioCacheManager dioCacheManager = DioCacheManager(CacheConfig());
    Options myOption =
        buildCacheOptions(const Duration(days: 30), forceRefresh: true);
    dio.interceptors.add(dioCacheManager.interceptor);
    String url = 'https://mapi.orasys.org/public/api/products/all?page=$page';
    final response = await dio.get(url);
    if (response.statusCode == 200) {
      setState(() {
        productData = productData + response.data['data'];
        isLoadingMore = false;
      });
    }
    // print(productData);
  }

  Future<void> _scorllListener() async {
    if (isLoadingMore) return;
    if (scrollerController.position.pixels ==
        scrollerController.position.maxScrollExtent) {
      setState(() {
        isLoadingMore = true;
      });
      page = page + 1;
      await fulttergetAllProduct();
      setState(() {
        isLoadingMore = false;
      });
    }
    // fulttergetAllProduct();
    // print('scorlercontroller called');
  }

  readData() async {
    _getfavorite = await dbHelper.queryAllRowsf();
  }

  bool colorf(int index) {
    for (int i = 0; i < _getfavorite.length; i++) {
      if (_getfavorite[i].containsKey("p_id")) {
        if (_getfavorite[i]["p_id"] == productData[index]['productId']) {
          isColor = _getfavorite[i]["favorite"];
          print(isColor);
        }
      }
    }
    return isColor == 'False';
  }

  @override
  void initState() {
    // getAllProduct();
    readData();
    scrollerController.addListener(_scorllListener);
    fulttergetAllProduct();
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    readData();
    scrollerController.addListener(_scorllListener);
    fulttergetAllProduct();
    super.dispose();
  }

  @override
  Widget build(context) {
    var mq = MediaQuery.of(context);
    print(mq.size.width);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarWidget(
        appBar: AppBar(
          elevation: 10,
        ),
      ),
      body: SingleChildScrollView(
          controller: scrollerController,
          scrollDirection: Axis.vertical,
          child: Column(children: [
            Center(
                child: GridView.builder(
                    shrinkWrap: true,
                    primary: false,
                    padding: const EdgeInsets.all(16),
                    gridDelegate: (mq.size.width < 370)
                        ? const SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 0.60, //heigh of container
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 10,
                            crossAxisCount: 2)
                        : const SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 0.64, //heigh of container
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 10,
                            crossAxisCount: 2),
                    itemCount: isLoadingMore
                        ? productData.length + 1
                        : productData.length,
                    itemBuilder: (BuildContext context, int index) {
                      if (index < productData.length) {
                        return Container(
                            // width: MediaQuery.of(context).size.width*0.5,
                            // height: MediaQuery.of(context).size.height*0.04,
                            // width: 250,
                            // height: 100,
                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.grey, width: 0.2),
                                borderRadius: BorderRadius.circular(0),
                                color: Colors.white),
                            child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Stack(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailsPage(
                                                        id: productData[index]
                                                                ['productId']
                                                            .toString(),
                                                      )));
                                          // print(__products[index].productId);
                                        },
                                        child: Center(
                                            child: Image.network(
                                                // "https://mapi.orasys.org/images/512/${productData[index]['image']}",
                                                "https://mapi.orasys.org/images/512/${productData[index]['image']}",
                                                // "https://mapi.orasys.org/images/512/256/${productData[index]['image']}",
                                                fit: BoxFit.fitWidth)),
                                      ),
                                      Positioned(
                                        right: 2.sp,
                                        top: 2.sp,
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.favorite,
                                            size: 30,
                                            color: ((_getfavorite.where((e) =>
                                                        e['p_id'] ==
                                                        productData[index]
                                                            ['productId']))
                                                    .map((e) => e['favorite'])
                                                    .any((e) => e == "True"))
                                                ? Colors.red
                                                : Colors.white,
                                          ),
                                          onPressed: () async {
                                            // get data from database
                                            _getfavorite =
                                                await dbHelper.queryAllRowsf();
                                            bool k = false;
                                            if (_getfavorite.length > 0) {
                                              for (int i = 0;
                                                  i < _getfavorite.length;
                                                  i++) {
                                                if (productData[index]
                                                        ['productId'] ==
                                                    _getfavorite[i]["p_id"]) {
                                                  k = true;
                                                  if (_getfavorite[i]
                                                          ["favorite"] ==
                                                      'True') {
                                                    await dbHelper.updatef(
                                                      {
                                                        "id": _getfavorite[i]
                                                            ["id"],
                                                        "p_id": _getfavorite[i]
                                                            ["p_id"],
                                                        "productName":
                                                            _getfavorite[i]
                                                                ["productName"],
                                                        "description":
                                                            _getfavorite[i]
                                                                ['description'],
                                                        "image": _getfavorite[i]
                                                            ["image"],
                                                        "price": _getfavorite[i]
                                                            ["price"],
                                                        "Amount": 1,
                                                        "favorite": 'False',
                                                        "color": "white"
                                                      },
                                                    ).then((value) {
                                                      const snackBar = SnackBar(
                                                        backgroundColor:
                                                            Colors.red,
                                                        content: Text(
                                                            'تمت إزالة المنتج من المفضلة',
                                                            style: TextStyle(
                                                                fontSize: 18,
                                                                fontFamily:
                                                                    'Cairo')),
                                                        duration: Duration(
                                                            milliseconds: 1),
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            10))),
                                                      );

                                                      ScaffoldMessenger.of(
                                                              context)
                                                          .showSnackBar(
                                                              snackBar);
                                                    });
                                                    await readData();
                                                    break;
                                                  } else {
                                                    k = true;
                                                    await dbHelper.updatef({
                                                      "id": _getfavorite[i]
                                                          ["id"],
                                                      "p_id": _getfavorite[i]
                                                          ["p_id"],
                                                      "productName":
                                                          _getfavorite[i]
                                                              ["productName"],
                                                      "description":
                                                          _getfavorite[i]
                                                              ['description'],
                                                      "image": _getfavorite[i]
                                                          ["image"],
                                                      "price": _getfavorite[i]
                                                          ["price"],
                                                      "Amount": 1,
                                                      "favorite": 'True',
                                                      "color": "white"
                                                    }).then((value) {
                                                      const snackBar = SnackBar(
                                                        backgroundColor:
                                                            Colors.green,
                                                        content: Text(
                                                          'تمت إضافة المنتج للمفضلة ',
                                                          style: TextStyle(
                                                              fontSize: 18,
                                                              fontFamily:
                                                                  'Cairo'),
                                                        ),
                                                        duration: Duration(
                                                            milliseconds: 1),
                                                      );
                                                      ScaffoldMessenger.of(
                                                              context)
                                                          .showSnackBar(
                                                              snackBar);
                                                    });
                                                    await readData();
                                                    setState(() {});
                                                    break;
                                                  }
                                                }
                                              }
                                            }
                                            if (k == false) {
                                              await dbHelper.insertF({
                                                "p_id": productData[index]
                                                    ['productId'],
                                                "productName":
                                                    productData[index]
                                                        ['productAr'],
                                                "description":
                                                    productData[index]
                                                        ['description'],
                                                "image":
                                                    "https://mapi.orasys.org/images/512/${productData[index]['image']}",
                                                "price": productData[index]
                                                    ['price'],
                                                "Amount": 1,
                                                "favorite": "True",
                                                "color": "red"
                                              }).then((value) {
                                                const snackBar = SnackBar(
                                                  backgroundColor: Colors.green,
                                                  content: Text(
                                                    'تمت إضافة المنتج للمفضلة',
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontFamily: 'Cairo'),
                                                  ),
                                                  duration:
                                                      Duration(milliseconds: 1),
                                                );
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(snackBar);
                                              });
                                              await readData();
                                              setState(() {});
                                            }

                                            setState(() {});
                                          },
                                        ),
                                      ),
                                      Positioned(
                                          left: 0,
                                          // top: 5,
                                          bottom: 130.sp,
                                          // right: 58,
                                          // top:0.2,
                                          child: RotationTransition(
                                              turns:
                                                  const AlwaysStoppedAnimation(
                                                      -43 / 360),
                                              child: Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.02,
                                                // width: MediaQuery.of(context).size.width*0.01,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            2),
                                                    color: ColorManager.grey1),
                                                child: productData[index]
                                                            ['amount'] ==
                                                        '0'
                                                    ? Center(
                                                        child: MediumText(
                                                        text: 'Out of stock',
                                                        color: Colors.white70,
                                                      ))
                                                    : productData[index]
                                                                ['amount'] ==
                                                            productData[index]
                                                                ['minAmount']
                                                        ? Center(
                                                            child: MediumText(
                                                            text:
                                                                'Out of stock',
                                                            color:
                                                                Colors.white70,
                                                          ))
                                                        : null,
                                              ))),
                                    ],
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(8),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              productData[index]['productAr']
                                                  .toString(),
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontFamily: 'Cairo',
                                                color: ColorManager.grey1,
                                                fontWeight: FontWeight.w700,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ),

                                            Text(
                                              productData[index]['description']
                                                  .toString(),
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontFamily: 'Cairo',
                                                color: ColorManager.grey1,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ),

                                            // SizedBox(height:0.1),
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                MediumText(
                                                  text:
                                                  NumberFormat.currency(locale: 'ar_SY',symbol: '',decimalDigits: 0).format(productData[index].price ) + ' ل س',
                                                  color: ColorManager.grey1,
                                                ),
                                                Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            2),
                                                    color: ColorManager.primary,
                                                  ),
                                                  child:
                                                      productData[index]
                                                                  ['amount'] ==
                                                              '0'
                                                          ? InkWell(
                                                              onTap: () {
                                                                const snackBar =
                                                                    SnackBar(
                                                                  backgroundColor:
                                                                      Colors
                                                                          .green,
                                                                     content: Text(
                                                                    ' عذراً غير متوفرة الآن',
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            18,
                                                                        fontFamily:
                                                                            'Cairo'),
                                                                  ),
                                                                  duration:
                                                                      Duration(
                                                                          seconds:
                                                                              1),
                                                                );
                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                        snackBar);
                                                              },
                                                              child: const Icon(
                                                                Icons.add,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            )
                                                          : InkWell(
                                                              onTap: () async {
                                                                // get data from database
                                                                _getproduct =
                                                                    await dbHelper
                                                                        .queryAllRows();

                                                                bool k = false;
                                                                if (_getproduct
                                                                        .length >
                                                                    0) {
                                                                  for (int i =
                                                                          0;
                                                                      i <
                                                                          _getproduct
                                                                              .length;
                                                                      i++) {
                                                                    if (productData[index]
                                                                            [
                                                                            'productId'] ==
                                                                        _getproduct[i]
                                                                            [
                                                                            "p_id"]) {
                                                                      k = true;
                                                                      await dbHelper
                                                                          .update(
                                                                        {
                                                                          "id": _getproduct[i]
                                                                              [
                                                                              "id"],
                                                                          "p_id":
                                                                              _getproduct[i]["p_id"],
                                                                          "productName":
                                                                              _getproduct[i]["productName"],
                                                                          "image":
                                                                              _getproduct[i]["image"],
                                                                          "price":
                                                                              _getproduct[i]["price"],
                                                                          "Amount":
                                                                              int.parse(_getproduct[i]["Amount"]) + 1,
                                                                          "MinAmount":
                                                                              _getproduct[i]['MinAmount'],
                                                                          "Quantety":
                                                                              _getproduct[i]['Quantety']
                                                                        },
                                                                      ).then((value) {
                                                                        const snackBar = SnackBar(
                                                                            backgroundColor:
                                                                                Colors.red,
                                                                            content: Text('تمت زيادة كمية المنتج', style: TextStyle(fontSize: 18, fontFamily: 'Cairo')),
                                                                            duration: Duration(milliseconds: 250));
                                                                        ScaffoldMessenger.of(context)
                                                                            .showSnackBar(snackBar);
                                                                      });
                                                                    }
                                                                  }
                                                                }
                                                                if (k ==
                                                                    false) {
                                                                  await dbHelper
                                                                      .insert({
                                                                    "p_id": productData[
                                                                            index]
                                                                        [
                                                                        'productId'],
                                                                    "productName":
                                                                        productData[index]
                                                                            [
                                                                            'productAr'],
                                                                    "image":
                                                                        "https://mapi.orasys.org/images/512/${productData[index]['image']}",
                                                                    "price": productData[
                                                                            index]
                                                                        [
                                                                        'price'],
                                                                    "Amount": 1,
                                                                    "MinAmount":
                                                                        productData[index]
                                                                            [
                                                                            'minAmount'],
                                                                    "Quantety":
                                                                        productData[index]
                                                                            [
                                                                            'amount']
                                                                  }).then((value) {
                                                                    const snackBar =
                                                                        SnackBar(
                                                                      backgroundColor:
                                                                          Colors
                                                                              .green,
                                                                      content: Text(
                                                                          'تمت إضافة المنتج للسلة ',
                                                                          style: TextStyle(
                                                                              fontSize: 18,
                                                                              fontFamily: 'Cairo')),
                                                                      duration: Duration(
                                                                          milliseconds:
                                                                          250),
                                                                    );
                                                                    ScaffoldMessenger.of(
                                                                            context)
                                                                        .showSnackBar(
                                                                            snackBar);
                                                                  });
                                                                }
                                                                BlocProvider.of<
                                                                            CartBloc>(
                                                                        context)
                                                                    .add(
                                                                        UpdateCartIconCount());

                                                                // await CartFunctions.updateCartIconCount();
                                                                setState(() {});
                                                              },
                                                              child: const Icon(
                                                                Icons.add,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                ]));
                      } else {
                        return Center(
                          child: SizedBox(
                            height: 140,
                            child: Center(
                              child: LoadingAnimationWidget.threeRotatingDots(
                                size: 64,
                                color: ColorManager.grey1,
                              ),
                            ),
                          ),
                        );
                      }
                    }))
          ])),
      bottomNavigationBar: BottomNavBa(
        pageIndex: 5,
      ),
    );
  }
}
