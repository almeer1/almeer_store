import 'package:dio/dio.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/text_style.dart';
import 'package:meerstore/data/data_source/api_controller.dart';
import 'package:meerstore/data/model/product.dart';

import 'package:meerstore/view/screen/search/search_page.dart';

import 'package:dio_http_cache/dio_http_cache.dart';

import '../../../data/data_source/endpoints_api.dart';
import '../../../data/model/searchproduct_model.dart';
import '../../screen/search/searchauot-page.dart';

class HomeSearch extends StatefulWidget {
  HomeSearch({
    Key? key,
  }) : super(key: key);

  @override
  _HomeSearchState createState() => _HomeSearchState();
}

class _HomeSearchState extends State<HomeSearch> {
  ApiController apiController = ApiController();

  final productAr = "";
  var dio = Dio();
  List<searchProduct> _searchP = [];
  bool isloading = true;

  void initState() {
    // getAllProduct();
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    // getAllProduct();
  }

  TextEditingController controller = new TextEditingController();

  @override
  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);
    Locale _locale = const Locale('ar', '');
    var lang = AppLocalizations.of(context);
    return Padding(
      padding: const EdgeInsets.only(right: 16, left: 16, top: 8, bottom: 8),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (ctx) => NetworkTypeAheadPage()));
          // Navigator.of(context)
          //     .push(MaterialPageRoute(builder: (ctx) => SearchPage()));
        },
        child: Container(
          // height: MediaQuery.of(context).size.height*0.06,
          // width: MediaQuery.of(context).size.width*2,
          height: 40,
          decoration: BoxDecoration(
            border: Border.all(width: 0.1),
            borderRadius: BorderRadius.circular(50),
            color: Color(0xFFf7f7f7),
          ),
          child: Row(
            children: [
              SizedBox(width:12),
              InkWell(
                child:
                    Icon(Icons.search, size: 32.sp, color: ColorManager.grey1),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (ctx) => NetworkTypeAheadPage()));
                },
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "البحث",
                style: TextStyle(
                    fontSize: 16.sp,
                    fontFamily: 'Cairo',
                    color: ColorManager.grey1),
              ),
              // TextFormField(
              //   onTap: (){  Navigator.of(context).push(MaterialPageRoute(
              //     builder: (ctx)=> SearchPage(posts: _searchP,)
              //   ));},
              //   expands: true,
              //   style: getMediumStyle(color: ColorManager.darkGrey),
              //
              //   maxLines: null,
              //   decoration: InputDecoration(
              //     filled: true,
              //     fillColor: const Color(0xFFf7f7f7),
              //     isDense: true,
              //     hintText:lang!.search,
              //     helperStyle:  TextStyle(fontFamily: 'Cairo',fontSize:30.sp),
              //
              //     prefixIcon:
            ],
          ),
        ),
      ),
    );
  }
}






/*



var mq=MediaQuery.of(context);
Locale _locale = Locale('ar', '');
var lang=AppLocalizations.of(context);
return Card(


shape: RoundedRectangleBorder(
side:
BorderSide(color: Colors.white, width: 0.1),
borderRadius: BorderRadius.circular(0),
),
clipBehavior: Clip.antiAlias,

elevation: 0,
child: Container(
height: MediaQuery.of(context).size.height*0.04,
width: MediaQuery.of(context).size.width*2,
child: TextFormField(
expands: true,
style: getMediumStyle(color: ColorManager.darkGrey),

keyboardType: TextInputType.text,
maxLines: null,

decoration: InputDecoration(
filled: true,
fillColor: Color(0xFFf7f7f7),

isDense: true,
hintText:lang!.search,
helperStyle: TextStyle(fontFamily: 'Cairo',fontSize: 20),

prefixIcon: Icon(
Icons.search,
size: 20,
color: Colors.black,
),
),

),
));
*/
