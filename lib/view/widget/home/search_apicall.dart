import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meerstore/view/widget/home/product_search.dart';

Future<List<Product>> searchProducts(String query) async {
  final response = await http.get(Uri.parse(
      'https://mapi.orasys.org/public/api/products/searchProducts?searchText=$query'));
  if (response.statusCode == 200) {
    final List<dynamic> results = json.decode(response.body);
    return results.map((result) => Product.fromJson(result)).toList();
  } else {
    throw Exception('Failed to load search results');
  }
}
