import 'package:flutter/material.dart';

import 'package:meerstore/view/widget/home/product_search.dart';
import 'package:meerstore/view/widget/home/search_apicall.dart';

class SearchPagee extends StatefulWidget {
  @override
  _SearchPageeState createState() => _SearchPageeState();
}

class _SearchPageeState extends State<SearchPagee> {
  TextEditingController _searchController = TextEditingController();
  List<Product> _searchResults = [];

  void _onSearch(String query) async {
    final results = await searchProducts(query);
    setState(() {
      // _searchResults = results
      //     .where((product) =>
      //         product.productEn!.toLowerCase().startsWith(query.toLowerCase()))
      //     // .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          controller: _searchController,
          onChanged: _onSearch,
          decoration: InputDecoration(
            hintText: 'Search',
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: _searchResults.length,
        itemBuilder: (context, index) {
          final product = _searchResults[index];
          return ListTile(
            title: Text(product.productEn.toString()),
            subtitle: Text(product.description.toString()),
            leading: Image.network(product.image.toString()),
            trailing: Text('\$${product.price}'),
          );
        },
      ),
    );
  }
}
