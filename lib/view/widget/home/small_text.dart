import 'package:flutter/material.dart';
import 'package:meerstore/core/constant/color_manger.dart';
import 'package:meerstore/core/constant/font_manger.dart';

import 'package:meerstore/core/constant/text_style.dart';
class HomeSmallText extends StatelessWidget {
  final String text;




  HomeSmallText({Key? key, required this.text,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 44,
      width: 120,
      child: Text(text,
          style: getBoldtSmallStyle(color:ColorManager.grey1,),
         textAlign: TextAlign.center,
         overflow: TextOverflow.ellipsis,////
      ),
    );
  }
}

