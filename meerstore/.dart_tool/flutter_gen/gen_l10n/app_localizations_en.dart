import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get lang_menu_arabic => 'العربية';

  @override
  String get lang_menu_english => 'English';

  @override
  String get login_save_dont_have_account => 'Do not have an account?';

  @override
  String get login => 'Login';

  @override
  String get login_welcome => 'Hala! Welcome back';

  @override
  String get forget_pass => 'Forget your password?';

  @override
  String get email_label => 'Email';

  @override
  String get password_label => 'Password';

  @override
  String get email_text => 'Please enter your email address';

  @override
  String get pass_text => 'Please enter your password';

  @override
  String get signup => 'Sign Up';

  @override
  String get create_account => 'Create an account';

  @override
  String get first_name => 'First Name';

  @override
  String get enter_first_name => 'Please enter your first name';

  @override
  String get last_name => 'Last Name';

  @override
  String get enter_last_name => 'Please enter your last name';

  @override
  String get back => 'Back';

  @override
  String get new_password_text => 'Your password must be different from the previous passwords';

  @override
  String get password_hint => 'Please enter the new password';

  @override
  String get reset_password => 'Reset password';

  @override
  String get create_new_password => 'Create new password';

  @override
  String get forgetPasswordText => 'Enter your email and we will send you a link to reset your password';

  @override
  String get check_email => 'Check your account';

  @override
  String get send_email => 'We have sent a password confirmation to your account';

  @override
  String get open_email_app => 'Open the email app';

  @override
  String get skip => 'Skip?';

  @override
  String get confirm_later => 'I will confirm later';

  @override
  String get receive_email => 'You did not receive the email? Check internet quality or';

  @override
  String get another_email => 'try another email';

  @override
  String get food => 'Food';

  @override
  String get noon => 'Noon';

  @override
  String get grocery => 'Grocery';

  @override
  String get food_text => 'Awesome offers on the most delicious meals';

  @override
  String get noon_text => 'Best prices for everything you need';

  @override
  String get grocery_text => 'Largest assortment of supplies';

  @override
  String get hom_text => 'اقتراحات لك';

  @override
  String get search => 'Search';
}
